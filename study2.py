import os
import numpy as np
import pandas as pd
from pymema import run
from scipy import stats

# const
ls_endpoint = ["nucleus_pixel","cytoplasm_pixel","cell_pixel","Dapi_total_nuc","EdU_total_nuc","KRT14_mean_cell","VIM_mean_cell"]

ls_feature_stats = ["min","max","range","median","mad","mean","std","var","skew","kurtosis"]
ls_feature_divergence = ["entropy_joint","entropy_conditional","dkl","absd","ed","sed"]
# feature entropy, entropy_fnn, divergence_fnn, utest are ok

ls_value_divergence = ["entropy_joint_bit","entropy_conditional_bit","dkl_bit","absd_bit","ed_bit","sed_bit**2"]


# function
def hitlist_popu(
        s_study,
        s_significance_metric="sigmad",
        i_ecm_significance_level=1,
        i_ligand_significance_level=1,
        i_min_count=3
    ):
    """
    based on abs and 4bue_popu_entropy data
    """
    s_significance = f"{s_significance_metric}{i_ecm_significance_level}x{i_ligand_significance_level}count{i_min_count}"
    es_out_feature = set()

    for s_axis in {"ecm_", "ligand_run"}:
        # set significance level
        if (s_axis == "ecm_"):
            i_significance_level = i_ecm_significance_level
        elif (s_axis == "ligand_run"):
            i_significance_level = i_ligand_significance_level
        else:
            sys.exit("Error @ hitlist_popu : unknown significance axis {s_axis}.\nknowen are ecm_ and ligand_run.")
        # off we go
        for s_trafo in {"log2scale", "rrscale"}:
            for s_endpoint in ls_endpoint:
                df_out = pd.DataFrame()
                print(f"hitlist_popu process: {s_significance} {s_axis} {s_trafo} {s_endpoint}")

                # stats
                for s_feature_stats in ls_feature_stats:

                    # load data
                    s_ipath_abs = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_stats/"
                    s_file = f"{s_ipath_abs}{s_study}-{s_endpoint}-{s_trafo}-abs-{s_feature_stats}.tsv.gz"
                    try:
                        df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                        # process
                        es_pertu = set(df_scdata[s_axis].unique())
                        es_pertu.discard('gelatin')
                        d_out = {}
                        for s_pertu in es_pertu:
                            se_scdata = df_scdata.loc[
                                df_scdata[s_axis].isin({s_pertu}),
                                f"{s_axis}-{s_significance_metric}"
                            ]
                            se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                            se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                            i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                            r_cut = i_total * 2/3
                            if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = -(se_scdata_down.shape[0])
                                d_out.update({s_pertu: i_score})
                            elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = se_scdata_up.shape[0]
                                d_out.update({s_pertu: i_score})
                            else:
                                pass
                        # update output
                        if (len(d_out) > 0):
                            s_out_feature = s_feature_stats
                            df_out = pd.merge(
                                df_out,
                                pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                                left_index=True,
                                right_index=True,
                                how="outer"
                            )
                            es_out_feature.add(s_out_feature)
                    except FileNotFoundError:
                        pass

                # entropy
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/entropy/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_entropy.tsv.gz"
                df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                try:
                    # process
                    es_pertu = set(df_scdata[s_axis].unique())
                    es_pertu.discard("gelatin")
                    d_out = {}
                    for s_pertu in es_pertu:
                        se_scdata = df_scdata.loc[
                            df_scdata[s_axis].isin({s_pertu}),
                            f"{s_axis.split('_')[0]}-{s_significance_metric}"
                        ]
                        se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                        se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                        i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                        r_cut = i_total * 2/3
                        if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = -(se_scdata_down.shape[0])
                            d_out.update({s_pertu: i_score})
                        elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = se_scdata_up.shape[0]
                            d_out.update({s_pertu: i_score})
                        else:
                            pass
                    # update output
                    if (len(d_out) > 0):
                        s_out_feature = "entropy"
                        df_out = pd.merge(
                            df_out,
                            pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                            left_index=True,
                            right_index=True,
                            how="outer"
                        )
                        es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass

                # entropy fnn
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_fnn_entropy/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-abs-fnn_entropy.tsv.gz"
                try:
                    df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                    # process
                    es_pertu = set(df_scdata[s_axis].unique())
                    es_pertu.discard("gelatin")
                    d_out = {}
                    for s_pertu in es_pertu:
                        se_scdata = df_scdata.loc[
                            df_scdata[s_axis].isin({s_pertu}),
                            f"{s_axis.split('_')[0]}-{s_significance_metric}"
                        ]
                        se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                        se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                        i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                        r_cut = i_total * 2/3
                        if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = -(se_scdata_down.shape[0])
                            d_out.update({s_pertu: i_score})
                        elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = se_scdata_up.shape[0]
                            d_out.update({s_pertu: i_score})
                        else:
                            pass
                    # update output
                    if (len(d_out) > 0):
                        s_out_feature = "entropy_fnn"
                        df_out = pd.merge(
                            df_out,
                            pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                            left_index=True,
                            right_index=True,
                            how="outer"
                        )
                        es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass

                # divergence
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/divergence/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_divergence.tsv.gz"
                try:
                    df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                    # process
                    for s_feature_divergence in ls_feature_divergence:
                        es_pertu = set(df_scdata[s_axis].unique())
                        es_pertu.discard("gelatin")
                        d_out = {}
                        for s_pertu in es_pertu:
                            se_scdata = df_scdata.loc[
                                df_scdata[s_axis].isin({s_pertu}),
                                f"{s_feature_divergence}-{s_significance_metric}"
                            ]
                            se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                            se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                            i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                            r_cut = i_total * 2/3
                            if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = -(se_scdata_down.shape[0])
                                d_out.update({s_pertu: i_score})
                            elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = se_scdata_up.shape[0]
                                d_out.update({s_pertu: i_score})
                            else:
                                pass
                        # update output
                        if (len(d_out) > 0):
                            s_out_feature = s_feature_divergence
                            df_out = pd.merge(
                                df_out,
                                pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                                left_index=True,
                                right_index=True,
                                how="outer"
                            )
                            es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass

                # divergence fnn
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_fnn_divergence/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-abs-fnn_dkl.tsv.gz"
                try:
                    df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                    # process
                    es_pertu = set(df_scdata[s_axis].unique())
                    es_pertu.discard("gelatin")
                    d_out = {}
                    for s_pertu in es_pertu:
                        se_scdata = df_scdata.loc[
                            df_scdata[s_axis].isin({s_pertu}),
                            f"dkl_fnn-{s_significance_metric}"
                        ]
                        se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                        se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                        i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                        r_cut = i_total * 2/3
                        if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = -(se_scdata_down.shape[0])
                            d_out.update({s_pertu: i_score})
                        elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = se_scdata_up.shape[0]
                            d_out.update({s_pertu: i_score})
                        else:
                            pass
                    # update output
                    if (len(d_out) > 0):
                        s_out_feature = "dkl_fnn"
                        df_out = pd.merge(
                            df_out,
                            pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                            left_index=True,
                            right_index=True,
                            how="outer"
                        )
                        es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass

                # utest
                # load data
                s_ipath_utest = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_utest/"
                s_file = f"{s_ipath_utest}{s_study}-{s_endpoint}-{s_trafo}-abs-utest.tsv.gz"
                try:
                    df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                    # process
                    es_pertu = set(df_scdata[s_axis].unique())
                    es_pertu.discard("gelatin")
                    d_out = {}
                    for s_pertu in es_pertu:
                        se_scdata = df_scdata.loc[
                            df_scdata[s_axis].isin({s_pertu}),
                            f"utest-{s_significance_metric}"
                        ]
                        se_scdata = se_scdata.loc[se_scdata < 0.001]
                        if (se_scdata.shape[0] >= i_min_count):
                            i_score = se_scdata.shape[0]
                            d_out.update({s_pertu: i_score})
                    # update output
                    if (len(d_out) > 0):
                        s_out_feature = "utest"
                        df_out = pd.merge(
                            df_out,
                            pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                            left_index=True,
                            right_index=True,
                            how="outer"
                        )
                        es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                        pass

                # write to file
                s_opath = f"./dataMema/{s_study}/mema_result_{s_significance}/"
                s_file = f"{s_opath}{s_study}-{s_endpoint}-{s_trafo}-result_{s_axis.split('_')[0]}_{s_significance}.tsv.gz"
                os.makedirs(s_opath, exist_ok=True)
                df_out.to_csv(s_file, sep="\t", compression="gzip")

        # fuse hit list from log2scale and rrscale
        '''
        df_output = pd.DataFrame()
        for s_out_feature in sorted(es_out_feature):
            #print(f"scale fuseing feature: {s_out_feature}")
            try:
                df_feature = df_out.loc[:,[f"log2scale-{s_out_feature}",f"rrscale-{s_out_feature}"]]
                se_feature = df_feature.loc[df_feature[f"log2scale-{s_out_feature}"].notna(),:].loc[df_feature[f"rrscale-{s_out_feature}"].notna(),:].mean(axis=1)
                se_feature.name = s_out_feature
                df_output = pd.merge(
                    df_output,
                    se_feature.to_frame(),
                    left_index=True,
                    right_index=True,
                    how="outer"
                )
            except KeyError:
                pass

        # write to file
        s_opath = f"./dataMema/{s_study}/mema_result_{s_significance}/"
        s_file = f"{s_opath}{s_study}-{s_endpoint}-result_{s_axis.split('_')[0]}_{s_significance}.tsv.gz"
        os.makedirs(s_opath, exist_ok=True)
        df_output.index = pd.Series(df_output.index).replace(np.nan, 'None')
        df_output.index.name = s_axis.split('_')[0]
        df_output.to_csv(s_file, sep="\t", compression="gzip")
        '''


def hitlist_popu_significancefusion(
        s_study,
        ls_significance_tag=[
            "sigmad2x2count2",
            "sigmad1x1count3",
        ],
        ls_endpoint=[
            "KRT14_mean_cell",
            "VIM_mean_cell",
        ],
        ls_metric=[
            "entropy",
            "dkl",
        ]
    ):
    """
    based on abs and 4bue_popu_entropy data
    """
    # off we go
    ls_header = ["index", "study", "perturbation", "axis", "sigificance", "endpoint","metric", "measure"]
    for s_trafo in {"log2scale", "rrscale"}:
        for s_axis in {"ecm", "ligand"}:
            print(f"hitlist_popu_significancefusion processing: {s_trafo} {s_axis}")
            i_index = 0
            ll_out =[]
            for s_endpoint in ls_endpoint:
                for s_metric in ls_metric:
                    df_result = pd.DataFrame()
                    for s_significance_tag in ls_significance_tag:
                        # load data
                        #s_path = f"./mema_result_{s_significance_tag}/"
                        s_path = f"./dataMema/{s_study}/mema_result_{s_significance_tag}/"
                        s_file = f"{s_study}-{s_endpoint}-{s_trafo}-result_{s_axis}_{s_significance_tag}.tsv.gz"
                        df_input = pd.read_csv(f"{s_path}{s_file}", sep="\t", index_col=0)
                        # extarct result
                        s_column = f"{s_trafo}-{s_metric}"
                        s_column_tag = f"{s_column}-{s_significance_tag}"
                        se_input = df_input.loc[df_input[s_column].notna(),s_column]
                        se_input.name = s_column_tag
                        df_result = pd.merge(df_result, se_input.to_frame(), left_index=True, right_index=True, how="outer")
                    # fuse result
                    for s_column_tag in df_result.columns:
                        if ("output" in df_result.columns):
                            df_result["output"] = df_result["output"].astype(str) + "|" + df_result[s_column_tag].astype(str) 
                        else:
                            df_result["output"] = df_result[s_column_tag].astype(str)
                    # update output
                    for s_pertu in df_result.index:
                        i_index += 1
                        s_measure = df_result.loc[s_pertu,"output"]
                        l_out = [i_index, s_study, s_pertu, s_axis, "|".join(ls_significance_tag), s_endpoint, s_metric, s_measure]
                        ll_out.append(l_out)
            # write output to file
            df_out = pd.DataFrame(ll_out, columns=ls_header)
            df_out.set_index("index", inplace=True)
            s_path = f"./dataMema/{s_study}/mema_result_{'_'.join(sorted(ls_significance_tag))}/"
            s_file = f"{s_study}-{'_'.join(sorted(ls_endpoint))}-{'_'.join(sorted(ls_metric))}-{s_trafo}-result_{s_axis}_{'_'.join(sorted(ls_significance_tag))}.tsv"
            os.makedirs(s_path, exist_ok=True)
            df_out.to_csv(f"{s_path}{s_file}",  sep="\t")

    # output
    return(True)



def hitlist_popu_joint(
        s_study,
        es_endpoint={"KRT14_mean_cell", "VIM_mean_cell"},
        ei_fuse={1,2,4},
        s_significance_metric="sigmad",
        i_ecm_significance_level=1,
        i_ligand_significance_level=1,
        i_min_count=3
    ):
    """
    based on abs and 4bue_popu_entropy data
    """
    s_endpoint = "-".join(sorted(es_endpoint, reverse=True))
    s_significance = f"{s_significance_metric}{i_ecm_significance_level}x{i_ligand_significance_level}count{i_min_count}"
    es_out_feature = set()

    for s_axis in {"ecm_", "ligand_run"}:
        # set significance level
        if (s_axis == "ecm_"):
            i_significance_level = i_ecm_significance_level
        elif (s_axis == "ligand_run"):
            i_significance_level = i_ligand_significance_level
        else:
            sys.exit("Error @ hitlist_popu : unknown significance axis {s_axis}.\nknowen are ecm_ and ligand_run.")
        # off we go
        for s_trafo in {"log2scale", "rrscale"}:
            for i_fuse in ei_fuse:
                s_analysis = s_endpoint.replace("-","_") +  f"_fusion{i_fuse}"
                df_out = pd.DataFrame()
                print(f"hitlist_popu_joint process: {s_significance} {s_axis} {s_trafo} {s_analysis}")

                # entropy_joint
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy_joint/{s_trafo}/entropy/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_analysis}-{s_trafo}-4bue_popu_entropy.tsv.gz"
                df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                try:
                    # process
                    es_pertu = set(df_scdata[s_axis].unique())
                    es_pertu.discard("gelatin")
                    d_out = {}
                    for s_pertu in es_pertu:
                        se_scdata = df_scdata.loc[
                            df_scdata[s_axis].isin({s_pertu}),
                            f"{s_axis.split('_')[0]}-{s_significance_metric}"
                        ]
                        se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                        se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                        i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                        r_cut = i_total * 2/3
                        if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = -(se_scdata_down.shape[0])
                            d_out.update({s_pertu: i_score})
                        elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                            i_score = se_scdata_up.shape[0]
                            d_out.update({s_pertu: i_score})
                        else:
                            pass
                    # update output
                    if (len(d_out) > 0):
                        s_out_feature = "entropy"
                        df_out = pd.merge(
                            df_out,
                            pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                            left_index=True,
                            right_index=True,
                            how="outer"
                        )
                        es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass


                # divergence
                # load data
                s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy_joint/{s_trafo}/divergence/"
                s_file = f"{s_ipath_entropy}{s_study}-{s_analysis}-{s_trafo}-4bue_popu_divergence.tsv.gz"
                try:
                    df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
                    # process
                    for s_feature_divergence in ls_feature_divergence:
                        es_pertu = set(df_scdata[s_axis].unique())
                        es_pertu.discard("gelatin")
                        d_out = {}
                        for s_pertu in es_pertu:
                            se_scdata = df_scdata.loc[
                                df_scdata[s_axis].isin({s_pertu}),
                                f"{s_feature_divergence}-{s_significance_metric}"
                            ]
                            se_scdata_down = se_scdata.loc[se_scdata < -(i_significance_level)]
                            se_scdata_up = se_scdata.loc[se_scdata > i_significance_level]
                            i_total = se_scdata_down.shape[0] + se_scdata_up.shape[0]
                            r_cut = i_total * 2/3
                            if (se_scdata_down.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = -(se_scdata_down.shape[0])
                                d_out.update({s_pertu: i_score})
                            elif (se_scdata_up.shape[0]  > r_cut) and (i_total >= i_min_count):
                                i_score = se_scdata_up.shape[0]
                                d_out.update({s_pertu: i_score})
                            else:
                                pass
                        # update output
                        if (len(d_out) > 0):
                            s_out_feature = s_feature_divergence
                            df_out = pd.merge(
                                df_out,
                                pd.Series(d_out, name=f"{s_trafo}-{s_out_feature}").to_frame(),
                                left_index=True,
                                right_index=True,
                                how="outer"
                            )
                            es_out_feature.add(s_out_feature)
                except FileNotFoundError:
                    pass

                # write to file
                s_opath = f"./dataMema/{s_study}/mema_result_{s_significance}/"
                s_file = f"{s_opath}{s_study}-{s_analysis}-{s_trafo}-result_{s_axis.split('_')[0]}_{s_significance}.tsv.gz"
                os.makedirs(s_opath, exist_ok=True)
                df_out.to_csv(s_file, sep="\t", compression="gzip")


def feature_zscore_popu(s_study, s_how="inner"):
    """
    this is input for t-sne and umap maybe i combine.
    """
    for s_trafo in {"log2scale", "rrscale"}:
        for s_endpoint in ls_endpoint:
            print(f"feature_zscore_popu process: {s_trafo} {s_endpoint}")
            b_get_annotation= True

            ## stats ##
            for s_feature_stats in ls_feature_stats:
                # load data
                s_ipath_abs = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_stats/"
                s_file = f"{s_ipath_abs}{s_study}-{s_endpoint}-{s_trafo}-abs-{s_feature_stats}.tsv.gz"
                df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)

                # annot
                if (b_get_annotation):
                    df_out = df_scdata.loc[:,run.ls_LABEL_POPU].copy()
                    b_get_annotation = False

                # process
                s_value = f"{s_endpoint}-{s_trafo}-abs-{s_feature_stats}"
                #print(s_value)
                #print(df_scdata.info())
                se_popu = df_scdata[s_value]
                se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
                se_popu.name = s_value
                df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)


            ## entropy ##
            # load data
            s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/entropy/"
            s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_entropy.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "entropy_bit"
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            ## entropy fnn ##
            # load data
            s_ipath_entropy_fnn = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_fnn_entropy/"
            s_file = f"{s_ipath_entropy_fnn}{s_study}-{s_endpoint}-{s_trafo}-abs-fnn_entropy.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "entropy_fnn_bit"
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            ## divergence ##
            # load data
            s_ipath_divergence = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/divergence/"
            s_file = f"{s_ipath_divergence}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_divergence.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            for s_value in ls_value_divergence:
                #print(s_value)
                #print(df_scdata.info())
                se_popu = df_scdata[s_value]
                se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
                se_popu.name = s_value
                df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            # divergence fnn
            # load data
            s_ipath_dkl_fnn = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_fnn_divergence/"
            s_file = f"{s_ipath_dkl_fnn}{s_study}-{s_endpoint}-{s_trafo}-abs-fnn_dkl.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "dkl_fnn_bit"
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)


            ## utest ##
            # bue 20190531: this needs to be a proper translationform pvalue to z score
            '''
            # load data
            s_ipath_utest = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_utest/"
            s_file = f"{s_ipath_utest}{s_study}-{s_endpoint}-{s_trafo}-abs-utest.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "p_value"
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu = pd.Series(stats.mstats.zscore(se_popu), index=se_popu.index)
            se_popu.name = "utest_pvalue"
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)
            '''

            # write to file
            s_opath = f"./dataMema/{s_study}/mema_result_zscore/{s_trafo}/"
            s_file = f"{s_opath}{s_study}-{s_endpoint}-{s_trafo}-result_zscore.tsv.gz"
            os.makedirs(s_opath, exist_ok=True)
            df_out.index.name = "index"
            df_out.to_csv(s_file, sep="\t", compression="gzip")


def entropy_expression_popu(s_study, s_how="inner"):
    for s_trafo in {"log2scale", "rrscale"}:
        for s_endpoint in ls_endpoint:
            print(f"entropy_expression_popu process: {s_trafo} {s_endpoint}")

            # stats median
            # load data
            s_ipath_abs = f"./dataMema/{s_study}/mema_extract_abs/{s_trafo}/popu_stats/"
            s_file = f"{s_ipath_abs}{s_study}-{s_endpoint}-{s_trafo}-abs-median.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)

            # annot
            df_out = df_scdata.loc[:,run.ls_LABEL_POPU].copy()

            # process
            s_value = f"{s_endpoint}-{s_trafo}-abs-median"
            #print(s_value)
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            # entropy
            # load data
            s_ipath_entropy = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/entropy/"
            s_file = f"{s_ipath_entropy}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_entropy.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "entropy_bit"
            #print(s_value)
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            # divergence
            # load data
            s_ipath_divergence = f"./dataMema/{s_study}/mema_extract_4bue_popu_entropy/{s_trafo}/divergence/"
            s_file = f"{s_ipath_divergence}{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_divergence.tsv.gz"
            df_scdata = pd.read_csv(s_file, sep="\t", index_col=0)
            # process
            s_value = "dkl_bit"
            #print(s_value)
            #print(df_scdata.info())
            se_popu = df_scdata[s_value]
            se_popu.name = s_value
            df_out = pd.merge(df_out, se_popu.to_frame(), left_index=True, right_index=True, how=s_how)

            # write to file
            s_opath = f"./dataMema/{s_study}/mema_result_entropy/{s_trafo}/"
            s_file = f"{s_opath}{s_study}-{s_endpoint}-{s_trafo}-result_entropy.tsv.gz"
            os.makedirs(s_opath, exist_ok=True)
            df_out.to_csv(s_file, sep="\t", compression="gzip")
