####
# title: mema_norm_to_rdataframe.py
#
# language: python3
# license: GPLv>=3
# date: 2019-08-26
# author: bue
#
# run: python3 mema_norm_to_rdataframe.py
#
# description:
#     glue datafrane for analysis together
####

# library
import os
import pandas as pd
from pymema import run

# const
s_study = os.getcwd().split("/")[-1]

s_path_abs = "./mema_extract_abs/"
#s_path_annot = "./mema_ac/"
#s_fpath_actsv = "./mema_ac/{}_tidy_perturbation.tsv.gz"

es_endpoint = [
    "cell_pixel",
    "cytoplasm_pixel",
    "Dapi_total_nuc",
    "EdU_total_nuc",
    "KRT14_mean_cell",
    "nucleus_pixel",
    "VIM_mean_cell",
]

s_opath = "../../output/df_glued/"
os.makedirs(s_opath, exist_ok=True)


# function
def glue_segement_df_cell():
    """
    description:
        glues guillaumes mesurement
    """
    print(f"\nglue_segement_df_cell ...")
    df_glued_study = pd.DataFrame()

    # for each run
    for s_file in sorted ([s_file for s_file in os.listdir("./mema_level0/") if s_file.endswith("_imageIDs.tsv")]):
        s_run = s_file.split("_")[0]
        s_runid = f"mema-{s_run}"
        print(f"glue_segement_df_cell process {s_file} ...")

        # load perturbation annotation
        df_assay_coor = run.perturbation_antsv2df(f"./mema_ac/{s_run}_tidy_perturbation.tsv.gz")

        # load imageid to spot mapping
        df_coor_image = pd.read_csv(f"./mema_level0/{s_file}", sep="\t")
        # get coor
        df_coor_image.sort_values(["WellName","Row","Column"], inplace=True)
        df_coor_image.reset_index(inplace=True)
        df_coor_image["coor"] = df_coor_image.index + 1
        # extract relevanty columns
        df_coor_image = df_coor_image.loc[:,["coor","ImageID"]]

        # load guillaume segmentation output
        df_image_segment = pd.DataFrame()
        for s_well in [
                "_Well_1_level_0.csv",
                "_Well_2_level_0.csv",
                "_Well_3_level_0.csv",
                "_Well_4_level_0.csv",
                "_Well_5_level_0.csv",
                "_Well_6_level_0.csv",
                "_Well_7_level_0.csv",
                "_Well_8_level_0.csv",
            ]:
            try:
                df_image_segment = df_image_segment.append(
                    pd.read_csv(f"./mema_level0/{s_file.replace('_imageIDs.tsv',s_well)}")
                )
            except FileNotFoundError:
                print(f"Warning: {s_file.replace('_imageIDs.tsv',s_well)} does not exit (which might be ok for this run or might not).")

        # run fusion
        df_coor_image_segement = pd.merge(df_coor_image, df_image_segment, on="ImageID")
        df_glued_run = pd.merge(df_assay_coor, df_coor_image_segement, on="coor")
        df_glued_run.rename({"Label": "CellID"}, axis=1, inplace=True)

        # handle index
        df_glued_run.index = df_glued_run.apply(lambda n: f"{n.runid}-{n.coor}-{n.CellID}", axis=1)
        df_glued_run.index.name = "index"
        df_glued_run.sort_index(axis=0, inplace=True)

        # study fusion
        df_glued_study = df_glued_study.append(df_glued_run, verify_integrity=True)

    # write to file
    print(f"glue_segement_df_cell zipping result ...")
    df_glued_study.to_csv(f"{s_opath}{s_study}_df_segmentation.tsv.gz", sep="\t", compression="gzip", header=True)


def glue_norm_df_cell():
    """
    description:
        glues normaliced singel cell level data together.
    """
    print(f"\nglue_norm_df_cell ...")
    for s_norm in {"log2scale","rrscale"}:
        # glue
        df_glued_study = None
        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/")):
            if s_file.startswith(s_study) and s_file.endswith(f"-{s_norm}-abs.tsv.gz"):
                print(f"process glue_norm_df_cell {s_file} ...")
                df_endpoint = pd.read_csv(f"./mema_extract_abs/{s_norm}/{s_file}", sep="\t")
                if (df_glued_study is None):
                    df_glued_study = df_endpoint
                    df_glued_study.drop({"ecm-sigma", "ecm-sigmad", "ligand-sigma", "ligand-sigmad"}, axis=1, inplace=True)
                else:
                    s_endpoint = s_file.split("-")[1]
                    df_glued_study = pd.merge(df_glued_study, df_endpoint.loc[:,s_endpoint], left_index=True, right_index=True)

        # writet to file
        print(f"glue_norm_df_cell zipping result ...")
        df_glued_study.to_csv(f"{s_opath}{s_study}_df_{s_norm}_cell.tsv.gz", sep="\t", compression="gzip", header=True, index=False)


def glue_norm_df_spot():
    """
    description:
        glues normaliced spot level data together.
    """
    print(f"\nglue_norm_df_spot ...")
    for s_norm in {"log2scale","rrscale"}:
        df_glued_study = pd.DataFrame()
        for s_file_run in sorted(os.listdir(f"./mema_extract_abs/log2scale/coor_cellcount/Dapi_total_nuc-log2scale-cellcount/")):
            if s_file_run.startswith(s_study) and s_file_run.endswith("-Dapi_total_nuc-log2scale-cellcount.tsv.gz"):
                print(f"glue_norm_df_spot process {s_file_run} ...")
                s_run = s_file_run.split("-")[1]

                # glue coor_cellcount
                df_glued_run = pd.read_csv(
                    f"./mema_extract_abs/log2scale/coor_cellcount/Dapi_total_nuc-log2scale-cellcount/{s_study}-{s_run}-Dapi_total_nuc-log2scale-cellcount.tsv.gz",
                    sep="\t",
                    index_col=0,
                )
                df_glued_run.drop({"y-sigma", "y-sigmad", "x-sigma", "x-sigmad"}, axis=1, inplace=True)
                df_glued_run.index = df_glued_run.runid_coor

                # glue coor_stats
                for s_dir_endpoint in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/coor_stats/")):
                    if s_dir_endpoint.endswith(f"-{s_norm}-abs"):
                        s_endpoint = s_dir_endpoint.split("-")[0]
                        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/coor_stats/{s_dir_endpoint}/")):
                            if s_file.startswith(f"{s_study}-{s_run}-{s_dir_endpoint}") and s_file.endswith(".tsv.gz"):
                                s_statistic = s_file.split("-")[-1].split(".")[0]
                                print(f"glue_norm_df_spot process {s_file} ...")
                                df_stats =  pd.read_csv(
                                    f"./mema_extract_abs/{s_norm}/coor_stats/{s_dir_endpoint}/{s_study}-{s_run}-{s_dir_endpoint}-{s_statistic}.tsv.gz",
                                    sep="\t",
                                    index_col=0,
                                )
                                df_stats.index = df_stats.runid_coor
                                df_glued_run = pd.merge(
                                    df_glued_run,
                                    df_stats.loc[:,f"{s_dir_endpoint}-{s_statistic}"],
                                    left_index=True,
                                    right_index=True,
                                )
                # glue run to study
                df_glued_study = df_glued_study.append(df_glued_run, verify_integrity=True)

        # writet to file
        print(f"glue_norm_df_spot zipping result ...")
        df_glued_study.to_csv(f"{s_opath}{s_study}_df_{s_norm}_spot.tsv.gz", sep="\t", compression="gzip", header=True, index=False)


def glue_norm_df_popu():
    """
    description:
        glues normaliced population level data together.
    """
    print(f"\nglue_norm_df_popu ...")
    for s_norm in {"log2scale","rrscale"}:
        df_glued_study = None

        # glue popu_stats
        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/popu_stats/")):
            if s_file.startswith(f"{s_study}-") and s_file.endswith(".tsv.gz"):
                s_statistic = s_file
                s_statistic = s_statistic.replace(f"{s_study}-", "")
                s_statistic = s_statistic.replace(".tsv.gz", "")
                print(f"glue_norm_df_popu process {s_file} ...")
                df_stats =  pd.read_csv(
                    f"./mema_extract_abs/{s_norm}/popu_stats/{s_file}",
                    sep="\t",
                    index_col=0,
                )
                if (df_glued_study is None):
                    df_glued_study = df_stats
                    #try:
                    df_glued_study.drop({"ecm_-sigma", "ecm_-sigmad", "ligand_run-sigma", "ligand_run-sigmad"}, axis=1, inplace=True)
                    #except KeyError:
                    #    pass
                else:
                    df_glued_study = pd.merge(
                        df_glued_study,
                        df_stats.loc[:, s_statistic],
                        left_index=True,
                        right_index=True,
                    )

        # glue utest
        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/popu_utest/")):
            if s_file.startswith(f"{s_study}-") and s_file.endswith(".tsv.gz"):
                s_statistic = s_file
                s_statistic = s_statistic.replace(f"{s_study}-", "")
                s_statistic = s_statistic.replace(".tsv.gz", "")
                print(f"glue_norm_df_popu process {s_file} ...")
                df_stats =  pd.read_csv(
                    f"./mema_extract_abs/{s_norm}/popu_utest/{s_file}",
                    sep="\t",
                    index_col=0,
                )
                df_stats.rename(
                    {
                        "u_statistics": f"{s_statistic}_statistics",
                        "p_value": f"{s_statistic}_p_value",
                    },
                    axis=1,
                    inplace=True,
                )
                df_glued_study = pd.merge(
                    df_glued_study,
                    df_stats.loc[
                        :,
                        [f"{s_statistic}_statistics", f"{s_statistic}_p_value"]
                    ],
                    left_index=True,
                    right_index=True,
                )

        # glue fnn entropy
        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/popu_fnn_entropy/")):
            if s_file.startswith(f"{s_study}-") and s_file.endswith(".tsv.gz"):
                s_statistic = s_file
                s_statistic = s_statistic.replace(f"{s_study}-", "")
                s_statistic = s_statistic.replace(".tsv.gz", "")
                print(f"glue_norm_df_popu process {s_file} ...")
                df_stats =  pd.read_csv(
                    f"./mema_extract_abs/{s_norm}/popu_fnn_entropy/{s_file}",
                    sep="\t",
                    index_col=0,
                )
                df_stats.rename(
                    {
                        "entropy_fnn_bit": f"{s_statistic}_bit",
                    },
                    axis=1,
                    inplace=True,
                )
                df_glued_study = pd.merge(
                    df_glued_study,
                    df_stats.loc[
                        :,
                        [f"{s_statistic}_bit"]
                    ],
                    left_index=True,
                    right_index=True,
                )

        # glue fnn divergence
        for s_file in sorted(os.listdir(f"./mema_extract_abs/{s_norm}/popu_fnn_divergence/")):
            if s_file.startswith(f"{s_study}-") and s_file.endswith(".tsv.gz"):
                s_statistic = s_file
                s_statistic = s_statistic.replace(f"{s_study}-", "")
                s_statistic = s_statistic.replace(".tsv.gz", "")
                print(f"glue_norm_df_popu process {s_file} ...")
                df_stats =  pd.read_csv(
                    f"./mema_extract_abs/{s_norm}/popu_fnn_divergence/{s_file}",
                    sep="\t",
                    index_col=0,
                )
                df_stats.rename(
                    {
                        "dkl_fnn_bit": f"{s_statistic}_bit",
                    },
                    axis=1,
                    inplace=True,
                )
                df_glued_study = pd.merge(
                    df_glued_study,
                    df_stats.loc[
                        :,
                        [f"{s_statistic}_bit"]
                    ],
                    left_index=True,
                    right_index=True,
                )

        # writet to file
        print(f"glue_norm_df_popu zipping result ...")
        df_glued_study.to_csv(f"{s_opath}{s_study}_df_{s_norm}_popu.tsv.gz", sep="\t", compression="gzip", header=True, index=False)


# main call
if __name__ == '__main__':

    # run dmc
    glue_segement_df_cell()
    #glue_norm_df_cell()
    #glue_norm_df_spot()
    #glue_norm_df_popu()
