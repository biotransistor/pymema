# libraries
import altair as alt
import matplotlib.pyplot as plt
import os
import pandas as pd
from pymema import run
from pysci import calx, hexa
from scipy import stats

# const
s_path_norm = "./data_mema/{}/mema_extract_{}/{}/"


# functions
def statistic_well_kde(s_study, es_norm={"abs"}):
    """
    matplotlib kdes
    plate centric
    """
    print("run statistic_well_kde")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in sorted(es_norm):
            if (s_norm != "4bue"):
                s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}"
                s_opath = f"{s_ipath}well_stats/px/"
                os.makedirs(s_opath, exist_ok=True)
                for s_ifile in sorted(os.listdir(s_ipath)):
                    if (s_ifile.endswith(f"-{s_norm}.tsv.gz")):
                        print(f"statistic_well processing: {s_ifile}")
                        _, s_endpoint, _, s_ext = s_ifile.split("-")

                        # plot
                        s_ofile = s_ifile.replace(".tsv.gz","-well_kde.png")
                        fig, ax = plt.subplots()
                        df_scstudy = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)
                        for s_ligand_run in df_scstudy.ligand_run.unique():
                            if (s_ligand_run.startswith("PBS")):
                                s_color = "k"
                                r_alpha = 1.0
                                r_linewidth = 1.2
                            else:
                                s_color = None
                                r_alpha = 0.3
                                r_linewidth = 1
                            se_analysis = df_scstudy.loc[df_scstudy.ligand_run.isin({s_ligand_run}), s_endpoint]
                            se_analysis.plot(
                                kind="kde",
                                #label=s_ligand_run,
                                #legend=True,
                                color=s_color,
                                alpha=r_alpha,
                                linewidth=r_linewidth,
                                figsize=(8,6),
                                ax=ax,
                            )

                        # save
                        ax.set_xlabel(f"{s_norm} log2(intensity)")
                        ax.set_title(f"{s_study}-{s_endpoint}-{s_trafo}-{s_norm}")
                        #ax.legend(loc="top left", bbox_to_anchor=(1, 1), fontsize="xx-small")
                        fig.tight_layout()
                        fig.savefig(f"{s_opath}{s_ofile}")
                        plt.close()
    # output
    return(True)


def statistic_population_probability(
        s_study,
        es_endpoint={"VIM_mean_cell","KRT14_mean_cell"},
        es_trafo={"log2scale","rrscale"},
        es_norm={'4bue_popu_dist','4bue_popu_dist_ok'}
    ):
    """
    plots distro from each treatment population
    this takes a fucking long time to run
    """
    print("run statistic_population_probability")
    for s_norm in es_norm:
        for s_trafo in es_trafo:
            s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}popu_dist/"
            s_opath = f"{s_ipath}px/"
            os.makedirs(s_opath, exist_ok=True)

            # loop through files
            for s_endpoint in es_endpoint:
                for s_ifile in sorted(os.listdir(s_ipath)):
                    if (s_ifile.startswith(f"{s_study}-{s_endpoint}-{s_trafo}-")) and (s_ifile.endswith(f"-{s_norm}.tsv.gz")):
                        print(f"statistic_population_probability processing: {s_ifile}")
                        _, _, _, s_perturbation, s_run, s_ext = s_ifile.split("-")
                        se_probability = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0, squeeze=True)

                        # plot
                        s_ofile = s_ifile.replace(".tsv.gz", ".png")
                        if not (os.path.isfile(f"{s_opath}{s_ofile}")):
                            s_title = f"{s_study}-{s_endpoint}-{s_trafo}-{s_perturbation}-{s_run}"
                            # altair
                            o_chart = hexa.distro_vega(se_probability, s_title=s_title)
                            o_chart.save(f"{s_opath}{s_ofile}")
                            # matplotlib
                            #fig = hexa.distro_plt(se_probability, s_title=s_title)
                            #fig.savefig(f"{s_opath}{s_ofile}")
    # output
    return(True)


def statistic_population(s_study, es_norm={"abs"}, tti_yx_significance=((0,0),(1,1),(2,2),(3,3),(4,4)), i_bin=2**7):
    """
    altair significance histograms and ecm ligand heatmaps
    study centric
    """
    print("run statistic_population")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in sorted(es_norm):
            if (s_norm != "4bue"):
                s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}"
                s_opath =f"{s_ipath}popu_stats/"
                os.makedirs(s_opath, exist_ok=True)
                for s_file in sorted(os.listdir(s_ipath)):
                    if (s_file.endswith(f"-{s_norm}.tsv.gz")):
                        print(f"statistic_population processing: {s_trafo} {s_file}")
                        _, s_endpoint, _, s_ext = s_file.split("-")

                        # load data
                        df_scstudy =  pd.read_csv(f"{s_ipath}{s_file}", sep="\t", index_col=0)

                        # calx
                        run.xyz_statistics(
                            df_scdata=df_scstudy,
                            s_collapse_unit="popu",
                            s_measurement=s_endpoint,
                            s_analysis_prefix=f"{s_endpoint}-{s_trafo}-{s_norm}",
                            s_y="ecm_",
                            s_x="ligand_run",
                            s_y_reference=None,
                            s_x_reference=run.s_REFERENCE_LIGAND,
                            s_y_column_reference=None,
                            s_x_column_reference="ligand",
                            tti_yx_significance=tti_yx_significance,
                            s_y_sort="ascending",
                            s_x_sort="ascending",
                            i_bin=i_bin,
                            s_title_prefix=f"{s_study}-",
                            s_opath=s_opath,
                        )

    # ouput
    return(True)


def statistic_population_size(s_study):
    """
    altair significance histograms and ecm ligand heatmaps
    study centric
    """
    print("run statistic_population_size")

    # all population per run
    s_ipath = f"{s_path_norm.format(s_study, '4bue_popu_dist', 'log2scale')}popu_size/"
    s_opath = f"{s_ipath}px/"
    s_ifile = f"{s_study}-Dapi_total_nuc-log2scale-4bue_popu_size.tsv.gz"
    os.makedirs(s_opath, exist_ok=True)
    # load data
    df_population = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

    # bar plot
    for s_run in sorted(df_population.run.unique()):
        print(f"plot population size for {s_run}")
        df_run = df_population.loc[df_population.run.isin({s_run}),:].reset_index()
        o_chart = alt.Chart(df_run).mark_bar().encode(
            x = alt.X(
                "perturbation_run",
                sort=alt.EncodingSortField(
                    field="Dapi_total_nuc-4bue_popu_size",
                    op="sum",
                    order="descending",
                ),
            ),
            y = "Dapi_total_nuc-4bue_popu_size",
        )
        s_ofile = f"{s_study}-{s_run}-Dapi_total_nuc-log2scale-4bue_popu_size_barchart.png"
        o_chart.save(f"{s_opath}{s_ofile}")

    # all population ecm ligand_run heatmap
    for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
        s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
        o_chart = run.xyzvega(
            df_xyz=df_population,
            s_z="Dapi_total_nuc-4bue_popu_size",
            s_y="ecm",
            s_x="ligand_run",
            s_y_significance="ecm-sigmad",
            s_x_significance="ligand-sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort="ascending",
            s_x_sort="ascending",
            s_title=f"treatment_population_size",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}{s_ofile}")

    # all ok population per study
    s_ipath = f"{s_path_norm.format(s_study, '4bue_popu_dist_ok', 'log2scale')}popu_size/"
    s_opath = f"{s_ipath}px/"
    s_ifile = f"{s_study}-Dapi_total_nuc-log2scale-4bue_popu_size_ok.tsv.gz"
    s_ofile = s_ifile.replace(".tsv.gz","_barchart.png")
    os.makedirs(s_opath, exist_ok=True)
    # load data
    df_population = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

    # bar plot
    print(f"plot study wide ok population size")
    o_chart = alt.Chart(df_population).mark_bar().encode(
        x = alt.X(
            "perturbation_run",
            sort=alt.EncodingSortField(
                field="Dapi_total_nuc-4bue_popu_size",
                op="sum",
                order="descending",
            ),
        ),
        y = "Dapi_total_nuc-4bue_popu_size",
    )
    o_chart.save(f"{s_opath}{s_ofile}")

    # all ok population ecm ligand_run heatmap
    for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
        s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
        o_chart = run.xyzvega(
            df_xyz=df_population,
            s_z="Dapi_total_nuc-4bue_popu_size",
            s_y="ecm",
            s_x="ligand_run",
            s_y_significance="ecm-sigmad",
            s_x_significance="ligand-sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort="ascending",
            s_x_sort="ascending",
            s_title=f"treatment_population_size",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}{s_ofile}")


def statistic_population_mode(s_study, es_norm={"4bue_popu_dist","4bue_popu_dist_ok"}):
    """
    altair significance histograms and ecm ligand heatmaps
    study centric
    """
    print("run statistic_population_mode")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in es_norm:
            s_mode = s_norm.replace("_dist","_mode")
            s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}mode/"
            s_opath = f"{s_ipath}px/"
            os.makedirs(s_opath, exist_ok=True)
            for s_ifile in sorted(os.listdir(s_ipath)):
                if (s_ifile.endswith(f"-{s_mode}.tsv.gz")):
                    print(f"statistic_population_mode processing: {s_ifile}")
                    _, s_endpoint, _, s_ext = s_ifile.split("-")
                    df_mode = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                    # plot
                    s_ofile = s_ifile.replace(".tsv.gz",".png")
                    s_title = f"{s_study}-{s_endpoint}-{s_trafo}-{s_mode}"
                    o_chart = run.xyzvega(
                        df_xyz=df_mode,
                        s_z="mode",
                        s_y="ecm_",
                        s_x="ligand_run",
                        s_y_significance="ecm-sigmad",
                        s_x_significance="ligand-sigmad",
                        i_y_significance_level=0,
                        i_x_significance_level=0,
                        s_ztype="ordinal",
                        s_y_sort="ascending",
                        s_x_sort="ascending",
                        s_title=s_title,
                    )
                    o_chart.save(f"{s_opath}{s_ofile}")
    # output
    return(True)


def statistic_population_entropy(s_study, s_extension="4bue_popu_entropy"):
    """
    altair significance histograms and ecm ligand heatmaps
    plate centric!
    """
    # 4bue_popu_entropy
    print("run statistic_population_entropy")
    for s_trafo in {"log2scale","rrscale"}:
        s_ipath = f"{s_path_norm.format(s_study, s_extension, s_trafo)}entropy/"
        s_opath = f"{s_ipath}px/"
        os.makedirs(s_opath, exist_ok=True)
        for s_ifile in sorted(os.listdir(s_ipath)):
            if (s_ifile.endswith("-4bue_popu_entropy.tsv.gz")):
                print(f"statistic_population_entropy processing: {s_ifile}")
                _, s_endpoint, _, s_ext = s_ifile.split("-")
                df_entropy = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                # plot altair heatmap
                for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
                    s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
                    o_chart = run.xyzvega(
                        df_xyz = df_entropy,
                        s_z = "entropy_bit",
                        s_y = "ecm_",
                        s_x = "ligand_run",
                        s_y_significance="ecm-sigmad",
                        s_x_significance="ligand-sigmad",
                        i_y_significance_level=i_y_sig,
                        i_x_significance_level=i_x_sig,
                        s_y_sort = "ascending",
                        s_x_sort = "ascending",
                        s_title = f"{s_study}-{s_endpoint}-{s_trafo}-entropy",
                    )
                    if (o_chart != None):
                        o_chart.save(f"{s_opath}{s_ofile}")

        # 4bue_popu_divergence
        s_ipath = f"{s_path_norm.format(s_study, s_extension, s_trafo)}divergence/"
        s_opath = f"{s_ipath}px/"
        os.makedirs(s_opath, exist_ok=True)
        for s_ifile in sorted(os.listdir(s_ipath)):
            if (s_ifile.endswith("-4bue_popu_divergence.tsv.gz")):
                print(f"statistic_population_divergence processing: {s_ifile}")
                _, s_endpoint, _, s_ext = s_ifile.split("-")
                df_divergence = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                for s_column in df_divergence.columns:
                    if (s_column.find("_bit")  > -1):
                        s_divergence = "_".join(s_column.split("_")[:-1])

                        # plot altair heatmap
                        # bue 20190404: this is a bug in the vega library:
                        if (df_divergence.loc[:,s_column].unique().shape[0] > 1 ):

                            for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
                                s_ofile = s_ifile.replace(".tsv.gz",f"-{s_divergence}-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
                                o_chart = run.xyzvega(
                                    df_xyz = df_divergence,
                                    s_z = s_column,
                                    s_y = "ecm_",
                                    s_x = "ligand_run",
                                    s_y_significance=f"{s_divergence}-sigmad",
                                    s_x_significance=f"{s_divergence}-sigmad",
                                    i_y_significance_level=i_y_sig,
                                    i_x_significance_level=i_x_sig,
                                    s_y_sort = "ascending",
                                    s_x_sort = "ascending",
                                    s_title = f"{s_study}-{s_endpoint}-{s_trafo}-{s_column}",
                                )
                                if (o_chart != None):
                                    o_chart.save(f"{s_opath}{s_ofile}")
                        else:
                            print(f"All {s_column} z values {df_divergence.loc[:,s_column].unique()} are the same! No xyzvega is plotted.")
    # output
    return(True)

def statistic_population_fnn(s_study, es_norm={"abs"}):
    """
    altair significance histograms and ecm ligand heatmaps
    plate centric!
    """
    print("run statistic_population_fnn")
    for s_norm in es_norm:
        for s_trafo in {"log2scale","rrscale"}:

            # fnn_entropy
            s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}popu_fnn_entropy/"
            s_opath = f"{s_ipath}px/"
            os.makedirs(s_opath, exist_ok=True)
            for s_ifile in sorted(os.listdir(s_ipath)):
                if (s_ifile.endswith("-fnn_entropy.tsv.gz")):
                    print(f"statistic_population_fnn processing: {s_ifile}")
                    _, s_endpoint, _, _, s_ext = s_ifile.split("-")
                    df_entropy = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                    # plot altair heatmap
                    for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
                        s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
                        s_title = f"{s_study}-{s_endpoint}-{s_trafo}-fnn_entropy"
                        o_chart = run.xyzvega(
                            df_xyz=df_entropy,
                            s_z=f"entropy_fnn_bit",
                            s_y="ecm_",
                            s_x="ligand_run",
                            s_y_significance="ecm-sigmad",
                            s_x_significance="ligand-sigmad",
                            i_y_significance_level=i_y_sig,
                            i_x_significance_level=i_x_sig,
                            s_y_sort="ascending",
                            s_x_sort="ascending",
                            s_title=s_title,
                        )
                        if (o_chart != None):
                            o_chart.save(f"{s_opath}{s_ofile}")

            # fnn_divergence
            s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}popu_fnn_divergence/"
            s_opath = f"{s_ipath}px/"
            os.makedirs(s_opath, exist_ok=True)
            for s_ifile in sorted(os.listdir(s_ipath)):
                if (s_ifile.endswith("-fnn_dkl.tsv.gz")):
                    print(f"statistic_population_fnn processing: {s_ifile}")
                    _, s_endpoint, _, _, s_ext = s_ifile.split("-")
                    df_divergence = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                    # plot altair heatmap
                    for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
                        s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
                        s_title = f"{s_study}-{s_endpoint}-{s_trafo}-fnn_Dkl"
                        o_chart = run.xyzvega(
                            df_xyz = df_divergence,
                            s_z="dkl_fnn_bit",
                            s_y="ecm_",
                            s_x="ligand_run",
                            s_y_significance="dkl_fnn-sigmad",
                            s_x_significance="dkl_fnn-sigmad",
                            i_y_significance_level=i_y_sig,
                            i_x_significance_level=i_x_sig,
                            s_y_sort="ascending",
                            s_x_sort="ascending",
                            s_title=s_title,
                        )
                        if (o_chart != None):
                            o_chart.save(f"{s_opath}{s_ofile}")
    # output
    return(True)


def statistic_population_utest(s_study, es_norm={"abs"}): # es_norm= {"raw","norm","abs","absm","4bue"}
    """
    altair significance histograms and ecm ligand heatmaps
    plate centric - as population against reference population is compared.
    """
    print("run statistic_population_utest")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in es_norm:
            s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}popu_utest/"
            s_opath = f"{s_ipath}px/"
            os.makedirs(s_opath, exist_ok=True)
            for s_ifile in sorted(os.listdir(s_ipath)):
                if (s_ifile.endswith("-utest.tsv.gz")):
                    print(f"statistic_population_utest processing: {s_trafo} {s_ifile}")
                    _, s_endpoint, _, _, s_ext = s_ifile.split("-")
                    df_utest = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)

                    # plot altair heatmap
                    # bue 20190404: this is a bug in the vega library:
                    if (df_utest.p_value.unique().shape[0] > 1 ):
                        # plot
                        for i_y_sig, i_x_sig in ((0,0), (1,1), (2,2), (3,3), (4,4)):
                            s_ofile = s_ifile.replace(".tsv.gz",f"-heatmap-sigma{i_y_sig}x{i_x_sig}.png")
                            o_chart = run.xyzvega(
                                df_xyz = df_utest,
                                s_z = "p_value",
                                s_y = "ecm_",
                                s_x = "ligand_run",
                                s_y_significance="utest-sigmad",
                                s_x_significance="utest-sigmad",
                                i_y_significance_level=i_y_sig,
                                i_x_significance_level=i_x_sig,
                                #s_z_sort = "descending",
                                s_y_sort = "ascending",
                                s_x_sort = "ascending",
                                s_title = f"{s_study}-{s_endpoint}-{s_trafo}-{s_norm}-u_test"
                            )
                            if (o_chart != None):
                                o_chart.save(f"{s_opath}{s_ofile}")
                    else:
                        print(f"All z values {df_utest.p_value.unique()} are the same! No xyzvega is plotted.")
    # output
    return(True)


def statistic_well(s_study, es_norm={"abs","4bue"}, i_bin=2**7, s_runtype="mema"):
    """
    input:
        s_study: string of study name (as used in the path structure).
        d_norm: dictionar of normalization dictionaries of endpoint dictionaries of run dictionaries datafarme.
    plate centric
    """
    print("run statistic_well")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in sorted(es_norm):
                s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}"
                s_opath =f"{s_ipath}coor_stats/"
                os.makedirs(s_opath, exist_ok=True)
                #print(f"input: {s_ipath}")
                #print(f"output: {s_opath}")
                for s_file in sorted(os.listdir(s_ipath)):
                    if (s_file.endswith(f"-{s_norm}.tsv.gz")):
                        print(f"statistic_well processing: {s_trafo} {s_file}")
                        _, s_endpoint, _, s_ext = s_file.split("-")

                        # load data
                        df_scstudy =  pd.read_csv(f"{s_ipath}{s_file}", sep="\t", index_col=0)
                        #print(df_scstudy.info())

                        # off we go
                        for s_run in df_scstudy.run.unique():
                            run.statistic_well(
                                df_scrun=df_scstudy.loc[df_scstudy.run.isin({s_run}),:],
                                s_study=s_study,
                                s_run=s_run,
                                s_endpoint=s_endpoint,
                                s_trafo=s_trafo,
                                s_norm=s_norm,
                                i_bin=i_bin,
                                s_runtype=s_runtype,
                            )
    # output
    return(True)


def statistic_spot(s_study, es_norm={"abs","4bue"}, i_bin=2**7, s_runtype="mema"):
    """
    input:
        s_study: string of study name (as used in the path structure).
        d_norm: dictionar of normalization dictionaries of endpoint dictionaries of run dictionaries datafarme.
    plate centric
    """
    print("run statistic_spot")
    for s_trafo in {"log2scale","rrscale"}:
        for s_norm in sorted(es_norm):
                s_ipath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}"
                s_opath =f"{s_ipath}coor_stats/"
                os.makedirs(s_opath, exist_ok=True)
                #print(f"input: {s_ipath}")
                #print(f"output: {s_opath}")
                for s_file in sorted(os.listdir(s_ipath)):
                    if (s_file.endswith(f"-{s_norm}.tsv.gz")):
                        print(f"statistic_spot processing: {s_trafo} {s_file}")
                        _, s_endpoint, _, s_ext = s_file.split("-")

                        # load data
                        df_scstudy =  pd.read_csv(f"{s_ipath}{s_file}", sep="\t", index_col=0)
                        #print(df_scstudy.info())

                        # off we go
                        for s_run in df_scstudy.run.unique():
                            run.statistic_spot(
                                df_scrun=df_scstudy.loc[df_scstudy.run.isin({s_run}),:],
                                s_study=s_study,
                                s_run=s_run,
                                s_endpoint=s_endpoint,
                                s_trafo=s_trafo,
                                s_norm=s_norm,
                                i_bin=i_bin,
                                s_runtype=s_runtype,
                            )
    # output
    return(True)
