# library
import csv
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pymema import run
import re

# r library
from rpy2 import robjects
robjects.r('library(rrscale)')

### set ###
s_runtype = 'well'
i_core = 4 # number of cores to use to run rrscale. default NULL.

### off we go ###
s_ipath = f'./{s_runtype}_extract_raw/'
s_opath = f'./{s_runtype}_extract_scale/rrscale/'
s_tpath = f'./{s_runtype}_extract_scale/rrscale/tmp/'

es_endpoint = set()
os.makedirs(s_opath, exist_ok=True)
os.makedirs(s_tpath, exist_ok=True)
df_lmbda_study = pd.DataFrame()
for s_file in sorted(os.listdir(s_ipath)):
    if (s_file.endswith('-raw.tsv.gz')):
        print(f'\nextract: {s_file}')

        # load data
        s_study, s_endpoint, s_ext= s_file.split('-')
        es_endpoint.add(s_endpoint)
        df_scraw = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)

        # shape dataframe
        # bue: CellIDPopu is not racked further.
        print(f'shape input dataframe')
        df_calx1 = df_scraw.loc[:,['perturbation_run', 'coor', 'CellID', s_endpoint]]
        df_calx1.loc[:,s_endpoint].replace({0:np.nan}, inplace=True)
        df_calx1['CellIDPopu'] = 0
        df_calx2 = pd.DataFrame()
        es_perturbation_run = set(df_calx1.perturbation_run)
        i_total = len(es_perturbation_run)
        for i, s_perturbation_run in enumerate(es_perturbation_run):
            print(f'process {i+1}/{i_total}: {s_perturbation_run}')
            df_popu = df_calx1.loc[df_calx1.perturbation_run.isin({s_perturbation_run}),:].copy()
            df_popu.loc[:,'CellIDPopu'] = df_popu.reset_index().index + 1
            df_calx2 = df_calx2.append(df_popu)
        se_calx3 = df_calx2.loc[:,s_endpoint]
        se_calx3.index = pd.MultiIndex.from_frame(df_calx2.loc[:,['CellIDPopu','perturbation_run']])
        df_calx3 = se_calx3.unstack()
        # write to file
        print(f'write shaped input file')
        s_tmp = s_file.replace('-raw.tsv.gz','-rrshape.csv.gz')
        df_calx3.to_csv(f'{s_tpath}{s_tmp}', compression='gzip')

        # run rrscale per treatment population
        print(f'load shaped input file into R')
        s_command = f'df_calx3 <- read.csv("{s_tpath}{s_tmp}", row.names="CellIDPopu")'
        robjects.r(s_command)
        print(f'run rrscale on treatment population')
        s_command = f'o_calx4 <- rrscale(df_calx3, ncores={i_core})'  # zeros=NA
        robjects.r(s_command)

        # get gaussianization method and lmbda values
        lr_lmbda =  robjects.r('o_calx4$pars')
        r_lmbda = np.median(lr_lmbda)

        ls_popu = list(df_calx3.columns)
        df_lmbda = pd.DataFrame(zip(ls_popu, lr_lmbda), columns=['perturbation_run','lmbda'])
        df_lmbda['function'] = robjects.r('o_calx4$T_name')[0]
        df_lmbda['endpoint'] = s_endpoint
        df_lmbda_study = df_lmbda_study.append(df_lmbda)

        # run rrscale on whole study with mean lambda
        print(f'run rrscale on study')
        s_command = f'df_scraw <- read.csv("{s_ipath}{s_file}", sep="\t", row.names="index")'
        robjects.r(s_command)
        s_command = f'df_calx5 <- o_calx4$rr_fn(Y=df_scraw["{s_endpoint}"], lambda={r_lmbda})'
        robjects.r(s_command)
        print(f'write result to file')
        s_tmp = s_file.replace('-raw.tsv.gz','-rrscale.csv')
        s_command = f'write.csv(df_calx5, "{s_tpath}{s_tmp}")'
        robjects.r(s_command)

        # fuse result
        print(f'fuse result')
        df_calx5 = pd.read_csv(f'{s_tpath}{s_tmp}', index_col=0)
        s_out = s_file.replace('-raw.tsv.gz','-rrscale-scale.tsv.gz')
        df_scrr = pd.merge(df_scraw.drop(s_endpoint, axis=1), df_calx5, left_index=True, right_index=True)
        df_scrr.index.name = 'index'
        df_scrr.to_csv(f'{s_opath}{s_out}',  sep='\t', compression='gzip')

# write lmbda values to file
print('\noutput lmbda')
df_lmbda_study = df_lmbda_study.loc[:,['endpoint','perturbation_run','function','lmbda']]
df_lmbda_study.sort_values(['endpoint','perturbation_run']).reset_index(inplace=True)
df_lmbda_study.index.name = 'index'
s_out = f'{s_study}-rrscale_lmbda.tsv.gz'
df_lmbda_study.to_csv(f'{s_opath}{s_out}', sep='\t', compression='gzip')


# plot lambda kde for each endpoint
s_study = os.getcwd().split('/')[-1]
df_lmbda_study = pd.read_csv(f'{s_opath}{s_study}-rrscale_lmbda.tsv.gz', sep='\t', index_col=0)
fig, ax = plt.subplots()
ls_legend = []
for s_endpoint in df_lmbda_study.endpoint.unique():
    df_lmbda = df_lmbda_study.loc[df_lmbda_study.endpoint.isin({s_endpoint}),['lmbda']]
    print(f'process: {s_endpoint} {df_lmbda.loc[:,"lmbda"].mean()}')
    df_lmbda.plot(
        kind='kde',
        xlim=(-4,4),
        ax=ax,
        #label=s_endpoint,
        #legend=False,
    )
    ls_legend.append(s_endpoint)
ax.legend(ls_legend)
ax.set_title(f'treatment popu lambda dist {set(df_lmbda_study.function.unique())}')
fig.tight_layout()
fig.savefig(f'{s_opath}/{s_study}-rrscale_lmbda.png')
plt.close()


# get ok cell filter
print('\nrun rrscale2filter')
es_index_ok = None
for s_endpoint in es_endpoint:
    s_pathfile = f'{s_opath}/{s_study}-{s_endpoint}-rrscale-scale.tsv.gz'
    if os.path.isfile(s_pathfile):
        print(f'rrscale2filter processing: {s_pathfile}')
        df_scdata = pd.read_csv(f'{s_pathfile}', sep='\t', index_col=0)
        es_index_scdata = set(df_scdata.loc[df_scdata.loc[:,s_endpoint].notna(),:].index)
        if (es_index_ok is None):
            es_index_ok = es_index_scdata
        else:
            es_index_ok = es_index_ok.intersection(es_index_scdata)
        print(f'numb ok index: {len(es_index_ok)}')
# output
with open(f'{s_opath}{s_study}-rrscale_index_ok.json', 'w') as f_json:
    json.dump(sorted(es_index_ok), f_json)
