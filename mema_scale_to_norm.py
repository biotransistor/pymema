####
# title: mema_raw2norm.py
#
# language: Python3
# license: GPLv>=3
# date: 2019-03-11
# author: bue
#
# run: python3 mema_raw2norm.py
#
# description:
#    code to transform a study of raw, Guillaume segmented mema expression data
#    a into PBS COL1 normalized data and more.
####

# library
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pymema import run
from pysci import calx, hexa
from scipy import stats
import shutil
import statistics
import sys


### set ###
s_runtype = 'well'
es_pertutype = {'ligand','drug'} #{'ecm','ligand','drug'}

es_mean_endpoint = {
    'nucleus_pixel',
    'cell_pixel',
    'cytoplasm_pixel',
    'CDH1_mean_cell', # added with ss-hlrh1
    'HER3_mean_cell', # added with dapiher3krt14met
    'KRT5_mean_cell', # added with ss-4
    'KRT14_mean_cell',
    'KRT19_mean_cell', # added with ss-o
    'MET_mean_cell', # added with dapiher3krt14met
    'VIM_mean_cell',
}

es_median_endpoint = {
    'Dapi_total_nuc',
    'EdU_total_nuc',
}

es_norm_perturbation_collapsed = {
    'COL1_go0005584+DMSO_chebi28262+PBS_pubchemcid24978514',
    'COL1_go0005584+PBS_pubchemcid24978514',
    'COL1_go0005584+PBS_pubchemcid24978514+trametinib_chebi75998',
    'DMSO+PBS',
}

s_ref_ligand = 'PBS'  # 'PBS_pubchemcid24978514'
#s_ref_drug = 'DMSO'

dr_highbreak = hexa.dr_HIGHBREAK_0_15  # dr_HIGHBREAK_0_8


### off we go ###
# const
s_study = os.getcwd().split('/')[-1]

s_path_abs = f'./{s_runtype}_extract_abs/'
s_path_absm = f'./{s_runtype}_extract_absm/'
s_path_annot = f'./{s_runtype}_ac/'
s_path_norm = f'./{s_runtype}_extract_norm/'
s_path_raw = f'./{s_runtype}_extract_raw/'
s_path_rrscale = f'./{s_runtype}_extract_scale/rrscale/'
s_path_log2scale = f'./{s_runtype}_extract_scale/log2scale/'
s_path_4bue = f'./{s_runtype}_extract_4bue/'
s_path_4bue_popu_dist = f'./{s_runtype}_extract_4bue_popu_dist/'
s_path_4bue_popu_dist_ok = f'./{s_runtype}_extract_4bue_popu_dist_ok/'
s_path_4bue_popu_entropy = f'./{s_runtype}_extract_4bue_popu_entropy/'

s_fpath_actsv = './{}_ac/{}-{}_dataframetsv_tidy_perturbation.tsv'

es_static_endpoint = set()

es_endpoint = es_static_endpoint.union(es_mean_endpoint).union(es_median_endpoint)

with open(f'{s_path_rrscale}{s_study}-rrscale_index_ok.json') as f_json:
    es_index_ok = set(json.load(f_json))

# function
def refpertu(es_pertutype, s_ref_ligand):
    # get reference perturbation
    ds_ref = {}
    for s_file in sorted(os.listdir(s_path_annot)):
        if (s_file.endswith('_dataframetsv_tidy_perturbation.tsv')):
            print(f'refpertu processing: {s_file}')

            # get run annotation
            df_actsv = run.perturbation_antsv2df(f'{s_path_annot}{s_file}')
            df_reference = df_actsv.loc[df_actsv.ligand.isin({s_ref_ligand}),:]
            print(df_reference.head())
            if (df_reference.shape[0] > 1):
                df_test = df_actsv.loc[- df_actsv.ligand.isin({s_ref_ligand}),:]

                # ligand drug
                if (es_pertutype == {'ligand','drug'}):
                    # get ecm ligand and drug set
                    es_ligand = set(df_actsv.ligand.unique())
                    es_ligand.remove(s_ref_ligand)
                    as_drug = df_actsv.drug.unique()

                    for s_drug in as_drug:
                        df_ref = df_reference.loc[df_reference.drug.isin({s_drug}),:]
                        as_ref = df_ref.perturbation_run.unique()
                        if (len(as_ref) != 1):
                            sys.exit(
                                f'Error @ acfetch : for this ecm {s_ecm} drug {s_drug} combination none or more then one {s_ref_ligand} reference perturbation_run found {as_ref}.'
                            )
                        else:
                            s_ref = as_ref[0]
                            # get test perturbation
                            for s_ligand in es_ligand:
                                df_t = df_test.loc[
                                    df_test.ligand.isin({s_ligand}),:].loc[
                                    df_test.drug.isin({s_drug}),:
                                ]
                                as_test = df_t.perturbation_run.unique()
                                s_test = as_test[0]
                                if (len(as_test) != 1):
                                    sys.exit(
                                        f'Error @ acfetch : for this ecm {s_ecm} drug {s_drug} ligand {s_ligand} combination none or more then one perturbation_run found {as_test} found.'
                                    )

                                # update output
                                ds_ref.update({s_test: s_ref})


                # ecm ligabd drug
                elif (es_pertutype == {'ecm','ligand','drug'}):

                    # get ecm ligand and drug set
                    as_ecm = df_actsv.ecm.unique()
                    es_ligand = set(df_actsv.ligand.unique())
                    es_ligand.remove(s_ref_ligand)
                    as_drug = df_actsv.drug.unique()

                    for s_drug in as_drug:
                        for s_ecm in as_ecm:
                            # get reference perturbation
                            df_ref = df_reference.loc[df_reference.ecm.isin({s_ecm}),:].loc[
                                df_reference.drug.isin({s_drug}),:
                            ]
                            as_ref = df_ref.perturbation_run.unique()
                            if (len(as_ref) != 1):
                                sys.exit(
                                    f'Error @ acfetch : for this ecm {s_ecm} drug {s_drug} combination none or more then one {s_ref_ligand} reference perturbation_run found {as_ref}.'
                                )
                            else:
                                s_ref = as_ref[0]
                                # get test perturbation
                                for s_ligand in es_ligand:
                                    df_t = df_test.loc[
                                        df_test.ecm.isin({s_ecm}),:].loc[
                                        df_test.ligand.isin({s_ligand}),:].loc[
                                        df_test.drug.isin({s_drug}),:
                                    ]
                                    as_test = df_t.perturbation_run.unique()
                                    s_test = as_test[0]
                                    if (len(as_test) != 1):
                                        sys.exit(
                                            f'Error @ acfetch : for this ecm {s_ecm} drug {s_drug} ligand {s_ligand} combination none or more then one perturbation_run found {as_test} found.'
                                        )

                                    # update output
                                    ds_ref.update({s_test: s_ref})

    # output to file
    with open(f'{s_path_annot}{s_study}-ref_perturbation_run.json', 'w') as f_json:
        json.dump(ds_ref, f_json, indent=4, sort_keys=True)



def scale2norm(
        s_study,
        es_index_ok=None,
        es_norm_perturbation_collapsed=es_norm_perturbation_collapsed,
    ):
    '''
    Each perturbation and endpoint is plate wise pbs mean or median normalized
    depends on the endpoint to enable across plate investigation.
    '''
    # off we go
    print('run scale2norm')
    for s_ipath, s_opath, s_trafo in (
            (s_path_log2scale, f'{s_path_norm}log2scale/', 'log2scale'),
            (s_path_rrscale,  f'{s_path_norm}rrscale/', 'rrscale'),
        ):
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f'{s_opath}px/', exist_ok=True)
        for s_endpoint in es_endpoint:
            s_file = f'{s_study}-{s_endpoint}-{s_trafo}-scale.tsv.gz'
            print(f'scale2norm processing: {s_file} ...')

            # load and filter study
            try:
                df_scstudy = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)
                if (es_index_ok != None):
                    df_scstudy = df_scstudy.loc[es_index_ok,:]

                # get normalization center
                if (s_endpoint in es_mean_endpoint):
                    df_equi = df_scstudy.loc[
                        df_scstudy.perturbation.isin(es_norm_perturbation_collapsed),
                        :
                    ]
                    se_center = df_equi.groupby('runid')[s_endpoint].mean()
                elif (s_endpoint in es_median_endpoint):
                    df_equi = df_scstudy.loc[
                        df_scstudy.perturbation.isin(es_norm_perturbation_collapsed),
                        :
                    ]
                    se_center = df_equi.groupby('runid')[s_endpoint].median()
                elif (s_endpoint in es_static_endpoint):
                    se_center = pd.Series(0, index=set(df_scstudy.runid))
                else:
                    sys.exit(f'Error @ scale2norm : endpoint {s_endpoint} does not belong to es_static_endpoint, es_mean_endpoint, or es_median_endpoint')

                # normalize
                df_norm = pd.DataFrame()
                for s_runid in df_scstudy.runid.unique():
                    df_scrun = df_scstudy.loc[df_scstudy.runid.isin({s_runid}),:].copy()
                    print(f'hallo: {s_runid} {se_center}')  # {df_scrun.head()}
                    try:
                        se_center.loc[s_runid]
                        b_calx = True
                    except KeyError:
                        # no center no normalization
                        b_calx = False
                        df_scrun = calx.df2sigma(
                            df_tidy=df_scrun,
                            s_value=s_endpoint,
                            s_reference=None,
                            s_column_reference=None,
                            s_prefix='',
                            i_ddof=1
                        )
                        # but significance plot ecm study is ok
                        fig = calx.sigma2hist(
                            df_scrun,
                            s_value=s_endpoint,
                            s_column_color='ecm-sigmad',
                            s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-norm',
                            i_bin=1024
                        )
                        s_out = f'{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-norm-sigmad.png'
                        fig.savefig(f'{s_opath}px/{s_out}')
                        plt.close()

                    if (b_calx):
                        # center
                        df_scrun.loc[:, s_endpoint] = df_scrun.loc[:, s_endpoint] - se_center.loc[s_runid]

                        # calx sigmas
                        df_scrun = calx.df2sigma(
                            df_tidy=df_scrun,
                            s_value=s_endpoint,
                            s_reference=None,
                            s_column_reference=None,
                            s_prefix='ecm-',
                            i_ddof=1
                        )
                        df_scrun = calx.df2sigma(
                            df_tidy=df_scrun,
                            s_value=s_endpoint,
                            s_reference=run.s_REFERENCE_LIGAND,
                            s_column_reference='ligand',
                            s_prefix='ligand-',
                            i_ddof=1
                        )
                        df_norm = df_norm.append(df_scrun)

                        # significance plot ecm run
                        fig = calx.sigma2hist(
                            df_scrun,
                            s_value=s_endpoint,
                            s_column_color='ecm-sigmad',
                            s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-norm',
                            i_bin=1024
                        )
                        s_out = f'{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-norm-ecm_sigmad.png'
                        fig.savefig(f'{s_opath}px/{s_out}')
                        plt.close()

                        # significance plot ligand run
                        fig = calx.sigma2hist(
                            df_scrun,
                            s_value=s_endpoint,
                            s_column_color='ligand-sigmad',
                            s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-norm',
                            i_bin=1024
                        )
                        s_out = f'{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-norm-ligand_sigmad.png'
                        fig.savefig(f'{s_opath}px/{s_out}')
                        plt.close()

                # write to file
                s_out = f'{s_study}-{s_endpoint}-{s_trafo}-norm.tsv.gz'
                df_norm.to_csv(f'{s_opath}{s_out}', sep='\t', compression='gzip')

            # not endpoint of this study
            except FileNotFoundError:
                pass

    # output
    return(True)


def norm2abs(s_study):
    print('run norm2abs')
    for s_ipath, s_opath_abs, s_opath_absm, s_trafo in (
            (f'{s_path_norm}log2scale/', f'{s_path_abs}log2scale/', f'{s_path_absm}log2scale/', 'log2scale'),
            (f'{s_path_norm}rrscale/', f'{s_path_abs}rrscale/', f'{s_path_absm}rrscale/', 'rrscale'),
        ):
        os.makedirs(s_opath_abs, exist_ok=True)
        os.makedirs(s_opath_absm, exist_ok=True)
        #os.makedirs(f'{s_opath_abs}px/', exist_ok=True)
        os.makedirs(f'{s_opath_absm}px/', exist_ok=True)
        for s_endpoint in es_endpoint:
            s_file = f'{s_study}-{s_endpoint}-{s_trafo}-norm.tsv.gz'
            print(f'norm2abs processing: {s_ipath}{s_file} ...')

            # load study
            try:
                df_study = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)

                # abs handling
                df_abs = df_study.copy()
                df_abs.loc[:,s_endpoint] = df_abs.loc[:,s_endpoint] + 4
                s_out = s_file.replace('-norm.tsv.gz','-abs.tsv.gz')
                df_abs.to_csv(f'{s_opath_abs}{s_out}', sep='\t', compression='gzip')

                # mirrored abs handling
                df_absm = df_abs.copy()
                df_absm.loc[:,s_endpoint].clip(lower=0, upper=8, inplace=True)
                s_out = s_file.replace('-norm.tsv.gz','-absm.tsv.gz')
                df_absm.to_csv(f'{s_opath_absm}{s_out}', sep='\t', compression='gzip')

                # significance plot study
                # bue 20190531: does not work because even in a study
                # different plates will have different sigmas

            # not endpoint of this study
            except FileNotFoundError:
                pass


def absm4bue(s_study):
    print('run absm4bue')
    for s_ipath, s_opath, s_trafo in (
            (f'{s_path_absm}log2scale/', f'{s_path_4bue}log2scale/', 'log2scale'),
            (f'{s_path_absm}rrscale/', f'{s_path_4bue}rrscale/', 'rrscale'),
        ):
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f'{s_opath}px/', exist_ok=True)
        for s_endpoint in es_mean_endpoint.union(es_median_endpoint):
            s_file = f'{s_study}-{s_endpoint}-{s_trafo}-absm.tsv.gz'
            print(f'absm4bue processing: {s_file} ...')

            # load mirrored absolute data and get bins
            try:
                df_study = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)
                ls_bin = []
                for r_real in df_study.loc[:, s_endpoint]:
                    for r_bin, s_hex in sorted(dr_highbreak.items()):
                        #print(r_real, r_bin, s_hex)
                        if (r_real < r_bin):
                            s_bin = s_hex
                            break
                    # output
                    ls_bin.append(s_bin)
                df_study.loc[:, s_endpoint] = ls_bin

                # write to file
                s_out = s_file.replace('-absm.tsv.gz', '-4bue.tsv.gz')
                df_study.to_csv(f'{s_opath}{s_out}', sep='\t', compression='gzip')

                # plot study wide endpoint probability distro
                se_probability = df_study.loc[:,s_endpoint].value_counts(normalize=True)
                for s_bin in hexa.es_BIN.difference(set(se_probability.index)):
                    se_probability[s_bin] = 0
                se_probability.sort_index(inplace=True)
                se_probability.index.name = 'index'
                s_out = s_file.replace('-absm.tsv.gz', '-4bue.png')
                # altair
                # bue 20191003: maybe it does not work because altair can not handle zero values
                #o_chart = hexa.distro_vega(se_probability, s_title=f'{s_study}-{s_endpoint}-{s_trafo}-4bue')
                #o_chart.save(f'{s_opath}px/{s_out}')
                # matplotlib
                fig = hexa.distro_plt(se_probability, s_title=f'{s_study}-{s_endpoint}-{s_trafo}-4bue')
                plt.savefig(f'{s_opath}px/{s_out}')
                plt.close()

            # not endpoint of this study
            except FileNotFoundError:
                pass


def bue4ppdist(s_study, i_min_population_size=2**10):
    '''
    population probability distribution
    '''
    print('run bue4ppdist')
    for s_ipath, s_opath, s_opath_ok, s_trafo in (
            (f'{s_path_4bue}log2scale/', f'{s_path_4bue_popu_dist}log2scale/', f'{s_path_4bue_popu_dist_ok}log2scale/', 'log2scale'),
            (f'{s_path_4bue}rrscale/', f'{s_path_4bue_popu_dist}rrscale/', f'{s_path_4bue_popu_dist_ok}rrscale/', 'rrscale'),
        ):
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f'{s_opath}mode/', exist_ok=True)
        os.makedirs(f'{s_opath}mode/px/', exist_ok=True)
        os.makedirs(f'{s_opath}popu_dist/', exist_ok=True)
        os.makedirs(f'{s_opath}sc/', exist_ok=True)
        os.makedirs(s_opath_ok, exist_ok=True)
        os.makedirs(f'{s_opath_ok}mode/', exist_ok=True)
        os.makedirs(f'{s_opath_ok}mode/px/', exist_ok=True)
        os.makedirs(f'{s_opath_ok}popu_dist/', exist_ok=True)
        os.makedirs(f'{s_opath_ok}sc/', exist_ok=True)

        for s_endpoint in es_mean_endpoint.union(es_median_endpoint):  #{'Dapi_total_nuc'}:
            s_file = f'{s_study}-{s_endpoint}-{s_trafo}-4bue.tsv.gz'
            print(f'\nbue4ppdist processing: {s_file} ...')

            # load 4bue data
            try:
                df_analysis = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)
                df_analysis = df_analysis.loc[:, run.ls_LABEL_SC + [s_endpoint]]

                # get popu annotation
                df_annot_popu = df_analysis.loc[
                    :,
                    run.ls_LABEL_POPU
                ].drop_duplicates()

                # get bins and add >= 0.001 prbability for each bin
                ls_label_defined = ['perturbation_run','coor','ImageID','CellID', s_endpoint]
                ll_entry = []
                i_total = len(df_analysis.perturbation_run.unique()) - 1
                for i, s_perturbation_run in enumerate(df_analysis.perturbation_run.unique()):
                    print(f'spice up probability {i}/{i_total} {s_trafo} {s_endpoint} {s_perturbation_run}')
                    df_analysis_perturbation = df_analysis.loc[df_analysis.perturbation_run.isin({s_perturbation_run}),:]
                    i_define = int(np.ceil(df_analysis_perturbation.shape[0] * 0.001))
                    for s_hexa in hexa.es_BIN:
                        l_entry = [s_perturbation_run, 0, 0, 0, s_hexa]
                        for _ in range(i_define):
                            ll_entry.append(l_entry)
                # glue
                df_defined = pd.DataFrame(ll_entry, columns=ls_label_defined)
                df_glue = pd.merge(df_annot_popu, df_defined, on='perturbation_run')
                df_glue['runid_coor'] = df_glue.apply(lambda n: f'{n.runid}-{n.coor}', axis=1)
                df_glue = df_glue.loc[:, run.ls_LABEL_SC + [s_endpoint]]
                df_analysis_defined = df_analysis.append(df_glue, ignore_index=True)
                df_analysis_defined.index = df_analysis_defined.apply(lambda n: f'{n.runid}-{n.coor}-{n.CellID}', axis=1)
                df_analysis_defined.index.name = 'index'

                # write to file
                print('write to file')
                s_out = s_file.replace('-4bue.tsv.gz', '-4bue_sc.tsv.gz')
                df_analysis_defined.to_csv(f'{s_opath}sc/{s_out}', sep='\t', compression='gzip')

                # get population size
                print('get popu size')
                s_analysis = f'{s_endpoint}-4bue_popu_size'
                df_population = df_analysis.loc[:,['perturbation_run', s_endpoint]].groupby('perturbation_run').count()
                df_population.rename({s_endpoint: s_analysis}, axis=1, inplace=True)
                df_population = pd.merge(df_annot_popu, df_population, on='perturbation_run')
                df_population.index = df_population.perturbation_run
                df_population.index.name = 'index'

                # calx sigma and sigmad
                df_population = calx.df2sigma(
                    df_tidy=df_population,
                    s_value=s_analysis,
                    s_reference=None,
                    s_column_reference=None,
                    s_prefix='ecm-',
                    i_ddof=1
                )
                df_population = calx.df2sigma(
                    df_tidy=df_population,
                    s_value=s_analysis,
                    s_reference=run.s_REFERENCE_LIGAND,
                    s_column_reference='ligand',
                    s_prefix='ligand-',
                    i_ddof=1
                )

                # get ok population size
                print('get ok population size')
                se_ok = df_population.loc[df_population.loc[:, s_analysis] > i_min_population_size, s_analysis]
                es_ok = set(se_ok.index.unique())
                df_analysis_defined_ok = df_analysis_defined.loc[df_analysis_defined.perturbation_run.isin(es_ok),:]
                s_outok = s_file.replace('-4bue.tsv.gz', '-4bue_sc_ok.tsv.gz')
                df_analysis_defined_ok.to_csv(f'{s_opath_ok}sc/{s_outok}', sep='\t', compression='gzip')

                # write to file population size
                if (s_endpoint == 'Dapi_total_nuc') and (s_trafo == 'log2scale'):
                    df_population_ok = df_population.loc[df_population.perturbation_run.isin(es_ok),:].copy()

                    # calx sigma and sigmad
                    df_population_ok = calx.df2sigma(
                        df_tidy=df_population_ok,
                        s_value=s_analysis,
                        s_reference=None,
                        s_column_reference=None,
                        s_prefix='ecm-',
                        i_ddof=1
                    )
                    df_population_ok = calx.df2sigma(
                        df_tidy=df_population_ok,
                        s_value=s_analysis,
                        s_reference=run.s_REFERENCE_LIGAND,
                        s_column_reference='ligand',
                        s_prefix='ligand-',
                        i_ddof=1
                    )

                    os.makedirs(f'{s_opath}popu_size/', exist_ok=True)
                    s_out = s_file.replace('-4bue.tsv.gz', '-4bue_popu_size.tsv.gz')
                    df_population.to_csv(f'{s_opath}popu_size/{s_out}', sep='\t', compression='gzip')

                    os.makedirs(f'{s_opath_ok}popu_size/', exist_ok=True)
                    s_outok = s_file.replace('-4bue.tsv.gz', '-4bue_popu_size_ok.tsv.gz')
                    df_population_ok.to_csv(f'{s_opath_ok}popu_size/{s_outok}', sep='\t', compression='gzip')

                # get probability distribution and mode and write to file
                print('get treatment population probability and mode')
                d_mode = {}
                d_mode_ok = {}

                for s_perturbation_run in df_analysis_defined.perturbation_run.unique():

                    # get population
                    a_population = df_analysis_defined.loc[
                        df_analysis_defined.perturbation_run.isin({s_perturbation_run}),
                        s_endpoint
                    ].values

                    # get treatment population probability distribution and write to file
                    se_probability = hexa.sample2pmf(
                        a_population,
                        s_density_function='probability',
                    )
                    s_out = s_file.replace('-4bue.tsv.gz', f'-{s_perturbation_run}-4bue_popu_dist.tsv.gz')
                    se_probability.to_csv(f'{s_opath}/popu_dist/{s_out}', header=True, sep='\t', compression='gzip')

                    # get mode
                    try:
                        s_mode = statistics.mode(a_population)
                    except statistics.StatisticsError:
                        s_mode = 'abc'
                    d_mode.update({s_perturbation_run: s_mode})

                    # handle ok cases
                    if (s_perturbation_run in es_ok):
                        # treatment population probability
                        s_outok = s_file.replace('-4bue.tsv.gz', f'-{s_perturbation_run.replace(" ","")}-4bue_popu_dist_ok.tsv.gz')
                        shutil.copy(f'{s_opath}popu_dist/{s_out}', f'{s_opath_ok}popu_dist/{s_outok}')
                        # mode
                        d_mode_ok.update({s_perturbation_run: s_mode})

                # handle mode
                se_mode = pd.Series(d_mode, name='mode')
                se_mode.index.name = 'perturbation_run'
                df_mode = pd.merge(df_annot_popu, se_mode.reset_index(), on='perturbation_run')
                df_mode.index = df_mode.perturbation_run
                df_mode.sort_index(inplace=True)
                df_mode.index.name = f'{s_study}-{s_endpoint}'
                df_mode['bin'] = df_mode.loc[:,'mode'].apply(lambda n: hexa.di_BIN[n])

                # calx sigma and sigmad
                df_mode= calx.df2sigma(
                    df_tidy=df_mode,
                    s_value='bin',
                    s_reference=None,
                    s_column_reference=None,
                    s_prefix='ecm-',
                    i_ddof=1
                )
                df_mode = calx.df2sigma(
                    df_tidy=df_mode,
                    s_value='bin',
                    s_reference=run.s_REFERENCE_LIGAND,
                    s_column_reference='ligand',
                    s_prefix='ligand-',
                    i_ddof=1
                )

                s_out = s_file.replace('-4bue.tsv.gz', '-4bue_popu_mode.tsv.gz')
                df_mode.to_csv(f'{s_opath}mode/{s_out}', sep='\t', compression='gzip')

                # significance plot mode
                fig = calx.sigma2hist(
                    df_mode,
                    s_value='bin',
                    s_column_color='ecm-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode',
                    i_bin=64
                )
                fig.savefig(f'{s_opath}mode/px/{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode-ecm_sigmad.png')
                plt.close()

                fig = calx.sigma2hist(
                    df_mode,
                    s_value='bin',
                    s_column_color='ligand-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode',
                    i_bin=64
                )
                fig.savefig(f'{s_opath}mode/px/{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode-ligand_sigmad.png')
                plt.close()

                # handle mode ok
                se_mode_ok = pd.Series(d_mode_ok, name='mode')
                se_mode_ok.index.name = 'perturbation_run'
                df_mode_ok = pd.merge(df_annot_popu, se_mode_ok.reset_index(), on='perturbation_run')
                df_mode_ok.index = df_mode_ok.perturbation_run
                df_mode_ok.sort_index(inplace=True)
                df_mode_ok.index.name = f'{s_study}-{s_endpoint}'
                df_mode_ok['bin'] = df_mode_ok.loc[:,'mode'].apply(lambda n: hexa.di_BIN[n])

                # calx sigma and sigmad
                df_mode_ok = calx.df2sigma(
                    df_tidy=df_mode_ok,
                    s_value='bin',
                    s_reference=None,
                    s_column_reference=None,
                    s_prefix='ecm-',
                    i_ddof=1
                )
                df_mode_ok = calx.df2sigma(
                    df_tidy=df_mode_ok,
                    s_value='bin',
                    s_reference=run.s_REFERENCE_LIGAND,
                    s_column_reference='ligand',
                    s_prefix='ligand-',
                    i_ddof=1
                )

                s_outok = s_file.replace('-4bue.tsv.gz', '-4bue_popu_mode_ok.tsv.gz')
                df_mode_ok.to_csv(f'{s_opath_ok}mode/{s_outok}', sep='\t', compression='gzip')

                # significance plot mode_ok
                fig = calx.sigma2hist(
                    df_mode_ok,
                    s_value='bin',
                    s_column_color='ecm-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode_ok',
                    i_bin=64
                )
                fig.savefig(f'{s_opath_ok}mode/px/{s_study}-{s_endpoint}-4bue_popu_mode-ecm_sigmad.png')
                plt.close()

                fig = calx.sigma2hist(
                    df_mode_ok,
                    s_value='bin',
                    s_column_color='ligand-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-4bue_popu_mode_ok',
                    i_bin=64
                )
                fig.savefig(f'{s_opath_ok}mode/px/{s_study}-{s_endpoint}-4bue_popu_mode-ligand_sigmad.png')
                plt.close()

            # not endpoint of this study
            except FileNotFoundError:
                pass


def ppdist2entropy():
    print('run ppdist2entropy')
    for s_ipath, s_opath, s_trafo in (
            (f'{s_path_4bue_popu_dist_ok}log2scale/', f'{s_path_4bue_popu_entropy}log2scale/', 'log2scale'),
            (f'{s_path_4bue_popu_dist_ok}rrscale/', f'{s_path_4bue_popu_entropy}rrscale/', 'rrscale'),
        ):
        os.makedirs(f'{s_opath}entropy/', exist_ok=True)
        os.makedirs(f'{s_opath}entropy/px/', exist_ok=True)

        # get entropy
        print(f'ppdist2entropy calx entropy ...')
        ddr_entropy = {}
        es_run = set()
        for s_file in sorted(os.listdir(f'{s_ipath}popu_dist/')):
            if (s_file.endswith('-4bue_popu_dist_ok.tsv.gz')):
                # load data
                s_study, s_endpoint, s_trafo, s_perturbation, s_run, s_ext = s_file.split('-')
                es_run.add(s_run)
                df_probability = pd.read_csv(f'{s_ipath}popu_dist/{s_file}', sep='\t', index_col=0)

                # calculate probability
                r_h = calx.entropy_marginal(df_probability.probability.values)

                # update output
                try:
                    dr_entropy = ddr_entropy[s_endpoint]
                    dr_entropy.update({f'{s_perturbation}-{s_run}': r_h})
                except KeyError:
                    dr_entropy = {f'{s_perturbation}-{s_run}': r_h}
                ddr_entropy.update({s_endpoint: dr_entropy})

        # get annotation
        print(f'ppdist2entropy get population annotation ...')
        df_annot_popu = pd.DataFrame()
        for s_run in es_run:
            df_annot = run.perturbation_antsv2df(s_fpath_actsv.format(s_runtype, s_runtype, s_run))
            df_annot_popu = df_annot_popu.append(
                df_annot.loc[
                    :,
                    run.ls_LABEL_POPU
                ].drop_duplicates()
            )

        # merge
        for s_endpoint, dr_entropy in ddr_entropy.items():
            print(f'ppdist2entropy write {s_trafo} {s_endpoint} entropy to file ...')
            df_entropy = pd.DataFrame()
            se_entropy = pd.Series(dr_entropy, name='entropy_bit')
            se_entropy.index.name = 'perturbation_run'
            df_calx = pd.merge(df_annot_popu, se_entropy.reset_index(), on='perturbation_run')
            for s_runid in df_calx.runid.unique():

                # calx sigmas
                df_run = df_calx.loc[df_calx.runid.isin({s_runid}),:].copy()

                # calx sigma and sigmad
                #df_run = calx.df2sigma(df_tidy=df_run, s_value='entropy_bit', i_ddof=1)
                df_run = calx.df2sigma(
                    df_tidy=df_run,
                    s_value='entropy_bit',
                    s_reference=None,
                    s_column_reference=None,
                    s_prefix='ecm-',
                    i_ddof=1
                )
                df_run = calx.df2sigma(
                    df_tidy=df_run,
                    s_value='entropy_bit',
                    s_reference=run.s_REFERENCE_LIGAND,
                    s_column_reference='ligand',
                    s_prefix='ligand-',
                    i_ddof=1
                )
                df_entropy = df_entropy.append(df_run)

                # significance plot ecm run wide
                fig = calx.sigma2hist(
                    df_run,
                    s_value='entropy_bit',
                    s_column_color='ecm-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-4bue_popu_entropy',
                    i_bin=16
                )
                s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-4bue_popu_entropy-ecm_sigmad.png'
                fig.savefig(f'{s_opath}entropy/px/{s_out}')
                plt.close()

                # significance plot ligand run wide
                fig = calx.sigma2hist(
                    df_run,
                    s_value='entropy_bit',
                    s_column_color='ligand-sigmad',
                    s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-4bue_popu_entropy',
                    i_bin=16
                )
                s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-4bue_popu_entropy-ligand_sigmad.png'
                fig.savefig(f'{s_opath}entropy/px/{s_out}')
                plt.close()

            # write to file
            df_entropy.index = df_entropy.perturbation_run
            df_entropy.sort_index(inplace=True)
            df_entropy.index.name = f'{s_study}-{s_endpoint}'
            s_out = f'{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_entropy.tsv.gz'
            df_entropy.to_csv(f'{s_opath}entropy/{s_out}', sep='\t', compression='gzip')

            # significance plot study wide
            # bue 20190531: does not work because even in a study
            # different plates will have different sigmas


def ppdist2divergence():
    '''
    '''
    print('run ppdist2divergence')
    for s_ipath, s_opath, s_trafo in (
            (f'{s_path_4bue_popu_dist_ok}log2scale/', f'{s_path_4bue_popu_entropy}log2scale/', 'log2scale'),
            (f'{s_path_4bue_popu_dist_ok}rrscale/', f'{s_path_4bue_popu_entropy}rrscale/', 'rrscale'),
        ):
        os.makedirs(f'{s_opath}divergence/', exist_ok=True)
        os.makedirs(f'{s_opath}divergence/px/', exist_ok=True)
        b_get_mapping = True

        # calx divergence
        ddf_divergence = {} # kullback leibler and other divergence
        es_run = set()
        for s_test_file in sorted(os.listdir(f'{s_ipath}popu_dist/')):
            if (s_test_file.endswith('-4bue_popu_dist_ok.tsv.gz')):
                print(f'ppdist2divergence processing: {s_test_file} ...')
                # get test perturbation
                s_study, s_endpoint, s_trafo, s_test_pertu, s_run, s_ext = s_test_file.split('-')
                s_test_pertu_run = f'{s_test_pertu}-{s_run}'
                es_run.add(s_run)

                if (b_get_mapping):
                    # load reference test perturbation mapping
                    with open(f'{s_path_annot}{s_study}-ref_perturbation_run.json') as f_json:
                        ds_test2ref_perturbation = json.load(f_json)
                    b_get_mapping = False

                try:
                    # get reference perturbation
                    s_ref_pertu_run = ds_test2ref_perturbation[s_test_pertu_run]
                    s_ref_file = f'{s_study}-{s_endpoint}-{s_trafo}-{s_ref_pertu_run}-4bue_popu_dist_ok.tsv.gz'

                    # load data files
                    #print(f'load test file: {s_ipath}popu_dist/{s_test_file}')
                    df_test_probability = pd.read_csv(f'{s_ipath}popu_dist/{s_test_file}', sep='\t', index_col=0)
                    #print(f'load ref file: {s_ipath}popu_dist/{s_ref_file}')
                    df_ref_probability = pd.read_csv(f'{s_ipath}popu_dist/{s_ref_file}', sep='\t', index_col=0)

                    # calculate divergences (in this case same as entropy_distance)
                    r_hxy_joint = calx.entropy_joint(
                        ar_xfrequency=df_test_probability.probability.values,
                        ar_yfrequency=df_ref_probability.probability.values
                    )
                    r_hyx_conditional = calx.entropy_conditional(
                        ar_xfrequency=df_test_probability.probability.values,
                        ar_yfrequency=df_ref_probability.probability.values
                    )
                    # r_mi = calx.mutualinformation
                    r_dkl = calx.divergence_kullbackleibler(
                        ar_pfrequency=df_test_probability.probability.values,
                        ar_qfrequency=df_ref_probability.probability.values
                    )
                    r_ad = calx.absolute_difference(
                        ar_p=df_test_probability.probability.values,
                        ar_q=df_ref_probability.probability.values
                    )
                    r_ed = calx.euclidean_distanced(
                        ar_p=df_test_probability.probability.values,
                        ar_q=df_ref_probability.probability.values
                    )
                    r_sed = calx.squared_euclidean_distanced(
                        ar_p=df_test_probability.probability.values,
                        ar_q=df_ref_probability.probability.values
                    )

                    # pack output
                    se_entry = pd.Series(
                        [
                            s_test_pertu_run,
                            s_ref_pertu_run,
                            r_hxy_joint,
                            r_hyx_conditional,
                            r_dkl,
                            r_ad,
                            r_ed,
                            r_sed
                        ],
                        index=[
                            'perturbation_run',
                            'ref_perturbation',
                            'entropy_joint_bit',
                            'entropy_conditional_bit',
                            'dkl_bit',
                            'absd_bit',
                            'ed_bit',
                            'sed_bit**2',
                        ]
                    )
                    try:
                        df_divergence = ddf_divergence[s_endpoint]
                    except KeyError:
                        df_divergence = pd.DataFrame()
                    df_divergence = df_divergence.append(se_entry.to_frame().T)
                    ddf_divergence.update({s_endpoint: df_divergence})
                    #print(f'0k.')

                except KeyError:
                    #print(f'Reference perturbation.')
                    pass

                except FileNotFoundError:
                    #print(f'No ok reference file found.')
                    pass

        # get annotation
        df_annot_popu = pd.DataFrame()
        for s_run in es_run:
            # load annotation
            df_annot = run.perturbation_antsv2df(s_fpath_actsv.format(s_runtype, s_runtype, s_run))
            df_annot_popu = df_annot_popu.append(
                df_annot.loc[
                    :,
                    run.ls_LABEL_POPU
                ].drop_duplicates()
            )

        for s_endpoint, df_study in ddf_divergence.items():
            # re-type
            df_study = df_study.astype({
                'entropy_joint_bit': float,
                'entropy_conditional_bit': float,
                'dkl_bit': float,
                'absd_bit': float,
                'ed_bit': float,
                'sed_bit**2': float,
            })
            # merge
            #print(df_annot_popu.info())
            #print(df_calx.info())
            df_divergence = pd.DataFrame()
            df_calx = pd.merge(df_annot_popu, df_study, on='perturbation_run')
            for s_runid in df_calx.runid.unique():
                # calx sigmas
                df_run = df_calx.loc[df_calx.runid.isin({s_runid}),:].copy()
                for s_divergence in ['entropy_joint_bit', 'entropy_conditional_bit', 'dkl_bit', 'absd_bit', 'ed_bit', 'sed_bit**2']:
                    s_prefix = '_'.join(s_divergence.split('_')[:-1])
                    df_run = calx.df2sigma(
                        df_tidy=df_run,
                        s_value=s_divergence,
                        s_reference=None,
                        s_column_reference=None,
                        s_prefix=f'{s_prefix}-',
                        b_center=False,
                        i_ddof=1
                    )
                    # significance plot run wide
                    fig = calx.sigma2hist(
                        df_run,
                        s_value=s_divergence,
                        s_column_color=f'{s_prefix}-sigmad',
                        s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-4bue_popu_divergence-{s_divergence}',
                        i_bin=16
                    )
                    s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-4bue_popu_divergence-{s_prefix}_sigmad.png'
                    fig.savefig(f'{s_opath}divergence/px/{s_out}')
                    plt.close()

                # update study output
                df_divergence = df_divergence.append(df_run)

            # write to file
            df_divergence.index = df_divergence.perturbation_run
            df_divergence.sort_index(inplace=True)
            df_divergence.index.name = f'{s_study}-{s_endpoint}'
            s_out = f'{s_study}-{s_endpoint}-{s_trafo}-4bue_popu_divergence.tsv.gz'
            df_divergence.to_csv(f'{s_opath}divergence/{s_out}', sep='\t', compression='gzip')

            # study wide significance plot
            # bue 20190531: does not work because even in a study
            # different plates will have different sigmas


def utest(
        es_continuous = {'abs'}, #{'raw','norm','abs','absm'},
        es_discrete = {'4bue'},
        i_min_population_size = 2**10,
    ):
    '''
    '''
    # off we go
    print('run utest')
    b_get_mapping = True
    for s_norm in es_discrete.union(es_continuous):
        print(f"bue: {s_norm}")
        for s_path, s_trafo in (
                (f'./{s_runtype}_extract_{s_norm}/log2scale/', 'log2scale'),
                (f'./{s_runtype}_extract_{s_norm}/rrscale/', 'rrscale'),
            ):
            print(f"bue: {s_path} {s_trafo}")
            os.makedirs(f'{s_path}popu_utest/', exist_ok=True)
            os.makedirs(f'{s_path}popu_utest/px/', exist_ok=True)
            if (s_norm in es_discrete):
                b_continuity = False
            else:
                b_continuity = True

            print('bue', s_path, os.listdir(s_path))
            for s_ifile in sorted(os.listdir(s_path)):
                if (s_ifile.endswith(f'-{s_norm}.tsv.gz')):
                    print(f'utest processing: {s_ifile} ...')
                    s_study, s_endpoint, _, s_ext = s_ifile.split('-')

                    # load reference test perturbation mapping
                    if (b_get_mapping):
                        with open(f'{s_path_annot}{s_study}-ref_perturbation_run.json') as f_json:
                            ds_test2ref_perturbation = json.load(f_json)
                        b_get_mapping = False

                    # load data file
                    df_scdata = pd.read_csv(f'{s_path}{s_ifile}', sep='\t', index_col=0)

                    # off we go
                    df_utest = pd.DataFrame()
                    es_test_pertu_run = set(df_scdata.loc[:,'perturbation_run'].unique()).intersection(set(ds_test2ref_perturbation.keys()))
                    #print(es_test_pertu_run)
                    for s_test_pertu_run in es_test_pertu_run:

                        # filter data
                        s_ref_pertu_run = ds_test2ref_perturbation[s_test_pertu_run]
                        se_ref = df_scdata.loc[df_scdata.perturbation_run.isin({s_ref_pertu_run}), s_endpoint]
                        se_test = df_scdata.loc[df_scdata.perturbation_run.isin({s_test_pertu_run}), s_endpoint]

                        # bue: according to wikipedia, for an ok test the population should be > 20
                        # to make it compatible with the divergence analysis I have to set the population size to the same size as popu_ok.
                        if (se_ref.values.shape[0] > i_min_population_size) and (se_test.values.shape[0] > i_min_population_size):
                            # calx
                            try:
                                r_ustatistics, r_pvalue = stats.mannwhitneyu(
                                    x = se_test.values,
                                    y = se_ref.values,
                                    use_continuity = b_continuity,
                                    alternative = 'two-sided',
                                )

                                # pack output
                                se_entry = pd.Series(
                                    [
                                        s_test_pertu_run,
                                        s_ref_pertu_run,
                                        r_ustatistics,
                                        r_pvalue,
                                    ],
                                    index=[
                                        'perturbation_run',
                                        'ref_perturbation',
                                        'u_statistics',
                                        'p_value',
                                    ]
                                )
                                df_utest = df_utest.append(se_entry.to_frame().T)
                                #print(f'0k.')

                            except ValueError:
                                print(f'ValueWarning at ref {s_ref_perturbation} test {s_test_perturbation}: All numbers are identical in mannwhitneyu')
                        else:
                            #print(f'equal or less then i_min_population_size samples in reference {se_ref.values.shape[0]} or test {se_test.values.shape[0]} perturbation population.')
                            pass

                    # get annotation
                    print('get annotation')
                    df_annot_popu = df_scdata.loc[
                        :,
                        run.ls_LABEL_POPU
                    ].drop_duplicates()

                    # merge
                    print(f'merge {s_endpoint} utest result and annotation and write to file.')
                    #print(df_annot_popu.info())
                    #print(df_utest.info())
                    df_utest = pd.merge(df_annot_popu, df_utest, on='perturbation_run')
                    df_utest.index = df_utest.perturbation_run
                    df_utest.sort_index(inplace=True)
                    df_utest.index.name = f'{s_study}-{s_endpoint}'

                    # transform pvalues into sigmas
                    df_utest = calx.pvalue2sigma(
                        df_tidy=df_utest,
                        s_value='p_value',
                        b_twosided=True,
                        s_prefix='utest-'
                    )

                    # write to file
                    s_ofile = s_ifile.replace('.tsv.gz','-utest.tsv.gz')
                    df_utest.to_csv(f'{s_path}popu_utest/{s_ofile}', sep='\t', compression='gzip')

                    # significance plot
                    fig = calx.sigma2hist(
                        df_utest,
                        s_value='p_value',
                        s_column_color=f'utest-sigmad',
                        s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-{s_norm}_utest',
                        i_bin=32
                    )
                    s_out = f'{s_study}-{s_endpoint}-{s_trafo}-{s_norm}-utest_sigmad.png'
                    fig.savefig(f'{s_path}popu_utest/px/{s_out}')
                    plt.close()


# main call
if __name__ == '__main__':

    # run dmc
    refpertu(es_pertutype=es_pertutype, s_ref_ligand=s_ref_ligand)
    scale2norm(s_study=s_study, es_index_ok=es_index_ok, es_norm_perturbation_collapsed=es_norm_perturbation_collapsed)
    norm2abs(s_study=s_study)
    absm4bue(s_study=s_study)
    bue4ppdist(s_study)
    ppdist2entropy()
    ppdist2divergence()
    utest(es_continuous={'abs'}, es_discrete = {'4bue'})
