import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pysci import hexa, calx
from pymema import run
import shutil
import statistics


# const
s_path_4bue = f"./mema_extract_4bue/"
s_path_4bue_joint = f"./mema_extract_4bue_joint/"
s_path_4bue_popu_dist_joint = f"./mema_extract_4bue_popu_dist_joint/"
s_path_4bue_popu_dist_ok = "./mema_extract_4bue_popu_dist_ok/"
s_path_4bue_popu_dist_ok_joint = f"./mema_extract_4bue_popu_dist_ok_joint/"
s_path_4bue_popu_entropy_joint = f"./mema_extract_4bue_popu_entropy_joint/"

s_path_annot = "./mema_ac/"
s_fpath_actsv = "./mema_ac/{}_tidy_perturbation.tsv.gz"

# function
def bin2bin_joint(s_ipath, s_opath, s_study, s_trafo, es_endpoint, i_fuse=1):
    """
    input:
        s_ipath: path to the marginal 4bue data files.
        s_opath: path where the joint 4bue data files should be written.
        s_study: study folder name.
        s_trafo: data scale. particularely log2scale or rrscale.
        es_endpoint: set of endpoints which should be joined.
        i_fuse: interger to define if adjacent bins should be fused
          to keep the number of joint bins in range.

    output:
        joint 4bue data files.

    description:
        transforms marginal 4bue data files into joint 4bue data files.
    """
    ls_endpoint = sorted(es_endpoint, reverse=True)
    print(f"join_bin processing: endpoint {ls_endpoint}, fuse: {i_fuse}, scale: {s_trafo}")

    # get d_trafo
    d_trafo = hexa.bin_fusion_dict(sorted(hexa.es_BIN), i_fuse=i_fuse)

    # transform input values
    b_annot = True
    for s_endpoint in ls_endpoint:
        df_4bue =  pd.read_csv(
            f"{s_ipath}{s_study}-{s_endpoint}-{s_trafo}-4bue.tsv.gz",
            sep="\t",
            index_col=0,
        )
        if (b_annot):
            df_joint = df_4bue.loc[:,run.ls_LABEL_SC]  # annot
            b_annot = False
        se_4bue = df_4bue[s_endpoint].apply(lambda n: d_trafo[n])
        df_joint = pd.merge(df_joint, se_4bue.to_frame(), left_index=True, right_index=True)
    # get oint value and write to file
    df_joint["-".join(ls_endpoint)] = df_joint.loc[:,ls_endpoint].apply(lambda n: "-".join(n), axis=1)
    os.makedirs(s_opath, exist_ok=True)
    df_joint.to_csv(
        f"{s_opath}{s_study}-{'_'.join(ls_endpoint)}_fusion{i_fuse}-{s_trafo}-4bue.tsv.gz",
        sep="\t"
    )


def bue4_joint(s_study, es_endpoint={"KRT14_mean_cell", "VIM_mean_cell"}, ei_fuse={1,2,4}):
    """
    input:
        s_study: study folder name.
        es_endpoint: set of endpoints which should be joined.
        i_fuse: interger to define if adjacent bins should be fused
          to keep the number of joint bins in range.

    output:
        joint 4bue data files.

    description:
        transforms marginal 4bue data files into joint 4bue data files.
    """

    # off we go
    print("run bue4_joint!")
    for i_fuse in ei_fuse:
        for s_ipath, s_opath, s_trafo in (
                (f"{s_path_4bue}log2scale/", f"{s_path_4bue_joint}log2scale/", "log2scale"),
                (f"{s_path_4bue}rrscale/", f"{s_path_4bue_joint}rrscale/", "rrscale"),
            ):
            bin2bin_joint(
                s_ipath=s_ipath,
                s_opath=s_opath,
                s_study=s_study,
                s_trafo=s_trafo,
                es_endpoint=es_endpoint,
                i_fuse=i_fuse,
            )


def bue4ppdist_joint(s_study,  es_endpoint={"KRT14_mean_cell", "VIM_mean_cell"}, ei_fuse={1,2,4}, i_min_population_size=2**10):
    """
    input:
        s_study: study folder name.
        es_endpoint: set of endpoints which should be joined.
        i_fuse: interger to define if adjacent bins should be fused
          to keep the number of joint bins in range.
        i_min_population_size: minimal population to count as ok for entropy calcualtion.
    output:
        joint 4bue data files.

    description:
        transforms joint 4bue data files into joint probability data files.
    """
    print("run bue4ppdist_joint!")
    s_endpoint = "-".join(sorted(es_endpoint, reverse=True))

    # get ok
    print("bue4ppdist_joint get ok population")
    s_file = f"{s_study}-Dapi_total_nuc-log2scale-4bue_popu_size_ok.tsv.gz"
    df_ok_popu = pd.read_csv(f"{s_path_4bue_popu_dist_ok}log2scale/popu_size/{s_file}", sep="\t", index_col=0)
    es_ok = set(df_ok_popu.index.unique())

    # off we go
    for s_ipath, s_opath, s_opath_ok, s_trafo in (
            (f"{s_path_4bue_joint}log2scale/", f"{s_path_4bue_popu_dist_joint}log2scale/", f"{s_path_4bue_popu_dist_ok_joint}log2scale/", "log2scale"),
            (f"{s_path_4bue_joint}rrscale/", f"{s_path_4bue_popu_dist_joint}rrscale/", f"{s_path_4bue_popu_dist_ok_joint}rrscale/", "rrscale"),
        ):
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f"{s_opath}mode/", exist_ok=True)
        os.makedirs(f"{s_opath}popu_dist/", exist_ok=True)
        os.makedirs(f"{s_opath}sc/", exist_ok=True)
        os.makedirs(s_opath_ok, exist_ok=True)
        os.makedirs(f"{s_opath_ok}mode/", exist_ok=True)
        os.makedirs(f"{s_opath_ok}popu_dist/", exist_ok=True)
        os.makedirs(f"{s_opath_ok}sc/", exist_ok=True)

        for i_fuse in ei_fuse:

            # get ls_bin_joint
            print("bue4ppdist_joint get bin joint")
            es_input = s_endpoint.split("-")
            ls_bin_joint = hexa.get_bin_joint(sorted(hexa.es_BIN), i_fuse=i_fuse, i_input=len(es_input))

            # load 4bue data
            s_fusion = s_endpoint.replace("-","_") + f"_fusion{i_fuse}"
            s_file = f"{s_study}-{s_fusion}-{s_trafo}-4bue.tsv.gz"
            print(f"bue4ppdist_joint processing: {s_file} ...")
            df_analysis = pd.read_csv(f"{s_ipath}{s_file}", sep="\t", index_col=0)
            df_analysis = df_analysis.loc[:, run.ls_LABEL_SC + [s_endpoint]]

            # get popu annotation
            df_annot_popu = df_analysis.loc[
                :,
                run.ls_LABEL_POPU
            ].drop_duplicates()

            # get bins and add >= 0.001 prbability for each bin
            ls_label_defined = ["perturbation_run","coor","ImageID","CellID", s_endpoint]
            ll_entry = []
            i_total = len(df_analysis.perturbation_run.unique()) - 1
            for i, s_perturbation_run in enumerate(df_analysis.perturbation_run.unique()):
                print(f"bue4ppdist_joint spice up probability {i}/{i_total} {s_endpoint} {i_fuse} {s_trafo} {s_perturbation_run}")
                df_analysis_perturbation = df_analysis.loc[df_analysis.perturbation_run.isin({s_perturbation_run}),:]
                i_define = int(np.ceil(df_analysis_perturbation.shape[0] * 0.001))
                for s_hexa in sorted(ls_bin_joint):
                    l_entry = [s_perturbation_run, 0, 0, 0, s_hexa]
                    for _ in range(i_define):
                        ll_entry.append(l_entry)

            # glue annotation
            df_defined = pd.DataFrame(ll_entry, columns=ls_label_defined)
            df_glue = pd.merge(df_annot_popu, df_defined, on="perturbation_run")
            df_glue["runid_coor"] = df_glue.apply(lambda n: f"{n.runid}-{n.coor}", axis=1)
            df_glue = df_glue.loc[:, run.ls_LABEL_SC + [s_endpoint]]
            df_analysis_defined = df_analysis.append(df_glue, ignore_index=True)
            df_analysis_defined.index = df_analysis_defined.apply(lambda n: f"{n.runid}-{n.coor}-{n.CellID}", axis=1)
            df_analysis_defined.index.name = "index"

            # write to file
            print("bue4ppdist_joint write to file")
            s_out = s_file.replace("-4bue.tsv.gz", "-4bue_sc.tsv.gz")
            df_analysis_defined.to_csv(f"{s_opath}sc/{s_out}", sep="\t", compression="gzip")

            # get ok population size
            df_analysis_defined_ok = df_analysis_defined.loc[df_analysis_defined.perturbation_run.isin(es_ok),:]
            s_out = s_file.replace("-4bue.tsv.gz", "-4bue_sc.tsv.gz")
            df_analysis_defined.to_csv(f"{s_opath_ok}sc/{s_out}", sep="\t", compression="gzip")

            # get probability distribution and mode and write to file
            print("bue4ppdist_joint get treatment population probability and mode")
            d_mode = {}
            d_mode_ok = {}

            for s_perturbation_run in df_analysis_defined.perturbation_run.unique():

                # get population
                a_population = df_analysis_defined.loc[
                    df_analysis_defined.perturbation_run.isin({s_perturbation_run}),
                    s_endpoint
                ].values

                # get treatment population probability distribution and write to file
                se_probability = hexa.sample2pmf(
                    a_population,
                    es_bin =set(ls_bin_joint),
                    s_density_function="probability",
                )
                s_out = s_file.replace("-4bue.tsv.gz", f"-{s_perturbation_run}-4bue_popu_dist.tsv.gz")
                se_probability.to_csv(f"{s_opath}/popu_dist/{s_out}", header=True, sep="\t", compression="gzip")

                # get mode
                try:
                    s_mode = statistics.mode(a_population)
                except statistics.StatisticsError:
                    s_mode = "abc"
                d_mode.update({s_perturbation_run: s_mode})

                # handle ok cases
                if (s_perturbation_run in es_ok):
                    # treatment population probability
                    s_outok = s_file.replace("-4bue.tsv.gz", f"-{s_perturbation_run.replace(' ','')}-4bue_popu_dist_ok.tsv.gz")
                    shutil.copy(f"{s_opath}popu_dist/{s_out}", f"{s_opath_ok}popu_dist/{s_outok}")
                    # mode
                    d_mode_ok.update({s_perturbation_run: s_mode})


            # handle mode
            # bue: sigma only worx for marginal
            se_mode = pd.Series(d_mode, name="mode")
            se_mode.index.name = "perturbation_run"
            df_mode = pd.merge(df_annot_popu, se_mode.reset_index(), on="perturbation_run")
            df_mode.index = df_mode.perturbation_run
            df_mode.sort_index(inplace=True)
            df_mode.index.name = f"{s_study}-{s_endpoint}"
            s_out = s_file.replace("-4bue.tsv.gz", "-4bue_popu_mode.tsv.gz")
            df_mode.to_csv(f"{s_opath}mode/{s_out}", sep="\t", compression="gzip")

            # handle mode ok
            # bue: sigma only worx for marginal
            se_mode_ok = pd.Series(d_mode_ok, name="mode")
            se_mode_ok.index.name = "perturbation_run"
            df_mode_ok = pd.merge(df_annot_popu, se_mode_ok.reset_index(), on="perturbation_run")
            df_mode_ok.index = df_mode_ok.perturbation_run
            df_mode_ok.sort_index(inplace=True)
            df_mode_ok.index.name = f"{s_study}-{s_endpoint}"
            s_outok = s_file.replace("-4bue.tsv.gz", "-4bue_popu_mode_ok.tsv.gz")
            df_mode_ok.to_csv(f"{s_opath_ok}mode/{s_outok}", sep="\t", compression="gzip")


def ppdist2entropy_joint(s_study, es_endpoint={"KRT14_mean_cell","VIM_mean_cell"}, ei_fuse={1,2,4}):
    """
    input:
        s_study: study folder name.
        es_endpoint: set of endpoints which should be joined.
        i_fuse: interger to define if adjacent bins should be fused
          to keep the number of joint bins in range.
    output:
        joint entropy files.

    description:
        transforms joint  probaility data files into joint entropy data files.
    """
    print("run ppdist2entropy_joint!")
    s_endpoint = "-".join(sorted(es_endpoint, reverse=True))
    for s_ipath, s_opath, s_trafo in (
            (f"{s_path_4bue_popu_dist_ok_joint}log2scale/", f"{s_path_4bue_popu_entropy_joint}log2scale/", "log2scale"),
            (f"{s_path_4bue_popu_dist_ok_joint}rrscale/", f"{s_path_4bue_popu_entropy_joint}rrscale/", "rrscale"),
        ):
        os.makedirs(f"{s_opath}entropy/", exist_ok=True)
        os.makedirs(f"{s_opath}entropy/px/", exist_ok=True)

        # get entropy
        for i_fuse in ei_fuse:
            s_analysis = s_endpoint.replace("-","_") +  f"_fusion{i_fuse}"
            print(f"ppdist2entropy_joint processing: {s_analysis} {s_trafo}")
            ddr_entropy = {}
            es_run = set()
            print("ppdist2entropy_joint load popudist files and calcualte entropy ...")
            for s_file in sorted(os.listdir(f"{s_ipath}popu_dist/")):
                if (s_file.startswith(f"{s_study}-{s_analysis}-{s_trafo}")) and (s_file.endswith("-4bue_popu_dist_ok.tsv.gz")):
                    # load data
                    _, _, _, s_perturbation, s_run, s_ext = s_file.split("-")
                    es_run.add(s_run)
                    df_probability = pd.read_csv(f"{s_ipath}popu_dist/{s_file}", sep="\t", index_col=0)

                    # calculate probability
                    r_h = calx.entropy_marginal(df_probability.probability.values)

                    # update output
                    try:
                        dr_entropy = ddr_entropy[s_endpoint]
                        dr_entropy.update({f"{s_perturbation}-{s_run}": r_h})
                    except KeyError:
                        dr_entropy = {f"{s_perturbation}-{s_run}": r_h}
                    ddr_entropy.update({s_endpoint: dr_entropy})

            # get annotation
            print(f"ppdist2entropy_joint get population annotation ...")
            df_annot_popu = pd.DataFrame()
            for s_run in es_run:
                df_annot = run.perturbation_antsv2df(s_fpath_actsv.format(s_run))
                df_annot_popu = df_annot_popu.append(
                    df_annot.loc[
                        :,
                        run.ls_LABEL_POPU
                    ].drop_duplicates()
                )

            # merge
            print(f"ppdist2entropy_joint fuse result and write to file ...")
            for s_endpoint, dr_entropy in ddr_entropy.items():
                df_entropy = pd.DataFrame()
                se_entropy = pd.Series(dr_entropy, name="entropy_bit")
                se_entropy.index.name = "perturbation_run"
                df_calx = pd.merge(df_annot_popu, se_entropy.reset_index(), on="perturbation_run")
                for s_runid in df_calx.runid.unique():

                    # calx sigmas
                    df_run = df_calx.loc[df_calx.runid.isin({s_runid}),:].copy()

                    # calx sigma and sigmad
                    df_run = calx.df2sigma(
                        df_tidy=df_run,
                        s_value="entropy_bit",
                        s_reference=None,
                        s_column_reference=None,
                        s_prefix="ecm-",
                        i_ddof=1
                    )
                    df_run = calx.df2sigma(
                        df_tidy=df_run,
                        s_value="entropy_bit",
                        s_reference=run.s_REFERENCE_LIGAND,
                        s_column_reference="ligand",
                        s_prefix="ligand-",
                        i_ddof=1
                    )
                    df_entropy = df_entropy.append(df_run)

                    # significance plot ecm run wide
                    fig = calx.sigma2hist(
                        df_run,
                        s_value="entropy_bit",
                        s_column_color="ecm-sigmad",
                        s_xaxis_postfix=f" {s_study}-{s_runid}-{s_analysis}-{s_trafo}-4bue_popu_entropy",
                        i_bin=16
                    )
                    s_out = f"{s_study}-{s_runid.split('-')[-1]}-{s_analysis}-{s_trafo}-4bue_popu_entropy-ecm_sigmad.png"
                    fig.savefig(f"{s_opath}entropy/px/{s_out}")
                    plt.close()

                    # significance plot ligand run wide
                    fig = calx.sigma2hist(
                        df_run,
                        s_value="entropy_bit",
                        s_column_color="ligand-sigmad",
                        s_xaxis_postfix=f" {s_study}-{s_runid}-{s_analysis}-{s_trafo}-4bue_popu_entropy",
                        i_bin=16
                    )
                    s_out = f"{s_study}-{s_runid.split('-')[-1]}-{s_analysis}-{s_trafo}-4bue_popu_entropy-ligand_sigmad.png"
                    fig.savefig(f"{s_opath}entropy/px/{s_out}")
                    plt.close()

                # write to file
                df_entropy.index = df_entropy.perturbation_run
                df_entropy.sort_index(inplace=True)
                df_entropy.index.name = f"{s_study}-{s_analysis}"
                s_out = f"{s_study}-{s_analysis}-{s_trafo}-4bue_popu_entropy.tsv.gz"
                df_entropy.to_csv(f"{s_opath}entropy/{s_out}", sep="\t", compression="gzip")


def ppdist2divergence_joint(s_study, es_endpoint={"KRT14_mean_cell","VIM_mean_cell"}, ei_fuse={1,2,4}):  
    """
    input:
        s_study: study folder name.
        es_endpoint: set of endpoints which should be joined.
        i_fuse: interger to define if adjacent bins should be fused
          to keep the number of joint bins in range.
    output:
        joint entropy files.

    description:
        transforms joint probaility data files into joint divergence data files.
    """
    print("run ppdist2divergence_joint!")
    s_endpoint = "-".join(sorted(es_endpoint, reverse=True))
    for s_ipath, s_opath, s_trafo in (
            (f"{s_path_4bue_popu_dist_ok_joint}log2scale/", f"{s_path_4bue_popu_entropy_joint}log2scale/", "log2scale"),
            (f"{s_path_4bue_popu_dist_ok_joint}rrscale/", f"{s_path_4bue_popu_entropy_joint}rrscale/", "rrscale"),
        ):
        os.makedirs(f"{s_opath}divergence/", exist_ok=True)
        os.makedirs(f"{s_opath}divergence/px/", exist_ok=True)
        b_get_mapping = True

        # calx divergence
        for i_fuse in ei_fuse:
            s_analysis = s_endpoint.replace("-","_") +  f"_fusion{i_fuse}"
            print(f"ppdist2divergence_joint processing: {s_analysis} {s_trafo} ...")
            ddf_divergence = {} # kullback leibler and other divergence
            es_run = set()
            print(f"ppdist2divergence_joint calx ...")
            for s_test_file in sorted(os.listdir(f"{s_ipath}popu_dist/")):
                if  (s_test_file.startswith(f"{s_study}-{s_analysis}-{s_trafo}")) and (s_test_file.endswith("-4bue_popu_dist_ok.tsv.gz")):
                    # get test perturbation
                    _, _, _, s_test_pertu, s_run, s_ext = s_test_file.split("-")
                    s_test_pertu_run = f"{s_test_pertu}-{s_run}"
                    es_run.add(s_run)

                    if (b_get_mapping):
                        # load reference test perturbation mapping
                        with open(f"{s_path_annot}{s_study}-ref_perturbation_run.json") as f_json:
                            ds_test2ref_perturbation = json.load(f_json)
                        b_get_mapping = False

                    try:
                        # get reference perturbation
                        s_ref_pertu_run = ds_test2ref_perturbation[s_test_pertu_run]
                        s_ref_file = f"{s_study}-{s_analysis}-{s_trafo}-{s_ref_pertu_run}-4bue_popu_dist_ok.tsv.gz"

                        # load data files
                        #print(f"load test file: {s_ipath}popu_dist/{s_test_file}")
                        df_test_probability = pd.read_csv(f"{s_ipath}popu_dist/{s_test_file}", sep="\t", index_col=0)
                        #print(f"load ref file: {s_ipath}popu_dist/{s_ref_file}")
                        df_ref_probability = pd.read_csv(f"{s_ipath}popu_dist/{s_ref_file}", sep="\t", index_col=0)

                        # calculate divergences (in this case same as entropy_distance)
                        r_hxy_joint = calx.entropy_joint(
                            ar_xfrequency=df_test_probability.probability.values,
                            ar_yfrequency=df_ref_probability.probability.values
                        )
                        r_hyx_conditional = calx.entropy_conditional(
                            ar_xfrequency=df_test_probability.probability.values,
                            ar_yfrequency=df_ref_probability.probability.values
                        )
                        # r_mi = calx.mutualinformation
                        r_dkl = calx.divergence_kullbackleibler(
                            ar_pfrequency=df_test_probability.probability.values,
                            ar_qfrequency=df_ref_probability.probability.values
                        )
                        r_ad = calx.absolute_difference(
                            ar_p=df_test_probability.probability.values,
                            ar_q=df_ref_probability.probability.values
                        )
                        r_ed = calx.euclidean_distanced(
                            ar_p=df_test_probability.probability.values,
                            ar_q=df_ref_probability.probability.values
                        )
                        r_sed = calx.squared_euclidean_distanced(
                            ar_p=df_test_probability.probability.values,
                            ar_q=df_ref_probability.probability.values
                        )

                        # pack output
                        se_entry = pd.Series(
                            [
                                s_test_pertu_run,
                                s_ref_pertu_run,
                                r_hxy_joint,
                                r_hyx_conditional,
                                r_dkl,
                                r_ad,
                                r_ed,
                                r_sed
                            ],
                            index=[
                                "perturbation_run",
                                "ref_perturbation",
                                "entropy_joint_bit",
                                "entropy_conditional_bit",
                                "dkl_bit",
                                "absd_bit",
                                "ed_bit",
                                "sed_bit**2",
                            ]
                        )
                        # update output
                        try:
                            df_divergence = ddf_divergence[s_endpoint]
                        except KeyError:
                            df_divergence = pd.DataFrame()
                        df_divergence = df_divergence.append(se_entry.to_frame().T)
                        ddf_divergence.update({s_endpoint: df_divergence})
                        #print(f"0k.")

                    except KeyError:
                        #print(f"Reference perturbation.")
                        pass

                    except FileNotFoundError:
                        #print(f"No ok reference file found.")
                        pass

            # get annotation
            print(f"ppdist2divergence_joint get population annotation")
            df_annot_popu = pd.DataFrame()
            for s_run in es_run:
                # load annotation
                df_annot = run.perturbation_antsv2df(s_fpath_actsv.format(s_run))
                df_annot_popu = df_annot_popu.append(
                    df_annot.loc[
                        :,
                        run.ls_LABEL_POPU
                    ].drop_duplicates()
                )

            # pack result
            print(f"ppdist2divergence_joint fuse result and write to file ...")
            for s_endpoint, df_study in ddf_divergence.items():
                # re-type
                df_study = df_study.astype({
                    "entropy_joint_bit": float,
                    "entropy_conditional_bit": float,
                    "dkl_bit": float,
                    "absd_bit": float,
                    "ed_bit": float,
                    "sed_bit**2": float,
                })
                # merge
                #print(df_annot_popu.info())
                #print(df_calx.info())
                df_divergence = pd.DataFrame()
                df_calx = pd.merge(df_annot_popu, df_study, on="perturbation_run")
                for s_runid in df_calx.runid.unique():
                    # calx sigmas
                    df_run = df_calx.loc[df_calx.runid.isin({s_runid}),:].copy()
                    for s_divergence in ["entropy_joint_bit", "entropy_conditional_bit", "dkl_bit", "absd_bit", "ed_bit", "sed_bit**2"]:
                        s_prefix = "_".join(s_divergence.split("_")[:-1])
                        df_run = calx.df2sigma(
                            df_tidy=df_run,
                            s_value=s_divergence,
                            s_reference=None,
                            s_column_reference=None,
                            s_prefix=f"{s_prefix}-",
                            b_center=False,
                            i_ddof=1
                        )
                        # significance plot run wide
                        fig = calx.sigma2hist(
                            df_run,
                            s_value=s_divergence,
                            s_column_color=f"{s_prefix}-sigmad",
                            s_xaxis_postfix=f" {s_study}-{s_runid}-{s_analysis}-{s_trafo}-4bue_popu_divergence-{s_divergence}",
                            i_bin=16
                        )
                        s_out = f"{s_study}-{s_runid.split('-')[-1]}-{s_analysis}-{s_trafo}-4bue_popu_divergence-{s_prefix}_sigmad.png"
                        fig.savefig(f"{s_opath}divergence/px/{s_out}")
                        plt.close()

                    # update study output
                    df_divergence = df_divergence.append(df_run)

                # write to file
                df_divergence.index = df_divergence.perturbation_run
                df_divergence.sort_index(inplace=True)
                df_divergence.index.name = f"{s_study}-{s_analysis}"
                s_out = f"{s_study}-{s_analysis}-{s_trafo}-4bue_popu_divergence.tsv.gz"
                df_divergence.to_csv(f"{s_opath}divergence/{s_out}", sep="\t", compression="gzip")


# run
s_study = os.getcwd().split("/")[-1]
es_endpoint = {"KRT14_mean_cell", "KRT19_mean_cell", "VIM_mean_cell"}
ei_fuse = {1,2,4}

bue4_joint(
    s_study=s_study,
    es_endpoint=es_endpoint,
    ei_fuse=ei_fuse,
)

bue4ppdist_joint(
    s_study=s_study,
    es_endpoint=es_endpoint,
    ei_fuse=ei_fuse,
    i_min_population_size=2**10
)


ppdist2entropy_joint(
    s_study=s_study,
    es_endpoint=es_endpoint,
    ei_fuse=ei_fuse,
)

ppdist2divergence_joint(
    s_study=s_study,
    es_endpoint=es_endpoint,
    ei_fuse=ei_fuse,
)
