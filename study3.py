import os
import pandas as pd
import sys

# const
ls_endpoint = ["nucleus_pixel","cytoplasm_pixel","cell_pixel","Dapi_total_nuc","EdU_total_nuc","KRT14_mean_cell","VIM_mean_cell"]

# function
def study_result_fusion(
        es_study,
        s_sigmad0x0count0="sigmad1x1count3",
    ):
    """
    """
    # off we go
    ls_study = sorted(es_study)
    for s_endpoint in ls_endpoint:
        for s_axis in {"ecm", "ligand"}:
            for i, s_study in enumerate(ls_study):
                print(f"processing: {s_endpoint} {s_axis} {s_study} {i}")
                # load data
                s_ipath = f"dataMema/{s_study}/mema_result_{s_sigmad0x0count0}/"
                s_ifile = f"{s_study}-{s_endpoint}-result_{s_axis}_{s_sigmad0x0count0}.tsv.gz"
                df_load = pd.read_csv(f"{s_ipath}{s_ifile}", sep="\t", index_col=0)
                if (s_axis == "ligand"):
                    df_load.index = [n.split('-')[0] for n in df_load.index]
                    df_load = df_load.loc[~ df_load.index.isin({'PBS'}),:]
                # fuse
                if (i > 0):
                    df_fusion = pd.DataFrame()
                    es_hit = set(df_output.index.unique()).intersection(set(df_load.index.unique()))
                    for s_hit in sorted(es_hit):
                        se_fusion = (df_load.loc[s_hit,:] + df_output.loc[s_hit,:]) / 2
                        df_fusion = df_fusion.append(se_fusion)
                    df_output = df_fusion
                else:
                    df_output = df_load

            # write to file
            print(df_output.shape)
            #sys.exit()
            s_opath = f"dataMema/mema_result_{'_'.join(ls_study)}"
            s_ofile = f"{'_'.join(ls_study)}-result_{s_endpoint}-{s_axis}_{s_sigmad0x0count0}.tsv.gz"
            #print(s_opath, s_ofile)
            #print(df_output.info())
            os.makedirs(s_opath, exist_ok=True)
            df_output.to_csv(f"{s_opath}/{s_ofile}", sep="\t", compression="gzip")
