# libraries
import acpipe_acjson.acjson as ac
import altair as alt
import copy
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pysci import calx, hexa
from scipy import stats
import sys

# const
s_path_norm = "./data_mema/{}/mema_extract_{}/{}/"

ls_LABEL_SC = ["runid","runid_well", "runid_coor","run","well","coor","perturbation","perturbation_run","ecm","ecm_","ligand","ligand_run","drug","drug_","ImageID","CellID"]
ls_LABEL_COOR = ["runid","runid_coor","run","coor","x","y","perturbation","perturbation_run","ecm","ecm_","ligand","ligand_run","drug","drug_","ImageID"]
ls_LABEL_WELL = ["runid","runid_well","run","well","x","y","perturbation","perturbation_run","ecm","ecm_","ligand","ligand_run","drug","drug_"]
ls_LABEL_POPU = ["runid","run","perturbation","perturbation_run","ecm","ecm_","ligand","ligand_run","drug","drug_"]

s_REFERENCE_LIGAND = "PBS_pubchemcid24978514"

# functions
def perturbation_antsv2df(s_actsv_perturbation_tidy):
    """
    input:
        s_actsv_perturbation_tidy: string. path and tidy ac perturbation tsv file.
        ds_replace: correction dictionary for df.replace function.
    output:
        df_annot: datafame with columns: coor, perturbation, ecm, ligand, and drug.

    description:
        code to transform a tidy ac perturbation tsv file into the
        mema assay common  ecm ligand drug perturbation structure.
    """
    # load tidy assay coordinate tsv annotation file
    s_run = s_actsv_perturbation_tidy.split('/')[-1].split('_')[0].split('-')[-1]
    df_tidy_perturbation = pd.read_csv(s_actsv_perturbation_tidy, sep="\t")#, index_col=0)

    # coordinate
    df_pertu_axis = df_tidy_perturbation.loc[:,["runid","iWell","coor","perturbation-axisCollapsed"]].drop_duplicates()
    df_pertu_axis.rename({"iWell":"well", "perturbation-axisCollapsed": "perturbation"}, axis=1, inplace=True)
    df_pertu_axis["runid_coor"] = df_pertu_axis.apply(lambda n: f"{n.runid}-{n.coor}", axis=1)
    df_pertu_axis["runid_well"] = df_pertu_axis.apply(lambda n: f"{n.runid}-{n.well}", axis=1)
    df_pertu_axis["run"] = s_run
    df_pertu_axis = df_pertu_axis.loc[:,["runid","runid_well","runid_coor", "run","well","coor", "perturbation"]]
    df_pertu_axis["perturbation_run"] = df_pertu_axis.apply(
        lambda n: f"{'+'.join(['_'.join(s_perturbation.split('_')[0:-1]) if (s_perturbation.find('_') > -1) else s_perturbation for s_perturbation in n.perturbation.split('+')])}-{s_run}",
        axis=1
    )
    df_annot = copy.deepcopy(df_pertu_axis)

    # pertu basics
    df_pertu = df_tidy_perturbation.loc[:,["coor","perturbation-recordSetCollapsed","perturbation-recordSet"]].drop_duplicates()
    df_pertu["recordSetMajor"] = df_pertu.loc[:,"perturbation-recordSet"].apply(lambda n: n.split("-")[0])

    # generic recordset
    # bue 20200326: this is for the future. at the moment everithing is to mema based
    df_reagent = df_pertu.loc[df_pertu.recordSetMajor.isin({"recordset"}),:]
    if (df_reagent.shape[0] > 0):
        df_reagent = df_reagent.rename({"perturbation-recordSetCollapsed": "reagent"}, axis=1)
        df_reagent.drop({"perturbation-recordSet", "recordSetMajor"}, axis=1, inplace=True)
        df_reagent["reagent_"] = df_reagent.apply(
            lambda n: f"{'+'.join(['_'.join(s_reagent.split('_')[0:-1]) for s_reagent in n.reagent.split('+')])}",
            axis=1
        )
        df_annot = pd.merge(df_pertu_axis, df_reagent, on="coor")
    else:
        df_annot["reagent"] = None
        df_annot["reagent_"] = None

    # ecm extra cellular matrix protein
    df_es = df_pertu.loc[df_pertu.recordSetMajor.isin({"es"}),:]
    if (df_es.shape[0] > 0):
        df_es = df_es.rename({"perturbation-recordSetCollapsed": "ecm"}, axis=1)
        df_es.drop({"perturbation-recordSet", "recordSetMajor"}, axis=1, inplace=True)
        df_es["ecm_"] = df_es.apply(
            lambda n: f"{'+'.join(['_'.join(s_ecm.split('_')[0:-1]) if (s_ecm.find('_') > -1) else s_ecm for s_ecm in n.ecm.split('+')])}",
            axis=1
        )
        df_annot = pd.merge(df_pertu_axis, df_es, on="coor")

    # ligand
    df_ls = df_pertu.loc[df_pertu.recordSetMajor.isin({"ls"}),:]
    if (df_ls.shape[0] > 0):
        df_ls = df_ls.rename({"perturbation-recordSetCollapsed": "ligand"}, axis=1)
        df_ls.drop({"perturbation-recordSet", "recordSetMajor"}, axis=1, inplace=True)
        df_ls["ligand_run"] = df_ls.apply(
            lambda n: f"{'+'.join(['_'.join(s_ligand.split('_')[0:-1]) if (s_ligand.find('_') > -1) else s_ligand for s_ligand in n.ligand.split('+')])}-{s_run}",
            axis=1
        )
        df_annot = pd.merge(df_annot, df_ls, on="coor")
    else:
        df_annot["ligand"] = None
        df_annot["ligand_run"] = None

    # drug
    df_ds = df_pertu.loc[df_pertu.recordSetMajor.isin({"ds"}),:]
    if (df_ds.shape[0] > 0):
        df_ds = df_ds.rename({"perturbation-recordSetCollapsed": "drug"}, axis=1)
        df_ds.drop({"perturbation-recordSet","recordSetMajor"}, axis=1, inplace=True)
        df_ds["drug_"] = df_ds.apply(
            lambda n: f"{'+'.join(['_'.join(s_drug.split('_')[0:-1]) if (s_drug.find('_') > -1) else s_drug for s_drug in n.drug.split('+')])}",
            axis=1
        )
        df_annot = pd.merge(df_annot, df_ds, on="coor")
    else:
        df_annot["drug"] = None
        df_annot["drug_"] = None

    # index
    df_annot.index = df_annot.apply(lambda n: f"{s_run}-{n.coor}", axis=1)
    df_annot.index.name = "index"
    return(df_annot)


def xyz_statistics(
        df_scdata,
        s_collapse_unit,
        s_measurement,
        s_analysis_prefix,
        s_y,
        s_x,
        s_y_reference=None,
        s_x_reference=None,
        s_y_column_reference=None,
        s_x_column_reference=None,
        tti_yx_significance=((0,0),(1,1),(2,2),(3,3),(4,4)),
        s_y_sort="ascending",
        s_x_sort="ascending",
        i_bin=2**7,
        s_title_prefix="",
        s_opath="./",
    ):
    """
    input:
        df_scdata: single cell dataframe with annotation
        s_collapse_unit: column name to group df_scdata by.
            given the mema experiment, possible values are 'coor', 'well', 'popu'.
        s_measurement: df_scdata value column.
            e.g. f'{s_endpoint}' or cellcount.
        s_analysis_prefix: the prefix for the for the analysis.
            e.g. f'{s_endpoint}-{s_trafo}-{s_norm}' or s_measurement.
        s_y/x: column name for plot x and y axis.
        s_y/x_sort: sorting algorithm for x or y axis.
            possible values are 'ascending' or 'descending'.
            default is 'ascending'.
        s_title_prefix: title prefix.
            e.g. f'{s_study}-{s_run}-' or f'{s_study}-' or ''.
            default is ''.
        i_significance_level: how many sigmad form the median away
            counts as significant? default is 2.
        s_opath: output path.
            default is './'.
    output:
        {s_title_prefix}{s_analysis_prefix}-{s_collapse_unit}-xxx.png

    description:
        generate basic statistics xyz vega heatmap plots.
    """
    # generate output directory
    os.makedirs(s_opath, exist_ok=True)
    os.makedirs(f"{s_opath}px/", exist_ok=True)

    # handle collapse unit:
    if (s_collapse_unit == "coor"):
        s_groupby = "coor"
        ls_label = ls_LABEL_COOR #+ sorted({s_y_significance, s_x_significance})
    elif (s_collapse_unit == "well"):
        s_groupby = "well"
        ls_label = ls_LABEL_WELL
    elif (s_collapse_unit == "popu"):
        s_groupby = "perturbation_run"
        ls_label = ls_LABEL_POPU #+ sorted({s_y_significance, s_x_significance})
    else:
        sys.exit(f"Error @ xyz_statistics : unknown s_collapse_unit {s_collapse_unit}. known are 'coor' and 'popu'.")

    # handle const
    s_y_prefix = f"{s_y}-"
    s_x_prefix = f"{s_x}-"

    # calx
    #print(df_scdata.info())

    print(f"original: {df_scdata.shape}")
    se_filter = df_scdata.loc[:,s_groupby].value_counts() > 2
    a_filter = se_filter.loc[se_filter].index.values
    df_scdata = df_scdata.loc[
        df_scdata.loc[:,s_groupby].isin(a_filter),
        :
    ]  # filter by > 2 sample per collapse unit for propper statistics
    print(f"filtered: {df_scdata.shape}")
    df_annot = df_scdata.loc[:,ls_label].drop_duplicates()  # extract annotation
    o_grouped = df_scdata.loc[:,[s_groupby, s_measurement]].groupby(s_groupby)

    ### min ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-min"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_min = o_grouped.min().reset_index()
    df_min.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_min = pd.merge(df_annot, df_min, on=s_groupby)
    df_min.index = df_min.loc[:,s_groupby]
    df_min.index.name = "index"
    # sigma y
    df_min = calx.df2sigma(
        df_tidy=df_min,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_min,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_min = calx.df2sigma(
        df_tidy=df_min,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_min,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_min.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_min,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### 1st quartile ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-q1"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_q1 = o_grouped.quantile(0.25).reset_index()
    df_q1.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_q1 = pd.merge(df_annot, df_q1, on=s_groupby)
    df_q1.index = df_q1.loc[:,s_groupby]
    df_q1.index.name = "index"
    # sigma y
    df_q1 = calx.df2sigma(
        df_tidy=df_q1,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_q1,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_q1 = calx.df2sigma(
        df_tidy=df_q1,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_q1,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_q1.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_q1,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### 3rd quartile ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-q3"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_q3 = o_grouped.quantile(0.75).reset_index()
    df_q3.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_q3 = pd.merge(df_annot, df_q3, on=s_groupby)
    df_q3.index = df_q3.loc[:,s_groupby]
    df_q3.index.name = "index"
    # sigma y
    df_q3 = calx.df2sigma(
        df_tidy=df_q3,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_q3,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_q3 = calx.df2sigma(
        df_tidy=df_q3,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_q3,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_q3.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_q3,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### max ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-max"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_max = o_grouped.max().reset_index()
    df_max.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_max = pd.merge(df_annot, df_max, on=s_groupby)
    df_max.index = df_max.loc[:,s_groupby]
    df_max.index.name = "index"
    # sigma y
    df_max = calx.df2sigma(
        df_tidy=df_max,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_max,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_max = calx.df2sigma(
        df_tidy=df_max,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_max,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_max.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_max,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### inter quartile range ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-iqr"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_iqr = (
        df_q3.loc[:, s_analysis.replace("-iqr","-q3")] - df_q1.loc[:, s_analysis.replace("-iqr","-q1")]
    ).reset_index()
    df_iqr.rename({0: s_analysis, "index": s_groupby}, axis=1, inplace=True)
    df_iqr = pd.merge(df_annot, df_iqr, on=s_groupby)
    df_iqr.index = df_iqr.loc[:,s_groupby]
    df_iqr.index.name = "index"
    # sigma y
    df_iqr = calx.df2sigma(
        df_tidy=df_iqr,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_iqr,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_iqr = calx.df2sigma(
        df_tidy=df_iqr,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_iqr,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_iqr.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_iqr,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### range ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-range"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_range = (
        df_max.loc[:, s_analysis.replace("-range","-max")] - df_min.loc[:, s_analysis.replace("-range","-min")]
    ).reset_index()
    df_range.rename({0: s_analysis, "index": s_groupby}, axis=1, inplace=True)
    df_range = pd.merge(df_annot, df_range, on=s_groupby)
    df_range.index = df_range.loc[:,s_groupby]
    df_range.index.name = "index"
    # sigma y
    df_range = calx.df2sigma(
        df_tidy=df_range,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_range,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_range = calx.df2sigma(
        df_tidy=df_range,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_range,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_range.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_range,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### median ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-median"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_median = o_grouped.median().reset_index()
    df_median.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_median = pd.merge(df_annot, df_median, on=s_groupby)
    df_median.index = df_median.loc[:,s_groupby]
    df_median.index.name = "index"

    # sigma y
    df_median = calx.df2sigma(
        df_tidy=df_median,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_median,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_median = calx.df2sigma(
        df_tidy=df_median,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_median,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_median.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_median,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### mad (median) ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-mad"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_mad = o_grouped.apply(lambda n: calx.medianad(n.loc[:,s_measurement])).reset_index()
    df_mad.rename({0: s_analysis}, axis=1, inplace=True)
    df_mad = pd.merge(df_annot, df_mad, on=s_groupby)
    df_mad.index = df_mad.loc[:,s_groupby]
    df_mad.index.name = "index"
    # sigma y
    df_mad = calx.df2sigma(
        df_tidy=df_mad,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_mad,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_mad = calx.df2sigma(
        df_tidy=df_mad,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_mad,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_mad.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_mad,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### mean ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-mean"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_mean = o_grouped.mean().reset_index()
    df_mean.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_mean = pd.merge(df_annot, df_mean, on=s_groupby)
    df_mean.index = df_mean.loc[:,s_groupby]
    df_mean.index.name = "index"

    # sigma y
    df_mean = calx.df2sigma(
        df_tidy=df_mean,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_mean,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_mean = calx.df2sigma(
        df_tidy=df_mean,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_mean,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_mean.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_mean,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### standard deviation ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-std"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_std = o_grouped.std(ddof=1).reset_index()
    df_std.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_std = pd.merge(df_annot, df_std, on=s_groupby)
    df_std.index = df_std.loc[:,s_groupby]
    df_std.index.name = "index"
    # sigma y
    df_std = calx.df2sigma(
        df_tidy=df_std,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_std,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_std = calx.df2sigma(
        df_tidy=df_std,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_std,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_std.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_std,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### variance ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-var"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_var = o_grouped.var(ddof=1).reset_index()
    df_var.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_var = pd.merge(df_annot, df_var, on=s_groupby)
    df_var.index = df_var.loc[:,s_groupby]
    df_var.index.name = "index"

    # sigma y
    df_var = calx.df2sigma(
        df_tidy=df_var,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_var,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_var = calx.df2sigma(
        df_tidy=df_var,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_var,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_var.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_var,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### skewness ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-skew"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_skew = o_grouped.skew().reset_index()
    df_skew.rename({s_measurement: s_analysis}, axis=1, inplace=True)
    df_skew = pd.merge(df_annot, df_skew, on=s_groupby)
    df_skew.index = df_skew.loc[:,s_groupby]
    df_skew.index.name = "index"

    # sigma y
    df_skew = calx.df2sigma(
        df_tidy=df_skew,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_skew,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_skew = calx.df2sigma(
        df_tidy=df_skew,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_skew,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_skew.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_skew,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")


    ### kurtosis ###
    s_analysis = f"{s_analysis_prefix}-{s_collapse_unit}-kurtosis"
    print(f"processing: {s_title_prefix}{s_analysis} ...")
    df_kurt = o_grouped.apply(lambda n: stats.kurtosis(n.loc[:,s_measurement])).reset_index()
    df_kurt.rename({0: s_analysis}, axis=1, inplace=True)
    df_kurt = pd.merge(df_annot, df_kurt, on=s_groupby)
    df_kurt.index = df_kurt.loc[:,s_groupby]
    df_kurt.index.name = "index"

    # sigma y
    df_kurt = calx.df2sigma(
        df_tidy=df_kurt,
        s_value=s_analysis,
        s_reference=s_y_reference,
        s_column_reference=s_y_column_reference,
        s_prefix=s_y_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_kurt,
        s_value=s_analysis,
        s_column_color=f"{s_y_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_y}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # sigma x
    df_kurt = calx.df2sigma(
        df_tidy=df_kurt,
        s_value=s_analysis,
        s_reference=s_x_reference,
        s_column_reference=s_x_column_reference,
        s_prefix=s_x_prefix,
        i_ddof=1
    )
    fig = calx.sigma2hist(
        df_kurt,
        s_value=s_analysis,
        s_column_color=f"{s_x_prefix}sigmad",
        s_xaxis_postfix=f" {s_title_prefix}",
        i_bin=i_bin,
    )
    s_ofile = f"{s_title_prefix}{s_analysis}-histogram-{s_x}_sigmad.png"
    fig.savefig(f"{s_opath}px/{s_ofile}")
    plt.close()
    # write to file
    s_ofile = f"{s_title_prefix}{s_analysis}.tsv.gz"
    df_kurt.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")
    # heatmap
    for i_y_sig, i_x_sig in tti_yx_significance:
        s_ofile = f"{s_title_prefix}{s_analysis}-heatmap-sigma{i_y_sig}x{i_x_sig}.png"
        o_chart = xyzvega(
            df_xyz=df_kurt,
            s_z=s_analysis,
            s_y=s_y,
            s_x=s_x,
            s_y_significance=f"{s_y_prefix}sigmad",
            s_x_significance=f"{s_x_prefix}sigmad",
            i_y_significance_level=i_y_sig,
            i_x_significance_level=i_x_sig,
            s_y_sort=s_y_sort,
            s_x_sort=s_x_sort,
            s_title=f"{s_title_prefix}{s_analysis}",
        )
        if (o_chart != None):
            o_chart.save(f"{s_opath}px/{s_ofile}")

    # output
    return(True)


#### BEGIN WELL ####

def statistic_well(
        df_scrun,
        s_study,
        s_run,
        s_endpoint,
        s_trafo,
        s_norm,
        i_bin=2**7,
        s_runtype="mema",
        s_path_acjson="/Users/buchere/srcGitlab/analysis/an/20180408annotV5py/runset/annot_runset-{}-{}_ac.json",
    ):
    """
    """
    # get xy layout
    # bue 20190328: breaks at the moment for all even plates because of air
    with open(s_path_acjson.format(s_runtype,s_run)) as f_json:
         d_acjson = json.load(f_json)

    print("\nstatistic_well input df_scrun:")
    df_xy = xywell(d_acjson)
    #if (s_runtype in {"mema", "well"}):
    df_annot = pd.merge(df_scrun.loc[:,ls_LABEL_SC].drop({"ImageID","CellID"},axis=1).drop_duplicates(), df_xy, on="well")
    #else:
    #    sys.exit(f"@ statistic_well : unknowen s_runtype {s_runtype}. knowen are mema or well.")

    # entropy
    if (s_norm == "4bue"):
        s_analysis = f"{s_endpoint}-{s_trafo}-{s_norm}-well-entropy"
        s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}well_entropy/{s_analysis}/"
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f"{s_opath}px/", exist_ok=True)

        # calx
        print(f"processing: {s_study} {s_run} {s_analysis}!")
        # get well population probability distribution
        dr_entropy = {}
        for i_well in df_scrun.well.unique():
            df_well = df_scrun.loc[df_scrun.well.isin({i_well}),["well", s_endpoint]]
            se_probability = df_well.loc[:,s_endpoint].value_counts(normalize=True)
            #for s_bin in hexa.es_BIN.difference(set(se_probability.index)):
            #    se_probability[s_bin] = 0
            se_probability.sort_index(inplace=True)
            r_h = calx.entropy_marginal(se_probability.values, o_log=np.log2)
            dr_entropy.update({i_well: r_h})
        # get well population entropy
        se_entropy = pd.Series(dr_entropy, name="entropy_bit")
        se_entropy.index.name = "well"
        df_entropy = se_entropy.reset_index()
        df_analysis = pd.merge(df_annot, df_entropy, on="well")

        # sigma y
        df_analysis = calx.df2sigma(
            df_tidy=df_analysis,
            s_value="entropy_bit",
            s_reference=None,
            s_column_reference=None,
            s_prefix="y-",
            i_ddof=1
        )
        fig = calx.sigma2hist(
            df_analysis,
            s_value="entropy_bit",
            s_column_color="y-sigmad",
            s_xaxis_postfix=f" entropy_well",
            i_bin=i_bin,
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-y_sigmad.png"
        fig.savefig(f"{s_opath}px/{s_ofile}")
        plt.close()

        # sigma x
        df_analysis = calx.df2sigma(
            df_tidy=df_analysis,
            s_value="entropy_bit",
            s_reference=None,
            s_column_reference=None,
            s_prefix="x-",
            i_ddof=1
        )
        fig = calx.sigma2hist(
            df_analysis,
            s_value="entropy_bit",
            s_column_color="x-sigmad",
            s_xaxis_postfix=f" entropy_spot",
            i_bin=i_bin,
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-x_sigmad.png"
        fig.savefig(f"{s_opath}px/{s_ofile}")
        plt.close()

        # write to file
        s_ofile = f"{s_study}-{s_run}-{s_analysis}.tsv.gz"
        df_analysis.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")

        # xy heatmap
        #print(df_analysis.info())
        o_chart = xyzvega(
            df_xyz=df_analysis,
            s_z="entropy_bit",
            s_y="y",
            s_x="x",
            s_y_significance="y-sigmad",
            s_x_significance="x-sigmad",
            i_y_significance_level=0,
            i_x_significance_level=0,
            s_y_sort="descending",
            s_x_sort="ascending",
            s_title=f"{s_study}-{s_run}-{s_analysis}"
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-heatmap-sigma0x0.png"
        o_chart.save(f"{s_opath}{s_ofile}")

    # others
    else:
        # cell count
        if (s_endpoint == "Dapi_total_nuc") and (s_trafo == "log2scale") and (s_norm == "abs"):
            s_analysis = f"{s_endpoint}-{s_trafo}-cellcount"
            s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}well_cellcount/{s_analysis}/"
            os.makedirs(s_opath, exist_ok=True)
            os.makedirs(f"{s_opath}px/", exist_ok=True)
            print(f"output path: {s_opath} and {s_opath}px/")

            # calx
            print(f"processing: {s_study} {s_run} {s_analysis}!")
            df_count = df_scrun.loc[:,["well","CellID"]].groupby("well").count().reset_index()
            df_count.rename({"CellID": "cellcount"}, axis=1, inplace=True)
            df_analysis = pd.merge(df_annot, df_count, on="well")

            # sigma y
            df_analysis = calx.df2sigma(
                df_tidy=df_analysis,
                s_value="cellcount",
                s_reference=None,
                s_column_reference=None,
                s_prefix="y-",
                i_ddof=1
            )
            fig = calx.sigma2hist(
                df_analysis,
                s_value="cellcount",
                s_column_color="y-sigmad",
                s_xaxis_postfix=f" cellcount_spot",
                i_bin=i_bin,
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-y_sigmad.png"
            fig.savefig(f"{s_opath}px/{s_ofile}")
            plt.close()
            print(f"file written: {s_opath}px/{s_ofile}")

            # sigma x
            df_analysis = calx.df2sigma(
                df_tidy=df_analysis,
                s_value="cellcount",
                s_reference=None,
                s_column_reference=None,
                s_prefix="x-",
                i_ddof=1
            )
            fig = calx.sigma2hist(
                df_analysis,
                s_value="cellcount",
                s_column_color="x-sigmad",
                s_xaxis_postfix=f" cellcount_spot",
                i_bin=i_bin,
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-x_sigmad.png"
            fig.savefig(f"{s_opath}px/{s_ofile}")
            plt.close()

            # write to file
            s_ofile = f"{s_study}-{s_run}-{s_analysis}.tsv.gz"
            df_analysis.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")

            # xy heatmap
            #print(df_analysis.info())
            o_chart = xyzvega(
                df_xyz=df_analysis,
                s_z="cellcount",
                s_y="y",
                s_x="x",
                s_y_significance="y-sigmad",
                s_x_significance="x-sigmad",
                i_y_significance_level=0,
                i_x_significance_level=0,
                s_y_sort="descending",
                s_x_sort="ascending",
                s_title=f"{s_study}-{s_run}-{s_analysis}"
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-heatmap-sigma0x0.png"
            o_chart.save(f"{s_opath}px/{s_ofile}")

        # calx basic statistics
        s_analysis = f"{s_endpoint}-{s_trafo}-{s_norm}"
        s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}well_stats/{s_analysis}/"
        print(f"processing: {s_study} {s_run} {s_analysis}!")
        #if (s_runtype in {"mema" or "well"}):
        df_analysis = pd.merge(df_scrun.drop({"CellID"}, axis=1), df_xy, on="well")
        #else:
        #    sys.exit(f"@ statistic_spot : unknowen s_runtype {s_runtype}. knowen are mema or well.")
        xyz_statistics(
            df_scdata = df_analysis,
            s_collapse_unit="well",
            s_measurement=s_endpoint,
            s_analysis_prefix=s_analysis,
            s_y="y",
            s_x="x",
            s_y_reference=None,
            s_x_reference=None,
            s_y_column_reference=None,
            s_x_column_reference=None,
            tti_yx_significance=((0,0),),
            s_y_sort="descending",
            s_x_sort="ascending",
            i_bin=i_bin,
            s_title_prefix=f"{s_study}-{s_run}-",
            s_opath=s_opath,
        )

    # output
    return(True)

#### END WELL ####


#### BEGIN SPOT ####

def statistic_spot(
        df_scrun,
        s_study,
        s_run,
        s_endpoint,
        s_trafo,
        s_norm,
        i_bin=2**7,
        s_runtype="mema",
        s_path_acjson="/Users/buchere/srcGitlab/analysis/an/20180408annotV5py/runset/annot_runset-{}-{}_ac.json",
    ):
    """
    """
    # get xy layout
    # bue 20190328: breaks at the moment for all even plates because of air
    with open(s_path_acjson.format(s_runtype,s_run)) as f_json:
         d_acjson = json.load(f_json)
    # bue 20190329: begin hack because of air annotation
    d_dmso = {
        'batch': 'not_available',
        'catalogNu': 'not_available',
        'conc': 0.032,
        'concUnit': 'not_available',
        'cultType': 'batch',
        'externalId': 'LSM-36361',
        'manufacture': 'not_available',
        'recordSet': 'ds-Dmso_noA03_1v1',
        'timeBegin': 18,
        'timeEnd': 90,
        'timeUnit': 'hour_uo0000032'
    }
    for s_coor in d_acjson.keys():
        try:
             d_acjson[s_coor]['perturbation'].pop('air_Own')
             d_acjson[s_coor]['perturbation'].update({'DMSO_chebi28262': d_dmso})
        except AttributeError:
            pass
        except KeyError:
            pass
        except TypeError:
            pass
    # here the hack ends

    print("\nstatistic_spot input df_scrun:")
    df_xy = xycoor(d_acjson)

    #if (s_runtype in {"mema", "well"}):
    df_annot = pd.merge(df_scrun.loc[:,ls_LABEL_SC].drop({"CellID"},axis=1).drop_duplicates(), df_xy, on="coor")
    #else:
    #    sys.exit(f"@ statistic_spot : unknowen s_runtype {s_runtype}. knowen are mema or well.")

    # entropy
    if (s_norm == "4bue"):
        s_analysis = f"{s_endpoint}-{s_trafo}-{s_norm}-coor-entropy"
        s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}coor_entropy/{s_analysis}/"
        os.makedirs(s_opath, exist_ok=True)
        os.makedirs(f"{s_opath}px/", exist_ok=True)

        # calx
        print(f"processing: {s_study} {s_run} {s_analysis}!")
        # get spot population probability distribution
        dr_entropy = {}
        for i_coor in df_scrun.coor.unique():
            df_coor = df_scrun.loc[df_scrun.coor.isin({i_coor}),["coor", s_endpoint]]
            se_probability = df_coor.loc[:,s_endpoint].value_counts(normalize=True)
            #for s_bin in hexa.es_BIN.difference(set(se_probability.index)):
            #    se_probability[s_bin] = 0
            se_probability.sort_index(inplace=True)
            r_h = calx.entropy_marginal(se_probability.values, o_log=np.log2)
            dr_entropy.update({i_coor: r_h})
        # get spot population entropy
        se_entropy = pd.Series(dr_entropy, name="entropy_bit")
        se_entropy.index.name = "coor"
        df_entropy = se_entropy.reset_index()
        df_analysis = pd.merge(df_annot, df_entropy, on="coor")

        # sigma y
        df_analysis = calx.df2sigma(
            df_tidy=df_analysis,
            s_value="entropy_bit",
            s_reference=None,
            s_column_reference=None,
            s_prefix="y-",
            i_ddof=1
        )
        fig = calx.sigma2hist(
            df_analysis,
            s_value="entropy_bit",
            s_column_color="y-sigmad",
            s_xaxis_postfix=f" entropy_spot",
            i_bin=i_bin,
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-y_sigmad.png"
        fig.savefig(f"{s_opath}px/{s_ofile}")
        plt.close()

        # sigma x
        df_analysis = calx.df2sigma(
            df_tidy=df_analysis,
            s_value="entropy_bit",
            s_reference=None,
            s_column_reference=None,
            s_prefix="x-",
            i_ddof=1
        )
        fig = calx.sigma2hist(
            df_analysis,
            s_value="entropy_bit",
            s_column_color="x-sigmad",
            s_xaxis_postfix=f" entropy_spot",
            i_bin=i_bin,
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-x_sigmad.png"
        fig.savefig(f"{s_opath}px/{s_ofile}")
        plt.close()

        # write to file
        s_ofile = f"{s_study}-{s_run}-{s_analysis}.tsv.gz"
        df_analysis.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")

        # xy heatmap
        #print(df_analysis.info())
        o_chart = xyzvega(
            df_xyz=df_analysis,
            s_z="entropy_bit",
            s_y="y",
            s_x="x",
            s_y_significance="y-sigmad",
            s_x_significance="x-sigmad",
            i_y_significance_level=0,
            i_x_significance_level=0,
            s_y_sort="descending",
            s_x_sort="ascending",
            s_title=f"{s_study}-{s_run}-{s_analysis}"
        )
        s_ofile = f"{s_study}-{s_run}-{s_analysis}-heatmap-sigma0x0.png"
        o_chart.save(f"{s_opath}{s_ofile}")

    # others
    else:
        # cell count
        if (s_endpoint == "Dapi_total_nuc") and (s_trafo == "log2scale") and (s_norm == "abs"):
            s_analysis = f"{s_endpoint}-{s_trafo}-cellcount"
            s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}coor_cellcount/{s_analysis}/"
            os.makedirs(s_opath, exist_ok=True)
            os.makedirs(f"{s_opath}px/", exist_ok=True)
            print(f"output path: {s_opath} and {s_opath}px/")

            # calx
            print(f"processing: {s_study} {s_run} {s_analysis}!")
            df_count = df_scrun.loc[:,["coor","CellID"]].groupby("coor").count().reset_index()
            df_count.rename({"CellID": "cellcount"}, axis=1, inplace=True)
            df_analysis = pd.merge(df_annot, df_count, on="coor")

            # sigma y
            df_analysis = calx.df2sigma(
                df_tidy=df_analysis,
                s_value="cellcount",
                s_reference=None,
                s_column_reference=None,
                s_prefix="y-",
                i_ddof=1
            )
            fig = calx.sigma2hist(
                df_analysis,
                s_value="cellcount",
                s_column_color="y-sigmad",
                s_xaxis_postfix=f" cellcount_spot",
                i_bin=i_bin,
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-y_sigmad.png"
            fig.savefig(f"{s_opath}px/{s_ofile}")
            plt.close()
            print(f"file written: {s_opath}px/{s_ofile}")

            # sigma x
            df_analysis = calx.df2sigma(
                df_tidy=df_analysis,
                s_value="cellcount",
                s_reference=None,
                s_column_reference=None,
                s_prefix="x-",
                i_ddof=1
            )
            fig = calx.sigma2hist(
                df_analysis,
                s_value="cellcount",
                s_column_color="x-sigmad",
                s_xaxis_postfix=f" cellcount_spot",
                i_bin=i_bin,
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-histogram-x_sigmad.png"
            fig.savefig(f"{s_opath}px/{s_ofile}")
            plt.close()

            # write to file
            s_ofile = f"{s_study}-{s_run}-{s_analysis}.tsv.gz"
            df_analysis.to_csv(f"{s_opath}{s_ofile}", sep="\t", compression="gzip")

            # xy heatmap
            #print(df_analysis.info())
            o_chart = xyzvega(
                df_xyz=df_analysis,
                s_z="cellcount",
                s_y="y",
                s_x="x",
                s_y_significance="y-sigmad",
                s_x_significance="x-sigmad",
                i_y_significance_level=0,
                i_x_significance_level=0,
                s_y_sort="descending",
                s_x_sort="ascending",
                s_title=f"{s_study}-{s_run}-{s_analysis}"
            )
            s_ofile = f"{s_study}-{s_run}-{s_analysis}-heatmap-sigma0x0.png"
            o_chart.save(f"{s_opath}px/{s_ofile}")

        # calx basic statistics
        s_analysis = f"{s_endpoint}-{s_trafo}-{s_norm}"
        s_opath = f"{s_path_norm.format(s_study, s_norm, s_trafo)}coor_stats/{s_analysis}/"
        print(f"processing: {s_study} {s_run} {s_analysis}!")
        #if (s_runtype in {"mema","well"}):
        df_analysis = pd.merge(df_scrun.drop({"CellID"}, axis=1), df_xy, on="coor")
        #else:
        #    sys.exit(f"@ statistic_spot : unknowen s_runtype {s_runtype}. knowen are mema or well.")
        xyz_statistics(
            df_scdata = df_analysis,
            s_collapse_unit="coor",
            s_measurement=s_endpoint,
            s_analysis_prefix=s_analysis,
            s_y="y",
            s_x="x",
            s_y_reference=None,
            s_x_reference=None,
            s_y_column_reference=None,
            s_x_column_reference=None,
            tti_yx_significance=((0,0),),
            s_y_sort="descending",
            s_x_sort="ascending",
            i_bin=i_bin,
            s_title_prefix=f"{s_study}-{s_run}-",
            s_opath=s_opath,
        )

    # output
    return(True)

#### END SPOT ####



#def xycoor_cell():
#    return(df_xy)


#df xyzvega_cell():
#   return(o_chart)


def xycoor(d_acjson):
    """
    """
    dli_yx = ac.coor2yx(d_acjson)
    df_xy = pd.DataFrame(dli_yx, index=["y","x"]).T
    df_xy.index = df_xy.index.astype("int")
    df_xy.index.name = "coor"
    df_xy.reset_index(inplace=True)
    # output
    return(df_xy)

def xywell(d_acjson):
    """
    """
    dli_yx = ac.well2yx(d_acjson)
    df_xy = pd.DataFrame(dli_yx, index=["y","x"]).T
    df_xy.index = df_xy.index.astype("int")
    df_xy.index.name = "well"
    df_xy.reset_index(inplace=True)
    # output
    return(df_xy)

def xyzvega(
        df_xyz,
        s_z,
        s_y,
        s_x,
        s_y_significance,
        s_x_significance,
        i_y_significance_level=0,
        i_x_significance_level=0,
        s_ztype="quantitative",
        s_zsort="descending",
        s_y_sort="ascending",
        s_x_sort="ascending",
        s_title="",
    ):
    """
    """
    # handle significance
    #print(df_xyz.info())
    ei_y_sigma = set(df_xyz.loc[:, s_y_significance].unique())
    if (i_y_significance_level >= 1):
        ei_y_sigma.discard(1)
        ei_y_sigma.discard(-1)
    if (i_y_significance_level >= 2):
        ei_y_sigma.discard(2)
        ei_y_sigma.discard(-2)
    if (i_y_significance_level >= 3):
        ei_y_sigma.discard(3)
        ei_y_sigma.discard(-3)
    if (i_y_significance_level >= 4):
        ei_y_sigma.discard(4)
        ei_y_sigma.discard(-4)
    ei_x_sigma = set(df_xyz.loc[:, s_x_significance].unique())
    if (i_x_significance_level >= 1):
        ei_x_sigma.discard(1)
        ei_x_sigma.discard(-1)
    if (i_x_significance_level >= 2):
        ei_x_sigma.discard(2)
        ei_x_sigma.discard(-2)
    if (i_x_significance_level >= 3):
        ei_x_sigma.discard(3)
        ei_x_sigma.discard(-3)
    if (i_x_significance_level >= 4):
        ei_x_sigma.discard(4)
        ei_x_sigma.discard(-4)
    df_chart = df_xyz.loc[df_xyz[s_y_significance].isin(ei_y_sigma),:].loc[df_xyz[s_x_significance].isin(ei_x_sigma),:]

    # plot
    print(f"frame shape: {df_chart.shape}")
    #print(df_chart.info())
    o_chart = None
    if (df_chart.shape[0] > 1):
        o_chart = alt.Chart(df_chart).mark_rect().encode(
            y = alt.Y(
                f"{s_y}:O",
                sort=s_y_sort,
            ),
            x = alt.X(
                f"{s_x}:O",
                sort=s_x_sort,
            ),
            color = alt.Color(
                f"{s_z}",
                type=s_ztype,
                sort=s_zsort,
            ),
        ).properties(
            title=s_title,
        )
    # output
    return(o_chart)
