####
# title: mema_raw2norm.py
#
# language: Python3
# license: GPLv>=3
# date: 2019-03-11
# author: bue
#
# run: python3 mema_raw2norm.py
#
# description:
#    code to transform a study of raw, Guillaume segmented mema expression data
#    a into PBS COL1 normalized data and more.
####

# library
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pymema import run
from pysci import calx, hexa

# r library
from rpy2 import robjects
robjects.r('library(FNN)')


### set ###
s_runset = 'well'


### off we go ###
# const
s_path_annot = f'./{s_runset}_ac/'

# function
def fnn_entropy(
        es_continuous = {'abs'}, #{'raw','norm','abs','absm'},
        i_min_population_size = 2**10,
        i_k=16,
    ):
    '''
    '''

    # off we go
    print('run fnn_entropy')
    for s_norm in es_continuous:
        for s_path, s_trafo in (
                (f'./{s_runset}_extract_{s_norm}/log2scale/', 'log2scale'),
                (f'./{s_runset}_extract_{s_norm}/rrscale/', 'rrscale'),
            ):
            os.makedirs(f'{s_path}popu_fnn_entropy/', exist_ok=True)
            os.makedirs(f'{s_path}popu_fnn_entropy/px/', exist_ok=True)

            s_tmppath = f'{s_path}popu_fnn_entropy/tmp/'
            os.makedirs(s_tmppath, exist_ok=True)

            for s_ifile in sorted(os.listdir(s_path)):
                if (s_ifile.endswith(f'-{s_trafo}-{s_norm}.tsv.gz')):
                    print(f'fnn_entropy processing: {s_ifile} ...')
                    s_study, s_endpoint, _, s_ext = s_ifile.split('-')

                    # load data file
                    df_scdata = pd.read_csv(f'{s_path}{s_ifile}', sep='\t', index_col=0)

                    # off we go
                    df_entropy = pd.DataFrame()
                    es_pertu_run = set(df_scdata.loc[:,'perturbation_run'].unique())
                    i_total = len(es_pertu_run) - 1
                    #print(es_pertu_run)
                    for i, s_pertu_run in enumerate(es_pertu_run):

                        # filter data
                        se_scdata = df_scdata.loc[df_scdata.perturbation_run.isin({s_pertu_run}), s_endpoint]

                        print(f'process entropy {i}/{i_total} {s_endpoint} {s_trafo} {s_pertu_run}')

                        # to make it compatible with the divergence analysis I have to set the population size to the same size as popu_ok.
                        if (se_scdata.values.shape[0] > i_min_population_size):
                            print('popu size 0K!')

                            # calx
                            s_tmpfile = f'{s_pertu_run}-{s_endpoint}.csv'
                            se_scdata.to_csv(f'{s_tmppath}{s_tmpfile}', header=True)

                            # R session
                            #s_command = f'df_calx <- read.csv("{s_tmppath}{s_tmpfile}",  row.names="index")'
                            s_command = f'df_calx <- read.csv("{s_tmppath}{s_tmpfile}")'
                            robjects.r(s_command)
                            s_command = f'entropy(df_calx${s_endpoint}, k={i_k})'
                            r_entropy = robjects.r(s_command)[-1]

                            # pack output
                            if (not np.isnan(r_entropy)) and (not np.isinf(r_entropy)):
                                se_entry = pd.Series(
                                    [
                                        s_pertu_run,
                                        r_entropy,
                                    ],
                                    index=[
                                        'perturbation_run',
                                        f'entropy_fnn_bit',
                                    ]
                                )
                                df_entropy = df_entropy.append(se_entry.to_frame().T)
                                #print(f'0k.')

                        else:
                            #print(f'equal or less then i_min_population_size samples {se_scdata.values.shape[0]} in perturbation population.')
                            pass

                    # fuse result
                    if (df_entropy.shape != (0,0)):
                        # re-type
                        df_entropy.entropy_fnn_bit = df_entropy.entropy_fnn_bit.astype(float)

                        # get annotation
                        print('get annotation')
                        df_annot_popu = df_scdata.loc[
                            :,
                            run.ls_LABEL_POPU
                        ].drop_duplicates()

                        # merge
                        print(f'merge {s_endpoint} entropy result and annotation and write to file.')
                        #print(df_annot_popu.info())
                        #print(df_entropy.info())
                        df_entropy = pd.merge(df_annot_popu, df_entropy, on='perturbation_run')

                        # sigma calcualtion
                        print(f'sigmas calculation.')
                        df_study = pd.DataFrame()
                        # df_entropy = calx.df2sigma(df_tidy=df_entropy, s_value=f'{s_endpoint}-entropy_bit', i_ddof=1)
                        for s_runid in df_entropy.runid.unique():
                            df_run = df_entropy.loc[df_entropy.runid.isin({s_runid}),:].copy()
                            df_run = calx.df2sigma(
                                df_tidy=df_run,
                                s_value='entropy_fnn_bit',
                                s_reference=None,
                                s_column_reference=None,
                                s_prefix='ecm-',
                                i_ddof=1
                            )
                            df_run = calx.df2sigma(
                                df_tidy=df_run,
                                s_value='entropy_fnn_bit',
                                s_reference=run.s_REFERENCE_LIGAND,
                                s_column_reference='ligand',
                                s_prefix='ligand-',
                                i_ddof=1
                            )
                            df_study = df_study.append(df_run)

                            # significance plot run wide
                            #fig = calx.sigma2hist(df_entropy, s_value=f'{s_endpoint}-entropy_bit', s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-{s_norm}-fnn_entropy', s_color='sigmad', i_bin=32)
                            fig = calx.sigma2hist(
                                df_run,
                                s_value='entropy_fnn_bit',
                                s_column_color='ecm-sigmad',
                                s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-fnn_entropy',
                                i_bin=16
                            )
                            s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-fnn_entropy-ecm_sigmad.png'
                            fig.savefig(f'{s_path}popu_fnn_entropy/px/{s_out}')
                            plt.close()

                            fig = calx.sigma2hist(
                                df_run,
                                s_value='entropy_fnn_bit',
                                s_column_color='ligand-sigmad',
                                s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-fnn_entropy',
                                i_bin=16
                            )
                            s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-fnn_entropy-ligand_sigmad.png'
                            fig.savefig(f'{s_path}popu_fnn_entropy/px/{s_out}')
                            plt.close()

                        # write to file
                        df_study.index = df_study.perturbation_run
                        df_study.sort_index(inplace=True)
                        df_study.index.name = f'{s_study}-{s_endpoint}'
                        s_ofile = s_ifile.replace('.tsv.gz','-fnn_entropy.tsv.gz')
                        df_study.to_csv(f'{s_path}popu_fnn_entropy/{s_ofile}', sep='\t', compression='gzip')

                        # significance plot study wide
                        # bue 20190531: does not work because even in a study
                        # different plates will have different sigmas

                    else:
                        print(
                            'Warning: not a single entropy value could be calcualted via FNN.\nMost probably are all entropy values -inf, becasue the scaling caused to many values to be the same.\nNo result can be outputted!'
                        )


def fnn_divergence(
        es_continuous = {'abs'}, #{'raw','norm','abs','absm'},
        i_min_population_size = 2**10,
        i_k=16,
    ):
    '''
    '''
    # OpenMP hack
    # export KMP_DUPLICATE_LIB_OK=TRUE
    print('switch KMP_DUPLICATE_LIB_OK:')
    os.system('echo $KMP_DUPLICATE_LIB_OK')
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'TRUE'
    os.system('echo $KMP_DUPLICATE_LIB_OK')

    # off we go
    print('run fnn_divergence')
    b_get_mapping = True
    for s_norm in es_continuous:
        for s_path, s_trafo in (
                (f'./{s_runset}_extract_{s_norm}/log2scale/', 'log2scale'),
                (f'./{s_runset}_extract_{s_norm}/rrscale/', 'rrscale'),
            ):
            os.makedirs(f'{s_path}popu_fnn_divergence/', exist_ok=True)
            os.makedirs(f'{s_path}popu_fnn_divergence/px/', exist_ok=True)

            s_tmppath = f'{s_path}popu_fnn_divergence/tmp/'
            os.makedirs(s_tmppath, exist_ok=True)

            for s_ifile in sorted(os.listdir(s_path)):
                if (s_ifile.endswith(f'-{s_trafo}-{s_norm}.tsv.gz')):
                    print(f'fnn_divergence processing: {s_ifile} ...')
                    s_study, s_endpoint, _, s_ext = s_ifile.split('-')

                    # load reference test perturbation mapping
                    if (b_get_mapping):
                        with open(f'{s_path_annot}{s_study}-ref_perturbation_run.json') as f_json:
                            ds_test2ref_perturbation = json.load(f_json)
                        b_get_mapping = False

                    # load data file
                    df_scdata = pd.read_csv(f'{s_path}{s_ifile}', sep='\t', index_col=0)

                    # off we go
                    df_dkl = pd.DataFrame()
                    es_test_pertu_run = set(df_scdata.loc[:,'perturbation_run'].unique()).intersection(set(ds_test2ref_perturbation.keys()))
                    i_total = len(es_test_pertu_run) - 1
                    #print(es_test_pertu_run)
                    for i, s_test_pertu_run in enumerate(es_test_pertu_run):

                        # filter data
                        s_ref_pertu_run = ds_test2ref_perturbation[s_test_pertu_run]
                        se_ref = df_scdata.loc[df_scdata.perturbation_run.isin({s_ref_pertu_run}), s_endpoint]
                        se_test = df_scdata.loc[df_scdata.perturbation_run.isin({s_test_pertu_run}), s_endpoint]

                        print(f'process dkl {i}/{i_total} {s_endpoint} {s_trafo} {s_ref_pertu_run} {s_test_pertu_run}')

                        # to make it compatible with the divergence analysis I have to set the population size to the same size as popu_ok.
                        if (se_ref.values.shape[0] > i_min_population_size) and (se_test.values.shape[0] > i_min_population_size):

                            # calx
                            s_tmpfile_ref = f'ref-{s_ref_pertu_run}-{s_endpoint}.csv'
                            se_ref.to_csv(f'{s_tmppath}{s_tmpfile_ref}', header=True)

                            s_tmpfile_test = f'test-{s_test_pertu_run}-{s_endpoint}.csv'
                            se_test.to_csv(f'{s_tmppath}{s_tmpfile_test}', header=True)

                            # R session
                            #s_command = f'df_ref <- read.csv("{s_tmppath}{s_tmpfile_ref}",  row.names='index')'
                            s_command = f'df_ref <- read.csv("{s_tmppath}{s_tmpfile_ref}")'
                            robjects.r(s_command)
                            #s_command = f'df_test <- read.csv("{s_tmppath}{s_tmpfile_test}",  row.names='index')'
                            s_command = f'df_test <- read.csv("{s_tmppath}{s_tmpfile_test}")'
                            robjects.r(s_command)
                            s_command = f'KL.divergence(df_test${s_endpoint}, df_ref${s_endpoint}, k={i_k})'
                            r_dkl = robjects.r(s_command)[-1]

                            # pack output
                            if (not np.isnan(r_dkl)) and (not np.isinf(r_dkl)):
                                se_entry = pd.Series(
                                    [
                                        s_test_pertu_run,
                                        s_ref_pertu_run,
                                        r_dkl,
                                    ],
                                    index=[
                                        'perturbation_run',
                                        'ref_perturbation',
                                        'dkl_fnn_bit',
                                    ]
                                )
                                df_dkl = df_dkl.append(se_entry.to_frame().T)
                                #print(f'0k.')

                        else:
                            #print(f'equal or less then i_min_population_size samples in reference {se_ref.values.shape[0]} or test {se_test.values.shape[0]} perturbation population.')
                            pass

                    # fuse result
                    if (df_dkl.shape != (0,0)):
                        # re-type
                        df_dkl.dkl_fnn_bit = df_dkl.dkl_fnn_bit.astype(float)

                        # get annotation
                        print('get annotation')
                        df_annot_popu = df_scdata.loc[
                            :,
                            run.ls_LABEL_POPU
                        ].drop_duplicates()

                        # merge
                        print(f'merge {s_endpoint} dkl result and annotation and write to file.')
                        #print(df_annot_popu.info())
                        #print(df_dkl.info())
                        df_dkl = pd.merge(df_annot_popu, df_dkl, on='perturbation_run')

                        # sigma calcualtion
                        print(f'sigmas calculation.')
                        df_study = pd.DataFrame()
                        # df_dkl = calx.df2sigma(df_tidy=df_dkl, s_value=f'{s_endpoint}-Dkl_bit', i_ddof=1)
                        for s_runid in df_dkl.runid.unique():
                            df_run = df_dkl.loc[df_dkl.runid.isin({s_runid}),:].copy()
                            df_run = calx.df2sigma(
                                df_tidy=df_run,
                                s_value='dkl_fnn_bit',
                                s_reference=None,
                                s_column_reference=None,
                                s_prefix='dkl_fnn-',
                                i_ddof=1
                            )
                            df_study = df_study.append(df_run)

                            # significance plot run wide
                            #fig = calx.sigma2hist(df_dkl, s_value=f'{s_endpoint}-Dkl_bit', s_xaxis_postfix=f' {s_study}-{s_endpoint}-{s_trafo}-{s_norm}-fnn_dkl', s_color='sigmad', i_bin=32)
                            fig = calx.sigma2hist(
                                df_run,
                                s_value='dkl_fnn_bit',
                                s_column_color='dkl_fnn-sigmad',
                                s_xaxis_postfix=f' {s_study}-{s_runid}-{s_endpoint}-{s_trafo}-fnn_dkl',
                                i_bin=16
                            )
                            s_out = f'{s_study}-{s_runid.split("-")[-1]}-{s_endpoint}-{s_trafo}-fnn_dkl_sigmad.png'
                            fig.savefig(f'{s_path}popu_fnn_divergence/px/{s_out}')
                            plt.close()

                        # write to file
                        df_study.index = df_study.perturbation_run
                        df_study.sort_index(inplace=True)
                        df_study.index.name = f'{s_study}-{s_endpoint}'
                        s_ofile = s_ifile.replace('.tsv.gz','-fnn_dkl.tsv.gz')
                        df_study.to_csv(f'{s_path}popu_fnn_divergence/{s_ofile}', sep='\t', compression='gzip')

                        # significance plot study wide
                        # bue 20190531: does not work because even in a study
                        # different plates will have different sigmas

                    else:
                        print(
                            'Warning: not a single entropy value could be calcualted via FNN.\nMost probably are all entropy values -inf, becasue the scaling caused to many values to be the same.\nNo result can be outputted!'
                        )

    # OpenMP hack
    print('switch KMP_DUPLICATE_LIB_OK:')
    os.system('echo $KMP_DUPLICATE_LIB_OK')
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'FALSE'
    os.system('echo $KMP_DUPLICATE_LIB_OK')


# main call
if __name__ == '__main__':

    # run dmc
    fnn_entropy(es_continuous = {'abs'})
    #fnn_divergence(es_continuous = {'abs'})
