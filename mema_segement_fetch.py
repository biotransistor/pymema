####
# title: 20181206_the_fetch.py
#
# language: Python3
# license: GPLv>=3
# date: 2018-12-06
# author: bue
#
# run: python3 20181206_the_fetch.py
#
# description:
#    code to fetch mema study based segmentation mask tiff images
#    from Guillaume's shared folder.
#
#    this code depends on the folder structure it is part of.
#    this code should be on the root inside an thg folder.
#    inside the thg folder should be a imageid folder.
#    inside the imageid folder should be a json files per barcode of interest.
#    the json file name should be in the format imageid_{barcode}.json.
#    inside the json file should be a list if integers.
#    those integer are the image ids of interest.
#
#    this code will fetch all tiff images of interest
#    and store them in folder named after the plate barcode the image belongs to.
####

# library
import json
import os
import shutil

# const
s_path_imageid = "./imageid/"
s_path_guillaume = "/graylab/share/thibaulg/Mema/HCC1143_COL1 - Segmentation/"

# process
for s_file in os.listdir(s_path_imageid):
    s_barcode = s_file.split(".")[0].split("_")[-1]

    # get barcode related image ids
    if (s_file.startswith("imageid_")):
        with open("{}{}".format(s_path_imageid, s_file)) as f_json:
            li_imageid = json.load(f_json)

        # make output dir
        if not (os.path.exists(s_barcode)):
            os.makedirs(s_barcode)

        # fetch tiff images from guillaume
        for i_imageid in li_imageid:
            print("process: {} {} ...".format(s_barcode, i_imageid))
            shutil.copy("{}{} - Cells Basins.tif".format(s_path_guillaume, i_imageid), "./{}/".format(s_barcode))
            shutil.copy("{}{} - Nuclei Basins.tif".format(s_path_guillaume, i_imageid), "./{}/".format(s_barcode))
