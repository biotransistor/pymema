###
# title: mema_level0_fetch.py
#
# license: GPLv>=3
# date: 2019-05-10
# author: bue
#
# run:
#    adjust s_study and es_id
#    python3 mema_level0_fetch
#
# description:
#    code to fetch Guillaume's segmented level0 data.
####


# library
import os
import shutil

# setting
'''
s_study = "hcc1143_lowserum_sst"
es_id = {"LI8V01171", "LI8V01172", "LI8V01173", "LI8V01174", "LI8V01175", "LI8V01176", "LI8V01177", "LI8V01178", "LI8V01179"}  # ,"LI8V01180"
'''
'''
s_study = "hcc1143_highserum_sst"
es_id = {"LI8V01191", "LI8V01192", "LI8V01193", "LI8V01194", "LI8V01195", "LI8V01196", "LI8V01197", "LI8V01198", "LI8V01199"}
'''

'''
s_study = "hcc1143_highserum_tram_sst"
es_id = {"LI8V01181", "LI8V01182", "LI8V01183", "LI8V01184", "LI8V01185", "LI8V01186", "LI8V01187", "LI8V01188", "LI8V01189"}
'''

'''
s_study = "hmec153ld1my_201911"
es_id = ["LI8X01121", "LI8X01122", "LI8X01123", "LI8X01124", "LI8X01125","LI8X01126", "LI8X01127", "LI8X01128", "LI8X01129"]
'''

'''
s_study = "a549_201912"
es_id = ["LI8X01111", "LI8X01112", "LI8X01113", "LI8X01114", "LI8X01115", "LI8X01116", "LI8X01117", "LI8X01118", "LI8X01119"]
'''

# const
'''
s_ipath = "/eppec/storage/groups/heiserlab/lincs/"
s_ipath = "/eppec/storage/groups/heiserlab/lincs/devel/"
'''
s_ipath = "/data/share/lincs_user/{}/Analysis/"

s_opath = "./{}/mema_level0/".format(s_study)

ei_well = {1,2,3,4,5,6,7,8}
s_imageid = "{}_imageIDs.tsv"
s_well = "{}_Well_{}_level_0.csv"


# processing
os.makedirs(s_opath.format(s_study)) #exist_ok=True

for s_id in es_id:
    print("processing: {} ...".format(s_id))
    s_in = s_ipath.format(s_id) + s_imageid.format(s_id)
    s_out = s_opath.format(s_study) + s_imageid.format(s_id)
    shutil.copy(s_in, s_out)
    for i_well in ei_well:
        s_in = s_ipath.format(s_id) + "GT1/" + s_well.format(s_id, i_well)
        s_out = s_opath.format(s_study) + s_well.format(s_id, i_well)
        shutil.copy(s_in, s_out)
