# library
import numpy as np
import os
import pandas as pd

### set ###
s_runtype = 'well'

### off we go ###
s_ipath = f'./{s_runtype}_extract_raw/'
s_opath = f'./{s_runtype}_extract_scale/log2scale/'
os.makedirs(s_opath, exist_ok=True)
df_lmbda_study = pd.DataFrame()
for s_file in sorted(os.listdir(s_ipath)):
    if (s_file.endswith('-raw.tsv.gz')):
        print(f'process: {s_file}')

        # load data
        s_study, s_endpoint, s_ext= s_file.split('-')
        df_scraw = pd.read_csv(f'{s_ipath}{s_file}', sep='\t', index_col=0)

        # shape dataframe
        se_sclog2 = df_scraw.loc[:,s_endpoint].apply(np.log2)

        # fuse result
        s_out = s_file.replace('-raw.tsv.gz','-log2scale-scale.tsv.gz')
        df_sclog2 = pd.merge(df_scraw.drop(s_endpoint, axis=1), se_sclog2.to_frame(), left_index=True, right_index=True)
        df_sclog2.to_csv(f'{s_opath}{s_out}',  sep='\t', compression='gzip')
