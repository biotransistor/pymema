####
# title: mema_acjson_fetch.py
#
# language: Python3
# license: GPLv>=3
# date: 2019-03-08
# author: bue
#
# run: python3 mema_acjson_fetch.py
#
# description:
#    code to fetch and pack the realted acjson annotation in tidy datafarme.
####

# library
from acpipe_acjson import acjson as ac
import json
import os
import pandas as pd
from pymema import run
#import shutil

# const
s_study = os.getcwd().split("/")[-1]
s_runtype = "well"

s_path_input = "./mema_level0/"
s_path_output = "./mema_ac/"
s_path_acjson = "/Users/buchere/srcGitlab/analysis/an/20180408annotV5py/runset/"

s_ref_ligand="PBS_pubchemcid24978514"

# function
es_ok = set()
def acfetch():
    b_out = False
    os.makedirs(s_path_output, exist_ok=True)

    # for each file in input path
    for s_in in sorted(os.listdir(s_path_input)):
        if (s_in.endswith("_level_0.csv")):
            s_barcode = s_in.split("_")[0]
            if not (s_barcode in es_ok):
                s_acjson = f"annot_runset-{s_runtype}-{s_barcode}_ac.json"
                print(f"\nacfetch processing: {s_barcode} ...")

                # load acjson
                #shutil.copyfile(f"{s_path_acjson}{s_acjson}", f"{s_path_output}{s_acjson}")
                with open(f"{s_path_acjson}{s_acjson}") as f_json:
                    d_acjson = json.load(f_json)

                # get tidy annotation tsv
                es_out = ac.acjson2dataframetsv(d_acjson, s_opath=s_path_output)
                for s_out in es_out:
                    # load file
                    df_tidy = pd.read_csv(s_out, sep="\t")
                    # handle index
                    df_tidy.index = df_tidy.apply(lambda n: f"{n.runid}-{n.coor}", axis=1)
                    df_tidy.index.name = "index"
                    # write to file
                    s_tsv = s_out.replace(f"annot_runset-{s_runtype}-","")
                    s_tsv = s_tsv.replace("_dataframetsv", "")
                    s_tsv = s_tsv.replace(".tsv", ".tsv.gz")
                    df_tidy.to_csv(f"{s_tsv}", sep="\t", compression="gzip")
                    os.remove(s_out)
                es_ok.add(s_barcode)

    # output
    b_out = True
    return(b_out)


def refpertu():
    """
    """
    b_out = False
    ds_ref = {}
    for s_file in sorted(os.listdir(s_path_output)):
        if (s_file.endswith("_tidy_perturbation.tsv.gz")):
            print(f"refpertu processing: {s_file}")

            # get run annotation
            df_actsv = run.perturbation_antsv2df(f"{s_path_output}{s_file}")
            df_reference = df_actsv.loc[df_actsv.ligand.isin({s_ref_ligand}),:]
            if (df_reference.shape[0] > 1):
                df_test = df_actsv.loc[- df_actsv.ligand.isin({s_ref_ligand}),:]

                # get ecm ligand and drug set
                as_ecm = df_actsv.ecm.unique()
                es_ligand = set(df_actsv.ligand.unique())
                es_ligand.remove(s_ref_ligand)
                as_drug = df_actsv.drug.unique()

                for s_drug in as_drug:
                    for s_ecm in as_ecm:
                        # get reference perturbation
                        df_ref = df_reference.loc[
                            df_reference.ecm.isin({s_ecm}),
                            :
                        ].loc[
                            df_reference.drug.isin({s_drug}),
                            :
                        ]
                        as_ref = df_ref.perturbation_run.unique()
                        if (len(as_ref) != 1):
                            sys.exit(
                                f"Error @ acfetch : for this ecm {s_ecm} drug {s_drug} combination none or more then one {s_ref_ligand} reference perturbation_run found {as_ref}."
                            )
                        else:
                            s_ref = as_ref[0]
                            # get test perturbation
                            for s_ligand in es_ligand:
                                df_t = df_test.loc[
                                    df_test.ecm.isin({s_ecm}),
                                    :
                                ].loc[
                                    df_test.ligand.isin({s_ligand}),
                                    :
                                ].loc[
                                    df_test.drug.isin({s_drug}),
                                    :
                                ]
                                as_test = df_t.perturbation_run.unique()
                                s_test = as_test[0]
                                if (len(as_test) != 1):
                                    sys.exit(
                                        f"Error @ acfetch : for this ecm {s_ecm} drug {s_drug} ligand {s_ligand} combination none or more then one perturbation_run found {as_test} found."
                                    )

                                # update output
                                ds_ref.update({s_test: s_ref})

    # output to file
    with open(f"{s_path_output}{s_study}-ref_perturbation_run.json", "w") as f_json:
        json.dump(ds_ref, f_json, indent=4, sort_keys=True)

    # output
    b_out = True
    return(b_out)


# main call
if __name__ == '__main__':
    b_out = acfetch()
    print(b_out)
    b_out = refpertu()
    print(b_out)
