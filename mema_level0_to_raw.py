# library
import os
import pandas as pd
from pymema import run
import sys

## set ##
# bue: study folder have to have format study_name_stainingset
s_runtype = 'well'  # bue: well (with fields) or mema (with spots)
s_ipath = f'./{s_runtype}_level0/'
s_acpath = f'./{s_runtype}_ac/'

# const
s_opath = f'./{s_runtype}_extract_raw/'
s_study = os.getcwd().split('/')[-1]
s_ss = s_study.split('_')[-1]

ds_image_segment_column_rename = {
    'Label': 'CellID',
    'Nuclei_GT_Dapi_Surface': 'nucleus_pixel',
    'Cells_GT_CDH1_Surface': 'cell_pixel',
    'Cells_GT_KRT5_Surface': 'cell_pixel',
    'Cells_GT_KRT14_Surface': 'cell_pixel',
    'Cells_GT_KRT19_Surface': 'cell_pixel',
    'Cells_GT_VIM_Surface': 'cell_pixel',
    'Nuclei_GT_Dapi_TotalIntensity': 'Dapi_total_nuc',
    'Nuclei_GT_EdU_TotalIntensity': 'EdU_total_nuc',
    'Cells_GT_CDH1_MeanIntensity': 'CDH1_mean_cell', # added with ss-hlrh1
    'Cells_GT_HER3_MeanIntensity': 'HER3_mean_cell', # added with dapiher3krt14met
    'Cells_GT_KRT5_MeanIntensity': 'KRT5_mean_cell', # added with ss4
    'Cells_GT_KRT14_MeanIntensity': 'KRT14_mean_cell',
    'Cells_GT_KRT19_MeanIntensity': 'KRT19_mean_cell', # added with sso
    'Cells_GT_MET_MeanIntensity': 'MET_mean_cell', # added with dapiher3krt14met
    'Cells_GT_VIM_MeanIntensity': 'VIM_mean_cell',
}

es_column_mean2total = {
    'Nuclei_GT_Dapi_MeanIntensity',
    'Nuclei_GT_EdU_MeanIntensity',
}

dls_ss = {
    'dapieducdh1vim' : ['Dapi_total_nuc', 'EdU_total_nuc', 'CDH1_mean_cell', 'VIM_mean_cell'],  #  sshlrh
    'dapiedukrt5krt19' : ['Dapi_total_nuc', 'EdU_total_nuc', 'KRT5_mean_cell', 'KRT19_mean_cell'],  # ss4
    'dapiedukrt14vim' : ['Dapi_total_nuc', 'EdU_total_nuc', 'KRT14_mean_cell', 'VIM_mean_cell'],  # sst
    'dapiher3krt14met': ['Dapi_total_nuc', 'HER3_mean_cell', 'KRT14_mean_cell', 'MET_mean_cell'],  # ssaa
    'dapikrt14krt19vim' : ['Dapi_total_nuc', 'KRT14_mean_cell', 'KRT19_mean_cell', 'VIM_mean_cell'],  # sso
}
dls_ss.update({'ss4': dls_ss['dapiedukrt5krt19']})
dls_ss.update({'ssaa': dls_ss['dapiher3krt14met']})
dls_ss.update({'sshlrh': dls_ss['dapieducdh1vim']})
dls_ss.update({'sso': dls_ss['dapikrt14krt19vim']})
dls_ss.update({'sst': dls_ss['dapiedukrt14vim']})

es_endpoint = {'cytoplasm_pixel'}
es_endpoint = es_endpoint.union(set(ds_image_segment_column_rename.values()))
es_endpoint.discard('CellID')

if (s_runtype in {'mema'}):
    ls_static = ['runid','runid_well','runid_coor','run','well','coor','perturbation','perturbation_run','ecm','ecm_','ligand','ligand_run','drug','drug_','ImageID','CellID']
    ls_coor_image = ['coor','ImageID',]

elif (s_runtype in {'well'}):
    ls_static = ['runid','runid_well', 'runid_coor','run','well', 'coor','perturbation','perturbation_run','ecm','ecm_','ligand','ligand_run','drug','drug_','ImageID','Field', 'CellID']
    ls_coor_image = ['coor','Field','ImageID',]

else:
    sys.exit('Error: unknowen s_runtype {s_runtype}. check the set section.')

ls_image_segment = [
    'ImageID',
    'CellID',
    'cell_pixel',
    'cytoplasm_pixel',
    'nucleus_pixel',
]

# processing
os.makedirs(s_opath, exist_ok=True)
os.makedirs(f'{s_opath}raw/', exist_ok=True)
for s_file in sorted(os.listdir(s_ipath)):
    if (s_file.endswith('_imageIDs.tsv')):
        s_run = s_file.split('_')[0]
        s_runid = f'{s_runtype}-{s_run}'
        print(f'extract: {s_runid}')

        # load perturbation annotation
        df_assay_coor = run.perturbation_antsv2df(f'{s_acpath}{s_runid}_dataframetsv_tidy_perturbation.tsv')

        # load imageid to spot mapping
        df_coor_image = pd.read_csv(f'{s_ipath}{s_file}', sep='\t')

        if (s_runtype in {'mema'}):
            # get coor
            df_coor_image.sort_values(['WellName','Row','Column'], inplace=True)
            df_coor_image.reset_index(inplace=True)
            df_coor_image['coor'] = df_coor_image.index + 1
            # extract relevanty columns
            df_coor_image = df_coor_image.loc[:,ls_coor_image]
            # load guillaume segmentation output
            df_image_segment = pd.DataFrame()
            for s_well in [
                    '_Well_1_level_0.csv',
                    '_Well_2_level_0.csv',
                    '_Well_3_level_0.csv',
                    '_Well_4_level_0.csv',
                    '_Well_5_level_0.csv',
                    '_Well_6_level_0.csv',
                    '_Well_7_level_0.csv',
                    '_Well_8_level_0.csv',
                ]:
                try:
                    df_image_segment = df_image_segment.append(
                        pd.read_csv(f'{s_ipath}{s_file.replace("_imageIDs.tsv",s_well)}')
                    )
                except FileNotFoundError:
                    print(f'Warning: {s_file.replace("_imageIDs.tsv",s_well)} does not exit (which might be ok for this run or might not).')


        elif (s_runtype in {'well'}):
            # get coor
            i_fieldpwell = df_coor_image.Field.unique().shape[0]
            df_coor_image.sort_values(['Row','Column','Field'], inplace=True)
            df_coor_image.reset_index(inplace=True)
            df_coor_image['coor'] = df_coor_image.index + 1
            df_coor_image['coor'] = df_coor_image.apply(lambda n: (n.Row - 1) * 12 * i_fieldpwell + (n.Column - 1) * i_fieldpwell + n.Field + 1, axis=1)
            # extract relevanty columns
            df_coor_image = df_coor_image.loc[:,ls_coor_image]
            # load guillaume segmentation output
            df_image_segment = pd.read_csv(f'{s_ipath}{s_file.replace("_imageIDs.tsv","_level_0.csv")}')

        else:
            sys.exit('Error: unknowen s_runtype {s_runtype}. check the set section.')

        # raw value normalization
        # bue 20190510: Guillaume mentioned that it would be good for
        # normalizin the images to divide by each images smallest value

        # calculate total intensity
        es_column_raw = set(df_image_segment.columns)
        for s_column_mean in es_column_mean2total.intersection(es_column_raw):
            s_column_area = s_column_mean.replace('_MeanIntensity','_Surface')
            s_column_total = s_column_mean.replace('_MeanIntensity','_TotalIntensity')
            df_image_segment[s_column_total] = df_image_segment.loc[:, s_column_mean] * df_image_segment.loc[:, s_column_area]

        # rename columns
        df_image_segment.rename(ds_image_segment_column_rename, axis=1, inplace=True)
        df_image_segment = df_image_segment.loc[:,~df_image_segment.columns.duplicated()] # there might be more then one cell_pixel column

        # kick cells without nucleus
        df_image_segment = df_image_segment.loc[df_image_segment.nucleus_pixel.notna(),:]

        # handle cytoplasm_pixel
        df_image_segment['cytoplasm_pixel'] = df_image_segment.cell_pixel - df_image_segment.nucleus_pixel
        df_image_segment = df_image_segment.loc[(df_image_segment.cytoplasm_pixel > 0),:]

        # extract relevant columns
        df_image_segment = df_image_segment.loc[:, ls_image_segment + dls_ss[s_ss]]

        # merge perturbation annotation image_id and segment data
        df_extract = pd.merge(df_assay_coor, df_coor_image, on='coor', sort=False)
        df_extract = pd.merge(df_extract, df_image_segment, on='ImageID', sort=False)

        # handle index
        if (s_runtype in {'mema'}):
            df_extract.index = df_extract.apply(lambda n: f'{n.runid}-{n.coor}-{n.CellID}', axis=1)
            df_extract.index.name = 'index'
            df_extract.sort_index(axis=0, inplace=True)
        elif (s_runtype in {'well'}):
            df_extract.index = df_extract.apply(lambda n: f'{n.runid}-{n.coor}-{n.Field}-{n.CellID}', axis=1)
            df_extract.index.name = 'index'
            df_extract.sort_index(axis=0, inplace=True)
        else:
            sys.exit('Error: unknowen s_runtype {s_runtype}. check the set section.')

        # write to file
        es_extract = set(df_extract.columns)
        for s_endpoint in es_endpoint.intersection(es_extract):
            print(f'process endpoint: {s_endpoint}')
            s_out = f'{s_runid}-{s_endpoint}-raw.tsv.gz'
            df_out = df_extract.loc[:,ls_static + [s_endpoint]]
            df_out.to_csv( f'{s_opath}raw/{s_out}', sep='\t', compression='gzip')


# fuse run to study
for s_endpoint in sorted(es_endpoint.intersection(es_extract)):
    print(f'fuse: {s_endpoint}')
    df_scraw_study = pd.DataFrame()
    for s_file in sorted(os.listdir(f'{s_opath}/raw/')):
        if (s_file.endswith(f'-{s_endpoint}-raw.tsv.gz')):
            df_scraw_run = pd.read_csv(f'{s_opath}raw/{s_file}', sep='\t', index_col=0)
            df_scraw_study = df_scraw_study.append(df_scraw_run)
    s_out = f'{s_study}-{s_endpoint}-raw.tsv.gz'
    df_scraw_study.to_csv(f'{s_opath}{s_out}', sep='\t', compression='gzip', header=True)
